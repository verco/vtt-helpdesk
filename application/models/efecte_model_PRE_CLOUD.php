<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Efecte_model extends CI_Model {

	protected $efecte_url="";
	protected $app_url="";

	protected $efecte_web_user="";
	protected $efecte_web_pw="";
	protected $efecte_js_user="web";
	protected $efecte_js_pw="web";
   
   
    function __construct()
    {
        parent::__construct();
        $CI =& get_instance();
    	$this->efecte_url=$CI->config->item('efecte_url');
    	$this->app_url=$CI->config->item('app_url');
    	$this->efecte_web_user = $CI->config->item('webapi_user') != '' ? $CI->config->item('webapi_user') : $this->efecte_web_user;
    	$this->efecte_web_pw = $CI->config->item('webapi_pw') != '' ? $CI->config->item('webapi_pw') : $this->efecte_web_pw;
    }

	function removeDuplicates($results, $debug = false) {
		$out = array();
		$ids = array();
	//	var_dump($results);
		foreach($results as $k => $v){
//			echo "k:".$k."--".var_dump($v)."<br>";
			//echo "k:".$k."--".$v["rd_id"][0]."<br>";
			if(!(in_array($v["rd_id"][0],$ids))){
				array_push($ids,$v["rd_id"][0]);
				array_push($out,$v);
			}
		}
		//echo "ids:".var_dump($ids);
		return $out;
	}
		
	function getAttribute($attribute) {
		return '$'.$attribute.'$';
	}
	
/*	function getAttributeList($attributes) {
		return implode(', ', array_map('getAttribute', $attributes));
	}*/
	
	public function getAttributeSearchList($attributes){
		return "$".implode('$, $', $attributes)."$";
	}
	
	function getCondition($attribute, $operator, $term = false) {
		if ($term == '' || $term === false)
			return '$'.$attribute.'$ '.$operator;
		else
			return '$'.$attribute.'$ '.$operator.' \''.$term.'\'';
	}
	
	function getEqualsCondition($attribute, $term) {
		return $this->getCondition($attribute, '=', $term);
	}
	
	function getOrCondition($attribute, $values) {
		if(count($values)>0){
			$filter=" and (";
			$i=1;
			foreach($values as $v){
				$filter.="$".$attribute."$ = '".$v."'";
				if($i<count($values)){
					$filter.=" or ";
				}
				$i++;
			}
			$filter.=")";
		}else{
			$filter=" and $".$attribute."$ ='xxxxxxxxxxxx'";		
		}		
		
		
		return $filter;
	}
	
	function getTemplateCondition($field, $operator, $template_code) {
		return $field.' '.$operator.' \''.$template_code.'\'';
	}

	function getTemplateEqualsCondition($field, $template_code) {
		return $this->getTemplateCondition($field, '=', $template_code);
	}
	
	function getAndCondition($condition1, $condition2) {
		return "$condition1 and $condition2";
	}
	
	function getSearchString($template_code, $conditions, $deleted = 0) {
		if ($template_code==""){
			return "entity.deleted = $deleted".($conditions ? " and $conditions" : '');
		}else{
			return $this->getTemplateEqualsCondition('entity.template.code', $template_code)." and entity.hidden=0 and entity.deleted = $deleted".($conditions ? " and $conditions" : '');
		}
	}

	function searchValues($template_code, $conditions, $values, $debug=false) {
		$search_string = $this->getSearchString($template_code, $conditions);
		$params["search_string"] = $search_string;

		$xml = new SimpleXMLElement($this->readEntity($params, $debug));
		return $this->get_values($xml, $values);

	}

	function searchEntityValues($template_code, $conditions, $values, $debug = false) {
		$search_string = $this->getSearchString($template_code, $conditions);
		$params["search_string"] = $search_string;

		$xml = new SimpleXMLElement($this->readEntity($params, $debug));
		return $this->get_entity_values($xml, $values, $debug);

	}
	public function echoAttachments($arr,$type=false) {
		foreach($arr as $a){		
			if ($a["name"]!=""){
				$out.= "<a target=\"_blank\" href=\"".site_url('download/file').'/'.$a["entity_id"].'/'.$a["attribute_id"].'/'.$a["location"]."/".$file->$name."\" data-role=\"".$type."\" data-inline=\"true\" data-prefetch data-transition=\"slide\">".$a["name"]."</a><br>";
			}
		}
		return $out;
	}
	/**
	 * Search value(s) for an attribute for single data card.
	 * TODO: rename function?
	 *
	 * @return mixed 	value as string,
	 *					values as an array of strings,
	 *					or false, if value does not exist or query returns multiple data cards
	 */
	function searchValue($template_code, $conditions, $attribute, $debug = false) {
		$arr = $this->searchValues($template_code, $conditions, array($attribute), $debug);
		if (count($arr) == 1) {
			return $arr[0][$attribute];
		}
		return false;
	}

	function searchEntityValue($template_code, $conditions, $attribute, $debug = false) {
		$arr = $this->searchEntityValues($template_code, $conditions, array($attribute), $debug);
		if (count($arr) == 1) {
			return $arr[0][$attribute];
		}
		return false;
	}
	
	function setValues($template_code, $folder_code, $values, $debug = false) {
		// TODO: Take userId as (optional?) parameter
        $params = array();
		$params['template_code'] = $template_code;
		$params['folder_code'] = $folder_code;
		foreach ($values as $attr => $value) {
			$params['attr#'.$attr]['value'] = $value;
		}
		
		$results = $this->saveEntity($params, $debug);
		
		return $results; // TODO: results handling
	}
	
	function setEntityValues($template_code, $folder_code, $values, $ref_values=false, $eref_values=false, $debug = false) {
		// TODO: Take userId as (optional?) parameter
        $params = array();
		$params['template_code'] = $template_code;
		$params['folder_code'] = $folder_code;
		foreach ($values as $attr => $value) {
			$params['attr#'.$attr]['value'] = $value;
		}
		
		foreach ($ref_values as $r_attr => $r_value) {
			$params[$r_attr] = $r_value;
		}
		
		foreach ($eref_values as $er_attr => $er_value) {
			$params[$er_attr] = $er_value;
		}

		log_message('debug', 'params: '.print_r($params, TRUE));
		$results = $this->saveEntity($params, $debug);
		
		return $results; // TODO: results handling
	}

    function delete_entity ($params = false) {
    	$url =$this->efecte_url."/search.ws?query=select%20entity%20from%20entity";
    	$url = $url.$params;

    	$data = '';
    	$headers = array(
			"POST HTTP/1.0",
			"Content-type: text/xml;charset=\"UTF-8\"",
			"Accept: text/xml",
			"Cache-Control: no-cache",
			"Pragma: no-cache",
			"SOAPAction: \"run\"",
			"Content-length: ".strlen($xml_data),
			"Authorization: Basic " . base64_encode($credentials)
		);


		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_USERAGENT, $defined_vars['HTTP_USER_AGENT']);

		$data = curl_exec($ch);

		if (curl_errno($ch)) {
			print "Error: " . curl_error($ch);
		} else {
			if($debug==true){echo $data;}
			curl_close($ch);
		}

		die;
    }

    function get_attribute_ref_file_values ($xml = false, $code = false, $debug = false){
		$result = array();
		if($debug==true){var_dump($xml);}
		$attr_count = 0;
		
		foreach($xml->children() as $entities){
		$not_ex = true;
			foreach($entities->children() as $attr){
				if($attr->getName() =="attribute"){
					foreach($attr->attributes() as $key => $val){
						if($key == "code"){
							if($val == $code){
								foreach($attr->children() as $value){
									foreach($value->attributes() as $key2 => $val2){
				 						if($key2 == "name" || $key2 == 'location'){
											//echo "element:".$key2->getName()."\n";
											//if ($key2->getName()=="value"){
												if($debug==true){echo "index:" .$attr_count. " ,value:".$val2."<br>";}
												$result[$attr_count] = $val2.'';
												
												$attr_count++;
												$not_ex = false;
											//}
										}
									}
								}
							}
						}
					}
				}									
			}
			if ($not_ex == true){
				if($debug==true){echo "index:" .$attr_count. " ,value: N.A <br>";}
				$result[$attr_count] = "";
				$attr_count++;
			}

		}
		
		return $result;
	}

    protected function get_entity_values($xml = false, $values = false, $debug = false, $multiple = true, $template = false) { //, $template = ""
    	$arr = array();
		$temp_arr = array();
		
    	if ($debug) {
			log_message('debug', 'XML:'.$xml->asXML());
			log_message('debug', '$values: '.print_r($values, TRUE));
		}
		//echo $xml->asXml();
		//print_r($values);

		$x=-1;
		$j=0;
    	foreach ($xml->children() as $child) {
			$x++;
		}

    	//foreach ($xml->children() as $v) {
		foreach ($xml->children() as $child) {
			$is_searched_template=false;
			if($multiple == true || $x==$j){
				$single = array();
				$single["###entity_id###"]="";
				$single["###entity_name###"]="";					
				foreach ($child->attributes() as $ea => $ev) {
					if($ea=="id"){
						$single["###entity_id###"]=$ev."";
					}else if($ea=="name"){
						$single["###entity_name###"]=$ev."";					
					}
					$single['entityid'] = $vv;

				}
				foreach ($values as $val) {
					if ($val == 'entityid') {
						foreach ($child->attributes() as $kk => $vv) {
							if ($kk == 'id') 
								$single['entityid'] = $vv;
						}
					}
				}
				if($template != ""){
					foreach ($child->children() as $t) {
						if($t->getName()=="template"){
							foreach ($t->attributes() as $ta => $tv) {
								if ($ta=="code"){
									if($tv==$template){
										//echo "true ta:".$ta."--tv:".$tv."\n\n";
										$is_searched_template=true;
									}
								}
							}
						//if(template!="" && $name->getName();){
							//echo "name:".$name->getName();
						}
					}
				}else{
					$is_searched_template=true;				
				}
				
				//echo "tempalte:".$template."isSeached:".$is_searched_template;
				
				if($is_searched_template){
					//echo "\nadding.. \n";
					foreach ($child->children() as $name) {
						foreach ($values as $value) {
							//echo "--".$name['code']."::".$value."--\n";
							if ($name['code'] == $value) {
								$i=0;
								foreach ($name->children() as $h) {
									//echo "value=>".$h."\n";
									//echo "element:".$h->getName()."\n";
									if ($h->getName()=="value"){
										$single[$value][$i] = "".$h;
										$i++;
									}
								}
								if($single[$value]==""){
									//echo "no value\n";
									$i=0;
									foreach ($name->children() as $ref) {

										foreach ($ref->attributes() as $a => $v) {
											//echo "a:".$a."--v:".$v."\n";
											if ($a=="name"){
												//echo "hep -".$value.$i."::".$v."\n";
												$single[$value][$i] = "".$v;
	//											$single["###refdata###"][$value][$i] = array();
												$single["###refdata###"][$value][$i]["name"]= "".$v;
												$single["###refdata###"][$value][$i]["reference_id"]=$ref['id']."";
												$single["###refdata###"][$value][$i]["entity_id"]=$single["###entity_id###"]."";
												$i++;
											}else if ($a=="id"){
												$single["###refdata###"][$value][$i]["id"]= "".$v;
												$single["###refdata###"][$value][$i]["attribute_id"]=$name['id']."";
											}else if ($a=="location"){
												$single["###refdata###"][$value][$i]["location"]= "".$v;
												$single["###refdata###"][$value][$i]["attribute_id"]=$name['id']."";
											}
										}
									}
								}
							}
						}
					}
					$temp_arr[$j]=$single;
				}
				if (!empty($single)){
					$arr[] = $single;
				}
				
			
			//	echo "\nnext \n";
			}

			$j++;	

        }
		

		
		if ($debug) log_message('debug', 'arr:'.print_r($arr, TRUE));
		//echo "result:".print_r($arr, TRUE);
 		if($multiple){
			//print_r($temp_arr);
			return $temp_arr;
		}else{
			//print_r($arr);	
			return $arr;
		}
    }
	
    protected function get_entity_result_values($xml = false, $values = false, $debug = false, $multiple = true, $template = false) { //, $template = ""
    	$arr = array();
		$temp_arr = array();
		
    	if ($debug) {
			log_message('debug', 'XML:'.$xml->asXML());
			log_message('debug', '$values: '.print_r($values, TRUE));
		}
		//echo $xml->asXml();
		//print_r($values);

		$x=-1;
		$j=0;
		
		$i1=0;
		$i2=0;
    	foreach ($xml->children() as $entity) {
			$i2=0;
			//echo "name:".$entity->getName();
			if($entity->getName() == "row"){
				foreach ($entity->children() as $k => $v) {
					$arr[$i1][$values[$i2]][0]=$v."";
					$i2++;
				}
			}
			$i1++;
			$x++;
		}
		$i1=0;
		$i2=0;
    	foreach ($xml->children() as $row => $val) {
			$i2=0;
			//echo "name:".$row."::".$val.";;";
			if($row != "row"){
				$arr[$i1][$values[$i2]][0]=$val."";
				$i2++;
			}
			$i1++;
		}
	
		if ($debug) log_message('debug', 'arr:'.print_r($arr, TRUE));
		//echo "arr:".print_r($arr);
		return $arr;
    }
	
	protected function get_entity_result_values2($xml, $values) {
    	$arr = array();
		
		$i1=0;
		$i2=0;
		
    	foreach ($xml->children() as $entity) {
			$i2=0;
			if($entity->getName() == "row"){
				foreach ($entity->children() as $k => $v) {
					$arr[$i1][$values[$i2]]=$v."";
					$i2++;
				}
			}
			$i1++;
		}
		
		$i1=0;
		$i2=0;
		
    	foreach ($xml->children() as $row => $val) {
			$i2=0;
			if($row != "row"){
				$arr[$i1][$values[$i2]]=$val."";
				$i2++;
			}
			$i1++;
		}

		return $arr;
    }
	
    protected function get_values($xml = false, $values = false, $debug = false) {
    	$arr = array();
    	if ($debug) {
			log_message('debug', 'XML:'.$xml->asXML());
			log_message('debug', '$values: '.print_r($values, TRUE));
		}

    	foreach ($xml->children() as $child) {
    		$single = array();
    		foreach ($values as $val) {
    			if ($val == 'entityid') {
    				foreach ($child->attributes() as $kk => $vv) {
    					if ($kk == 'id') 
    						$single['entityid'] = $vv."";
    				}
    			}
    		}
            foreach ($child->children() as $name) {
				foreach ($values as $value) {
                    if ($name['code'] == $value) {
						foreach ($name->children() as $h) {
							//echo "element:".$h->getName()."\n";
							if ($h->getName()=="value"){
								$single[$value] = "".$h;
							}
                        }
						if($single[$value]==""){
							foreach ($name->children() as $ref) {
									foreach ($ref->attributes() as $a => $v) {
										//echo "koodi:".$a."--value:".$v."\n";
										if ($a=="name"){
											$single[$value] = "".$v;
										}
									}
							}
						}
					}
                }
            } 
            if (!empty($single))
            	$arr[] = $single;
        }
		if ($debug) log_message('debug', 'arr:'.print_r($arr, TRUE));
		//echo "result:".print_r($arr, TRUE);
        return $arr;
    }

    protected function get_ref_values($xml = false, &$array = false, $ref_values = false, $debug = false) {
    	$i=0;
    	if ($debug) log_message('debug','Efecte_model::get_ref_values(), XML:'.$xml->asXML());
    	foreach ($xml->children() as $child) {
            foreach ($child->children() as $name) {
                foreach ($ref_values as $value) {
	                if ($name['code'] == $value) {
						foreach ($name->children() as $h) {
							foreach ($h->attributes() as $kkk => $vvv) {
								$q = $vvv."";
								$array[$i][$value][] = $q;
							}
	                    } 
	                }  
	            } 
            }
        }  
        return true; 
    }

	protected function readEntity($params = false, $debug = false) {
		log_message('debug','START READENTITY');
		$start = microtime(true);
		log_message('debug','ID: '.$start);

		//$debug=true;
		$start_time = date("d-m-Y H:i:s");
	//	echo "start:".$start_time."<br>";
		if ($debug) log_message('debug', 'Efecte query started at '.$start_time);
		$credentials = $this->efecte_web_user.":".$this->efecte_web_pw;
		$disitinct="";
		if(isset($params["distinct"])){
		//	$disitinct="distinct%20";
		}
		
		$data="";
		$url =$this->efecte_url."/search.ws?query=select%20".$disitinct."entity%20from%20entity";
		if(isset($params["full_search_string"])){
			$url=$this->efecte_url."/search.ws?query=".str_ireplace(" ","%20",$params["full_search_string"]);
		} elseif(isset($params["search_string"])){
			$url=$url."%20where%20".str_ireplace(" ","%20",$params["search_string"]);
		}

		if ($debug) {
			log_message('debug', 'URL: '.$url);
			//echo "URL:".$url."<br>";

		}
//echo "URL:".$url;
		$headers = array(
			"POST HTTP/1.0",
			"Content-type: text/xml;charset=\"UTF-8\"",
			"Accept: text/xml",
			"Cache-Control: no-cache",
			"Pragma: no-cache",
			//"Content-length: ".strlen($xml_data),
			"Authorization: Basic " . base64_encode($credentials)
		);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		/* N.B.! These options do not work correctly on Quercus because the implementation is missing */
		// Comment out or set to true in production systems, if using SSL and 3rd party certificates
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // TRUE
		//curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);     // 2
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_USERAGENT, $defined_vars['HTTP_USER_AGENT']);

		$data = curl_exec($ch);

		if (curl_errno($ch)) {
			print "\nError: " . curl_errno($ch) . " " . curl_error($ch);
			die;
		} else {
			if($debug==true){echo $data;}
			curl_close($ch);
		}

		//$end_time = date("d-m-Y H:i:s");
//		if ($debug==true) log_message('debug', 'Efecte query ended at '.$end_time); //.' : query time:'.$end_time-$start_time);
		log_message('debug','END READENTITY. TIME: '.(microtime(true) - $start).' URL: '.$url);
		//$datetime1 = date_create('2009-10-11');
//$datetime2 = date_create('2009-10-13');
//$interval = date_diff($datetime1, $datetime2);
		//if ($debug==true) log_message('debug', "Query time: ".string(strtotime($end_time)-strtotime($start_time)));
		//if ($debug==true) log_message('debug', $data);
	//	echo "end:".$end_time."<br>";
		//return $data;
		return utf8_encode($data);
	
	}

	/**
     * Notes:
     * For multivalue Attributes, the params can be
     */
    protected function saveEntity($params = false, $main_debug = false ){
		
		if ($main_debug) $debug = true;
		if ($debug) {
			log_message('debug',"params:". print_r($params,true));
		}		
		
		$credentials = $this->efecte_web_user.":".$this->efecte_web_pw;
		
		if (isset($params["importxml"])){
			$importxml = $params["importxml"];
		}else{
			$refEntities ="";
			$importxml = '<?xml version="1.0" encoding="UTF-8"?><entityset><entity><template code="'.$params["template_code"].'"/>';
		
			foreach ($params as $key => $value){
				log_message('debug',$key."->".$value."\n");
				//echo "start:".substr($key,0,4).":<br>";
				//echo "end:".substr($key,5).":<br>";
				if (substr($key,0,4) == "attr"){
					if(isset($params[substr($key,0,4)]["reference"])){
					}else{
						$importxml.="<attribute code=\"".substr($key,5)."\">";
						foreach($params[$key] as $key2 => $value2){
                            //echo $key2."->".$value2.";</br>";
                            if(substr($key2,0,5) == "value"){
                                // NOTE: supports single value Attributes as plain values AND
                                // multi-values as Arrays
                                $valueArray = (is_array($value2)) ? $value2 : array($value2);
                                foreach ($valueArray as $multiValueEntry) {
                                    $importxml.="<value>".$multiValueEntry."</value>";
                                }
                            }
						}
						$importxml.="</attribute>";
					}
				} elseif (substr($key,0,4) == "refe"){
						//<reference id=\"".$ids[0]."\"/>
						$refdata=explode(":",substr($key,5));
						if ($refdata[1]!=""){
							//echo "refdata".$refdata[1]."::";
							$importxml.="<attribute code=\"".$refdata[0]."\">";
							$importxml.="<reference id=\"".$refdata[1]."\" />";
							$importxml.="</attribute>";
							if (isset($params[$key]["xml"])){

								log_message('debug','WHATTT:'.$params[$key]["xml"]);
								log_message('debug','WHATTT_2:'.$value);
								//echo "extra ".$key." added<br>";
								/*if (strlen($params[$key]["xml"]) > 3)
								 $refEntities.=$params[$key]["xml"];*/
								if (strlen($value) > 3)
								 $refEntities.=$value;
							}
						}
				} elseif (substr($key,0,4) == "embe"){
						//<reference id=\"".$ids[0]."\"/>
						$refdata=explode(":",substr($key,5));
						$importxml.="<attribute code=\"".$refdata[0]."\">";
						$importxml.="<embeddedreference id=\"$refdata[1]\" />";
						$importxml.="</attribute>";
						if (isset($params[$key]["xml"])){

							log_message('debug','WHATTT:'.$params[$key]["xml"]);
							//echo "extra ".$key." added<br>";
							if (strlen($params[$key]["xml"]) > 3)
							 $refEntities.=$params[$key]["xml"];
						}
						if (isset($params[$key])){

							log_message('debug','WHATTT2:'.$params[$key]);
							//echo "extra ".$key." added<br>";
							if (strlen($params[$key]) > 3)
							 $refEntities.=$params[$key];
						}
				
				}
			
			}
			$importxml.="</entity>";
			$importxml.=$refEntities;
			$importxml.="</entityset>";
		
		}
	
		//if ($debug) {
			log_message('debug', "import xml:".$importxml);
		//}
	
		$url = $this->efecte_url."/dataCardImport.ws?folderCode=".$params["folder_code"]."&createEmptyReferences=true";
		
		if ($debug) {
			log_message('debug',$url);
		}
		
		$headers = array(
			"POST HTTP/1.0",
			"Content-type: text/xml;charset=\"UTF-8\"",
			"Accept: text/xml",
			"Cache-Control: no-cache",
			"Pragma: no-cache",
			"SOAPAction: \"run\"",
			"Content-length: ".strlen(utf8_encode($importxml)),
			"Authorization: Basic " . base64_encode($credentials)
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_USERAGENT, $defined_vars['HTTP_USER_AGENT']);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $importxml);
		
		$data = curl_exec($ch);
		
		if (curl_errno($ch)) {
			print "Error: " . curl_error($ch);
			log_message('error',"Efecte update failed - Error:" . curl_error($ch));	
		} else {
			if($debug) { echo $data;
			log_message('debug',"Efecte import XML:".$importxml);	
			log_message('debug',"Efecte updated succesfull - result:".$data);	
			}
			curl_close($ch);
		}
		//return $data;
		//RESULT HANDLING
		$result=Array();
		$result["message"]=$data;
		//echo $data;
		
		$dataXml=new SimpleXMLElement($data);

		foreach($dataXml->children() as $key => $val){
			$result[$key]=$val;
		}

		if($result["description"]=="Error in xml-import"){
			$result["error"]=true;
			$result["errorxml"]=$dataXml;
		}else{
			$result["error"]=false;
		}
		log_message('debug',$result);

		if($debug==true){print_r($result);}		
		return $result;
		//RESULT HANDLING
	
	}

	protected function removeEntity($params = false, $debug = false) {
		$credentials = $this->efecte_web_user.":".$this->efecte_web_pw;
		
		$r=array();

		if(isset($params["id"])){
			$url=$this->efecte_url."/delete.ws?id=".$params["id"];
		} elseif(isset($params["query"])){
			$url=$this->efecte_url."/delete.ws?query=".$params["query"];
		}

		if ($debug) {
			log_message('debug', 'URL: '.$url);
		}
		
		if($url!=""){

			$headers = array(
				"POST HTTP/1.0",
				"Content-type: text/xml;charset=\"UTF-8\"",
				"Accept: text/xml",
				"Cache-Control: no-cache",
				"Pragma: no-cache",
				"Content-length: ".strlen($xml_data),
				"Authorization: Basic " . base64_encode($credentials)
			);
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			/* N.B.! These options do not work correctly on Quercus because the implementation is missing */
			// Comment out or set to true in production systems, if using SSL and 3rd party certificates
			//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // TRUE
			//curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);     // 2
			
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_USERAGENT, $defined_vars['HTTP_USER_AGENT']);

			$data = curl_exec($ch);

			if (curl_errno($ch)) {
				print "\nError: " . curl_errno($ch) . " " . curl_error($ch);
				die;
			} else {
				if($debug==true){echo $data;}
				curl_close($ch);
			}

			if ($debug) log_message('debug', $data);

			$r["result"]=utf8_encode($data);
			log_message('debug', 'removal success: result:'.$data);		
		}else{
			$r["error"]="No removal data.";
			log_message('debug', 'no removal info?');		
		}
		return $r;
	}
	
	
	function get_template_static_dropdown_attribute_values($attribute_id = false, $debug = false) {
        $CI =& get_instance();
        $CI->config->load('efecte');
        $params["full_search_string"] = "select value from com.efecte.datamodel.statics.StaticString where attribute.id = ".$attribute_id;
        $xml = new SimpleXMLElement($this->readEntity($params,$debug));
        $arr = array();
        foreach ($xml->string as $value) {
            $arr[$value.""] = $value."";
        }

        return ($arr);
    }

	function createSaveParams ($key = false, $key2 = false, $value = false, &$param, $debug = true) {
    
     	if ($key != false && $key2 != false && $value != false) {
 			// wrap values to cdata
 			if (substr($key,0,5) != 'refe:' && substr($key,0,5) != 'embe:') {
 				$param["attr#".$key][$key2] = '<![CDATA[
 				'.
 				$value.'
 				]]>';
 				log_message('error','value: '.$param["attr#".$key][$key2]);
 			}
 			elseif (substr($key,0,5) == 'refe:') {
 				$param["refe:".substr($key,5).':'.$key2] = $value;
 				foreach ($param as $k => $v) {
 					log_message('error','keys:'.$k.' value:'.$v);
 				}
 			} elseif (substr($key,0,5) == 'embe:') {
 				$param["embe:".substr($key,5).':'.$key2] = $value;
 				foreach ($param as $k => $v) {
 					log_message('error','keys:'.$k.' value:'.$v);
 				}
 			}
 			log_message('debug','Value added');
     		return $param;
     	} elseif ($key != false && $key2 == false && $value != false) {
     		// dont wrap template_code or folder_code
     		$param[$key] = $value;
    		log_message('debug','basic values added');
     		return $param;
     	}
     }

     function createRefEntity ($array = false, $ref_id = false, $ref_template, $debug = false) {
     	$entity = '';
     	$entity = '<entity id="'.$ref_id.'">';
     	$entity .= '<template code="'.$ref_template.'" />';
     	foreach ($array as $key => $value) {
     		$entity .= '<attribute code="'.$key.'">';
     		$entity .= '<value>'.$value.'</value>';
     		$entity .= '</attribute>';
     	}
     	$entity .= '</entity>';
     	if ($debug) log_message('debug','REF ENTITY: '.$entity);
     	return $entity;
     }

	
	/* FOR THE FEEDBACK "APPLICATION" */
	function getReferencePath($refs) {
		return implode(':', $refs);
	}

	function searchAttributeValues($template_code, $attributes, $conditions, $debug = false) {
        if ($debug) {
            log_message('debug', 'searchAttributeValues() $conditions: '.$conditions);
        }
		$conditions = 'entity.deleted = 0'.($conditions ? " and $conditions" : '');
		return $this->readFormalEntity($template_code, $conditions, $attributes, $debug);
	}

	private function readFormalEntity ($template_code = false, $conditions = false, $fields = array(), $debug = false) {
		/*import java.text.SimpleDateFormat;
		import java.util.Date;*/
		
		$CI =& get_instance();
		
		$params = array();
		$params["full_search_string"] = 'select entity.id, $'.implode("$, $", $fields).'$ from entity where template.code = \''.$template_code.'\' and '.$conditions;
		if ($debug) log_message('debug',$params["full_search_string"]);
		$xml = new SimpleXMLElement($this->readEntity($params,$debug));

		if ($debug) log_message('debug', 'XML:'.$xml->asXML());
		
		$arr = array();

		$k=0;
		foreach ($xml->children() as $row => $child) {
			//log_message('debug', 'row: '.print_r($row, true).', child: '.print_r($child, true));

			$i = -1;
			//if (sizeof($fields) > 1) {
				foreach ($child->children() as $type => $value) {
					//log_message('debug', 'type: '.print_r($type, true).', value: '.print_r($value, true));
                    if ($i == -1) {
                        $arr[$k]['###entityid###'] = (string) $value;
                        $i++;
                        continue;
                    }

					if ($type == 'null') {
						$i++;
						continue;
					}

	                $arr[$k][$fields[$i]] = $value . "";
                	$i++;                    
                }

			$k++;
		}

		if ($debug) log_message('debug', 'arr:'.print_r($arr, true));

		return $arr;
	}

}