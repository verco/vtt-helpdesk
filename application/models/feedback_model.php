<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Feedback_model extends Efecte_model
{
    /*
public $name   = '';
public $description = '';
public $version = '';
public $format  = '';
public $language = '';
public $file = '';
*/
    function __construct()
    {
        parent::__construct();
        $CI = &get_instance();
        $this->config = $CI->config;
    }
    /*
function get_all_questions() {
$CI =& get_instance();
$CI->config->load('efecte');
$arr = array();
foreach ($CI->config->item('fscfeedback_all_questions') as $question) {
$params["full_search_string"]="select ga.attribute.name from template t join templateGroups.group.groupAttributes ga where t.code = '".$CI->config->item('fscfeedback_template')."' and ga.code = '".$question."'";
$arr[] = new SimpleXMLElement($this->readEntity($params));
}

return $arr;
}
*/
    public function has_positive_feedback($ticket_id)
    {
        $newInc = 'INCN-';
        if (strpos($ticket_id, $newInc) === 0) {
            $conditions = $this->getEqualsCondition($this->getReferencePath(
                array($this->config->item('feedback_ticket_id'), $this->config->item('ticket_id'))
            ), $ticket_id);
        } else {
            $conditions = $this->getEqualsCondition($this->getReferencePath(
                array($this->config->item('feedback_ticket'), $this->config->item('ticket_id'))
            ), $ticket_id);
        }
        $feedbacks = $this->searchAttributeValues(
            $this->config->item('feedback_template'),
            array($this->config->item('feedback_resolution_accepted')),
            $conditions
        );

        foreach ($feedbacks as $feedback) {
            if ($feedback[$this->config->item('feedback_resolution_accepted')] == $this->config->item('feedback_resolution_accepted/yes')) {
                return true;
            }
        }

        return false;
    }

    public function create($ticket_id, $resolution_accepted, $rating = false, $comments = false, $expectation = false)
    {

        $attributes = array(
            $this->config->item('feedback_ticket') => $ticket_id,
            $this->config->item('feedback_ticket_id') => $ticket_id,
            $this->config->item('feedback_resolution_accepted') => $resolution_accepted
        );
        /*if ($satisfaction !== false) {
$attributes[$this->config->item('feedback_customer_satisfaction')] = $satisfaction;
}*/
        if ($rating !== false) {
            $attributes[$this->config->item('feedback_customer_satisfaction_rating')] = $rating;
        }
        if ($comments !== false) {
            $attributes[$this->config->item('feedback_comments')] = $comments;
        }
        if ($expectation !== false) {
            $attributes[$this->config->item('feedback_customer_satisfaction')] = $expectation;
        }

        $result = $this->setValues(
            $this->config->item('feedback_template'),
            $this->config->item('feedback_template_folder'),
            $attributes
        );

        return $result;
    }


    public function reopen_and_add_comment($ticket_id, $comment, $ticket)
    {

        if ($ticket_id === false) {
            return false;
        }

        $newInc = 'INCN-';
        $oldInc = 'INC-';
        $req = 'SR-';
        $currentDateTime = new DateTime();
        $currentDateTime->setTimezone(new DateTimeZone('Europe/Helsinki'));
        $now = $currentDateTime->format('Y/m/d H:i:s');

        // if template is incident
        if (strpos($ticket_id, $oldInc) === 0) {
            $attributes = array(
                $this->config->item('ticket_efecteid') => $ticket_id,
                $this->config->item('ticket_status') => $this->config->item('ticket_status_reopened')
            );
            $result = $this->setValues($this->config->item('ticket_template'), $this->config->item('ticket_folder'), $attributes);
        }

        if (strpos($ticket_id, $newInc) === 0) {
            $attributes = array(
                $this->config->item('new_incident_efecteid') => $ticket_id,
                $this->config->item('new_incident_status') => $this->config->item('new_incident_status_reopened'),
                //$this->config->item('new_incident_comment') => $comment
                $this->config->item('new_incident_comment') => '{"date":"' . $now . '","author":"' . $ticket['customer'] . '","message":"' . $comment . '"}'
            );
            $result = $this->setValues($this->config->item('new_incident_template'), $this->config->item('new_incident_folder'), $attributes);
        }

        if (strpos($ticket_id, $req) === 0) {
            $attributes = array(
                $this->config->item('new_request_efecteid') => $ticket_id,
                $this->config->item('new_request_status') => $this->config->item('new_request_status_reopened'),
                // Comment structure needs to be JSON {"date":"2020-11-16T07:33:19+0000","author":"Kangas Ilkka","message":"THIS IS COMMENT"}
                $this->config->item('new_request_comment') => '{"date":"' . $now . '","author":"' . $ticket['RequestedFor'] . '","message":"' . $comment . '"}'
            );
            $result = $this->setValues($this->config->item('new_request_template'), $this->config->item('new_request_folder'), $attributes);
        }
        return $result;
    }

    function get_ticket($ticket_id)
    {

        $tickets = '';
        $newInc = 'INCN-';
        $oldInc = 'INC-';
        $req = 'SR-';

        // if template is incident
        if (strpos($ticket_id, $oldInc) === 0) {
            $conditions = $this->getEqualsCondition($this->config->item('ticket_efecteid'), $ticket_id);
            $attributes = array(
                $this->config->item('ticket_person'),
                $this->config->item('ticket_subject'),
                $this->config->item('ticket_description'),
                $this->config->item('ticket_resolution'),
                $this->config->item('ticket_efecteid'),
                $this->config->item('ticket_created'),
                $this->config->item('ticket_updated')
            );

            $tickets = $this->searchAttributeValues($this->config->item('ticket_template'), $attributes, $conditions, false);
        }

        // if template is incident_new
        if (strpos($ticket_id, $newInc) === 0) {
            $conditions = $this->getEqualsCondition($this->config->item('new_incident_efecteid'), $ticket_id);

            $attributes = array(
                $this->config->item('new_incident_person'),
                $this->config->item('new_incident_subject'),
                $this->config->item('new_incident_description'),
                $this->config->item('new_incident_resolution'),
                $this->config->item('new_incident_efecteid'),
                $this->config->item('new_incident_incident_primary'),
                $this->config->item('new_incident_created'),
                $this->config->item('new_incident_updated')
            );
            $tickets = $this->searchAttributeValues($this->config->item('new_incident_template'), $attributes, $conditions, false);
            //var_dump($tickets);die;
        }

        // if template is ServiceRequest
        if (strpos($ticket_id, $req) === 0) {
            $conditions = $this->getEqualsCondition($this->config->item('new_request_efecteid'), $ticket_id);
            $attributes = array(
                $this->config->item('new_request_person'),
                $this->config->item('new_request_subject'),
                $this->config->item('new_request_details'),
                $this->config->item('new_request_resolution'),
                $this->config->item('new_request_efecteid'),
                $this->config->item('new_request_created'),
                $this->config->item('new_request_updated')
            );

            $tickets = $this->searchAttributeValues($this->config->item('new_request_template'), $attributes, $conditions, false);
            //var_dump($tickets);die;
        }
        //var_dump($ticket);die;
        if (count($tickets) != 1) {
            return false;
        }
        //$tickets = $this->map_values($tickets);
        $ticket = $tickets[0];

        //$ticket['attachments'] = $this->get_ticket_attachments($ticket['###entityid###']);

        return $ticket;
    }
}
