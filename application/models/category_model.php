<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category_model extends Efecte_model {

    public $name   = '';
    public $description = '';
    public $version = '';
    public $format  = '';
    public $language = '';
    public $file = '';

    function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
    }

    function get_all_main($debug = false) {
      
        $CI =& get_instance();
        $CI->config->load('efecte');
        
        $params["search_string"] = "entity.template.code = '".$CI->config->item('cat_template')."' and entity.deleted = 0 and $".$CI->config->item('cat_level')."$ != '1' order by ".$CI->config->item('cat_name')." desc";
        $xml = new SimpleXMLElement($this->readEntity($params,$debug));
        
        $arr = $this->get_values($xml, $CI->config->item('cat_show'));

        return $arr; // return the one incident
  	}
}