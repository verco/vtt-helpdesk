<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Request_model extends Efecte_model {

    function __construct()
    {
        parent::__construct();
    }

    function get_latest() {
        $CI =& get_instance();
        $CI->config->load('efecte');
        $params["search_string"] = "entity.template.code = '".$CI->config->item('helpdesk_template')."' and entity.deleted = 0 and entity.hidden = 0 and \$created\$ > '".date("d.m.Y", strtotime("yesterday"))."' order by $".$CI->config->item('helpdesk_created')."$ desc";
        $xml = new SimpleXMLElement($this->readEntity($params));
        
        $arr = $this->get_values($xml, $CI->config->item('helpdesk_show'));
        
        return $arr[0]; 
    }

    function get_requests($customer = false, $debug = false) {
        if ($customer != false) {
            $CI =& get_instance();
            $CI->config->load('efecte');
            $params["search_string"] = "entity.template.code = '".$CI->config->item('helpdesk_template')."' and $".$CI->config->item('helpdesk_customer').":".$CI->config->item('person_username')."$ = '".$customer."' and entity.deleted = 0 order by $".$CI->config->item('helpdesk_created')."$ desc";
            $xml = new SimpleXMLElement($this->readEntity($params, $debug));
            
            $arr = $this->get_values($xml, $CI->config->item('helpdesk_show'));
            
            return $arr; 
        } else return false;
    }

    function get_service_requests($customer = false, $debug = false) {
        if ($customer != false) {
            $CI =& get_instance();
            $CI->config->load('efecte');
            $params["search_string"] = "entity.template.code = '".$CI->config->item('request_template')."' and $".$CI->config->item('request_customer').":".$CI->config->item('person_username')."$ = '".$customer."' and entity.deleted = 0 order by $".$CI->config->item('request_created')."$ desc";
            $xml = new SimpleXMLElement($this->readEntity($params, $debug));
            
            $arr = $this->get_values($xml, $CI->config->item('request_show'));
            
            return $arr; 
        } else return false;
    }

    function get_single_request($entityid = false, $customer = false, $debug = false) {
        $CI =& get_instance();
        $CI->config->load('efecte');
        $params["search_string"] = "entity.template.code = '".$CI->config->item('helpdesk_template')."' and $".$CI->config->item('helpdesk_customer').":".$CI->config->item('person_username')."$ = '".$customer."' and entity.deleted = 0 and id = '".$entityid."'";
        $xml = new SimpleXMLElement($this->readEntity($params,$debug));
       
        $arr = $this->get_values($xml, $CI->config->item('helpdesk_show'));
        
        //$arr[0]->attachments = $this->get_attribute_ref_file_values($xml,$CI->config->item('helpdesk_attachments'));

        return $arr[0]; // return the one incident
   }

   function save_close($entityid = false, $customer = false) {
        $CI =& get_instance();
        $CI->config->load('efecte');

        
   }

}