<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Knowledge_model extends Efecte_model {

    public $name   = '';
    public $description = '';
    public $version = '';
    public $format  = '';
    public $language = '';
    public $file = '';

    function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();
    }

    function search_by_term($term = false, $debug = false) {
        if ($term === false || $term == '') return '';
        $CI =& get_instance();
        $CI->config->load('efecte');
        $params["search_string"] = "entity.template.code = '".$CI->config->item('kb_template')."' and ($".$CI->config->item('kb_description')."$ like '*$term*' or $".$CI->config->item('kb_name')."$ like '*$term*') and $".$CI->config->item('kb_publication')."$ = '".$CI->config->item('kb_external')."' and entity.deleted = 0";
        $xml = new SimpleXMLElement($this->readEntity($params, $debug));
        
        $arr = $this->get_values($xml, $CI->config->item('kb_show'));

        if (count( (array) $arr[0]) > 0)
            return $arr;
        else return '';
    }

    function get_single_article($entityid = false, $debug = false) {
        $CI =& get_instance();
        $CI->config->load('efecte');
        $params["search_string"] = "entity.template.code = '".$CI->config->item('kb_template')."' and $".$CI->config->item('kb_description')."$ like '*$term*' and $".$CI->config->item('kb_publication')."$ = '".$CI->config->item('kb_external')."' and entity.deleted = 0 and id = '".$entityid."'";
        $xml = new SimpleXMLElement($this->readEntity($params));
        
        $arr = $this->get_values($xml, $CI->config->item('kb_show'), $debug);

        foreach ($CI->config->item('kb_ref_show') as $ref) {
            $arr[0]->$ref = $this->get_ref_value($xml, $ref);
        }
        $arr[0]->attachments = $this->get_attribute_ref_file_values($xml,$CI->config->item('kb_attachments'));

        return $arr[0]; // return the one incident
    }
}