<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Process_model extends Efecte_model {

    public $name   = '';
    public $description = '';
    public $version = '';
    public $format  = '';
    public $language = '';
    public $file = '';

    function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
    }

    function get_all($debug = false) {
        $params["search_string"] = "entity.template.code = '".$this->CI->config->item('process_template')."' and entity.deleted = 0 and entity.hidden = 0 order by $".$this->CI->config->item('process_name')."$";
        $xml = new SimpleXMLElement($this->readEntity($params,$debug));
        $arr = $this->get_values($xml, $this->CI->config->item('process_show_fields'));

        foreach ($arr as $key => $process) {
            $params["search_string"] = "entity.template.code = '".$this->CI->config->item('type_template')."' and $".$this->CI->config->item('type_parent')."$ = '".urlencode($process['process_name'])."'  and entity.deleted = 0 and entity.hidden = 0 order by $".$this->CI->config->item('type_name')."$";
            $xml = new SimpleXMLElement($this->readEntity($params,$debug));
            $types = $this->get_values($xml, $this->CI->config->item('type_show_fields'));

            $arr[$key]['types'] = $types; 
        }
        return $arr;
    }
}