<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends Efecte_model {

	public $USER = '';
	public $DOMAIN = '';
	public $WORKSTATION = '';

	function __construct()
	{
		parent::__construct();
	}


	public function set_access_log($user){
        $CI =& get_instance();
        $CI->config->load('efecte');

        $params=array();   
		$day=date("d.m.Y");
		
        $this->createSaveParams('template_code',false,$CI->config->item('tlr_access_log_template'),$params);
        $this->createSaveParams('folder_code',false,$CI->config->item('tlr_access_log_code'),$params);
        $this->createSaveParams($CI->config->item('tlr_access_log_person'),'value',$user,$params);
        $this->createSaveParams($CI->config->item('tlr_access_log_date'),'value',$day,$params);
        $this->createSaveParams($CI->config->item('tlr_access_log_index'),'value',$user.'#'.$day,$params);
    //  var_dump($params);die;
        
        $saveResult = $this->saveEntity($params);
     // var_dump($saveResult);
        //die;		
	}
	
	public function isSuperior($search_user){

		$search_attributes="\$person_number$";
        $params["full_search_string"] = "select $search_attributes from entity where entity.template.code = 'client' and \$person_manager:person_number$='$search_user' and entity.deleted = 0 and entity.hidden = 0"; // and entity.hidden = 0
        //$params["search_string"] = "entity.template.code = '".$CI->config->item('displ_template')."' and \$f_entity_user_reference:".$isSuperior."person_number$='$user' and entity.deleted = 0 and entity.hidden = 0";
		$xml = new SimpleXMLElement($this->readEntity($params,false));
        $arr = $this->get_entity_result_values($xml, array('person_number'));

		//echo "arr:".print_r($arr);
		if ($arr[0]!=""){
			return true;
		}else{
			return false;
		}
	}

	public function hasExtendedList($search_user){

		$search_attributes="\$person_number$";
        $params["full_search_string"] = "select entity from entity where entity.template.code = 'client' and \$person_number$='$search_user' and entity.deleted = 0 and entity.hidden = 0"; // and entity.hidden = 0
        //$params["search_string"] = "entity.template.code = '".$CI->config->item('displ_template')."' and \$f_entity_user_reference:".$isSuperior."person_number$='$user' and entity.deleted = 0 and entity.hidden = 0";
		$xml = new SimpleXMLElement($this->readEntity($params,false));
        $arr = $this->get_entity_values($xml, array('person_number','ad_groups'));

		//echo "arr:".var_dump($arr);
		$out=false;
		
		foreach($arr[0]['ad_groups'] as $g){
			if ($g=='VTT-Hankinnat'){
				$out = true;
			}else if ($g=='vtt.bc-tiimi'){
				$out = true;
			}

		}

		return $out;

	}	
	
	public function isQuality($search_user){

		$search_attributes="\$rde_quality_on_duty$";
        $params["full_search_string"] = "select $search_attributes from entity where entity.template.code = 'research_environment' and \$rde_quality_on_duty:person_number$='$search_user' and entity.deleted = 0"; // and entity.hidden = 0
        //$params["search_string"] = "entity.template.code = '".$CI->config->item('displ_template')."' and \$f_entity_user_reference:".$isSuperior."person_number$='$user' and entity.deleted = 0 and entity.hidden = 0";
		$xml = new SimpleXMLElement($this->readEntity($params,false));
        $arr = $this->get_entity_result_values($xml, array('rde_quality_on_duty'));

		//echo "arr:".print_r($arr);
		if ($arr[0]!=""){
			return true;
		}else{
			return false;
		}
	}

	public function isCoord($search_user){

		$search_attributes="\$rde_on_duty$";
        $params["full_search_string"] = "select $search_attributes from entity where entity.template.code = 'research_environment' and \$rde_on_duty:person_number$='$search_user' and entity.deleted = 0"; // and entity.hidden = 0
        //$params["search_string"] = "entity.template.code = '".$CI->config->item('displ_template')."' and \$f_entity_user_reference:".$isSuperior."person_number$='$user' and entity.deleted = 0 and entity.hidden = 0";
		$xml = new SimpleXMLElement($this->readEntity($params,false));
        $arr = $this->get_entity_result_values($xml, array('rde_on_duty'));

		//echo "arr:".print_r($arr);
		if ($arr[0]!=""){
			return true;
		}else{
			return false;
		}
	}
	
	public function isSuperiorToRetired($search_user){

		$search_attributes="\$person_number$";
        $params["full_search_string"] = "select $search_attributes from entity where entity.template.code = 'client' \$person_emp_state$='P��ttynyt' and \$person_manager:person_number$='$search_user' and entity.deleted = 0";
        //$params["search_string"] = "entity.template.code = '".$CI->config->item('displ_template')."' and \$f_entity_user_reference:".$isSuperior."person_number$='$user' and entity.deleted = 0 and entity.hidden = 0";
		$xml = new SimpleXMLElement($this->readEntity($params,false));
        $arr = $this->get_entity_result_values($xml, array('person_number'));

		//echo "arr:".print_r($arr);
		if ($arr[0]!=""){
			return true;
		}else{
			return false;
		}
	}
	public function getUsersFullname($search_user){
		
		$search_attributes="\$full_name$";
        $params["full_search_string"] = "select $search_attributes from entity where entity.template.code = 'client' and \$person_number$='$search_user'and entity.deleted = 0 and entity.hidden = 0";
        //$params["search_string"] = "entity.template.code = '".$CI->config->item('displ_template')."' and \$f_entity_user_reference:".$isSuperior."person_number$='$user' and entity.deleted = 0 and entity.hidden = 0";
		$xml = new SimpleXMLElement($this->readEntity($params,false));
        $arr = $this->get_entity_result_values($xml, array('full_name'));

		return $arr;
	}	

	public function getEmployees($search_user){
		$search_attributes="\$person_number$,\$full_name$";
        $params["full_search_string"] = "select $search_attributes from entity where entity.template.code = 'client' and (\$person_manager:person_number$='$search_user' OR \$person_manager:person_manager:person_number$='$search_user' OR \$person_manager:person_manager:person_manager:person_number$='$search_user') and entity.deleted = 0 and entity.hidden = 0";
        //$params["search_string"] = "entity.template.code = '".$CI->config->item('displ_template')."' and \$f_entity_user_reference:".$isSuperior."person_number$='$user' and entity.deleted = 0 and entity.hidden = 0";
		$xml = new SimpleXMLElement($this->readEntity($params,false));
        $arr = $this->get_entity_result_values($xml, array('person_number','full_name'));
		
		return $arr;
	}	

	public function getDirectSuperiorTo($search_user){
		//var_dump($search_attributes);
		//var_dump($search_user);
		
		$search_attributes="\$person_number$,\$full_name$,\$person_has_direct_reports$";
        $params["full_search_string"] = "select $search_attributes from entity where entity.template.code = 'client' and \$person_manager:person_number$='$search_user' and entity.deleted = 0 and entity.hidden = 0";
        //$params["search_string"] = "entity.template.code = '".$CI->config->item('displ_template')."' and \$f_entity_user_reference:".$isSuperior."person_number$='$user' and entity.deleted = 0 and entity.hidden = 0";
		$xml = new SimpleXMLElement($this->readEntity($params,false));
        $arr = $this->get_entity_result_values($xml, array('person_number','full_name','person_has_direct_reports'));
		
		$i = 0;
		foreach($arr as $a){
			//var_dump($a);
			//var_dump($a['person_has_direct_reports']);
			if($a['person_has_direct_reports'][0] === "Yes") {
				$array[$i] = array("id" => $a['person_number'][0], "label" => $a['full_name'][0], "value" => $a['person_number'][0], "type" => "folder", "isManager" => "true");
				$i++;
			} else {
				$array[$i] = array("id" => $a['person_number'][0], "label" => $a['full_name'][0], "value" => $a['person_number'][0], "type" => "person", "isManager" => "false");
				$i++;
			}
		}
		//var_dump($array);
		// sort array by full_name in ascending order
			if (count($array) !== 0) {
				foreach ($array as $key => $row) {
					$label[$key] = $row['label'];
				}
				
				array_multisort($label, SORT_ASC, $array);
				//var_dump($array);
			}

		if((!$array)) {$array = array()}
		return $array;
	}
	
	//Palauttaa kaikki henki�n alaiset, ja alaisten alaiset, jne. -TLR
	public function getSuperiorTo($search_user){
		
		$search_attributes="\$person_number$";
        $params["full_search_string"] = "select $search_attributes from entity where entity.template.code = 'client' and (\$person_manager:person_number$='$search_user' OR \$person_manager:person_manager:person_number$='$search_user') and entity.deleted = 0 and entity.hidden = 0";
        //$params["search_string"] = "entity.template.code = '".$CI->config->item('displ_template')."' and \$f_entity_user_reference:".$isSuperior."person_number$='$user' and entity.deleted = 0 and entity.hidden = 0";
		$xml = new SimpleXMLElement($this->readEntity($params,false));
        $arr = $this->get_entity_result_values($xml, array('person_number'));

		$user_str="";
		
		//var_dump($isSuperiorTo);
		foreach($arr as $u){
			$user_str.="'".$u["person_number"][0]."',";
		}
		
		if ($user_str!=""){
			$user_str=substr($user_str,0,-1);
		}
		//echo "U:".$user_str."::";
		return $user_str;
	}

	public function getSuperiorToRetired($search_user){
		
		$search_attributes="\$person_number$";
        $params["full_search_string"] = "select $search_attributes from entity where entity.template.code = 'client' and (\$person_manager:person_number$='$search_user' OR \$person_manager:person_manager:person_number$='$search_user') and entity.deleted = 0 and entity.hidden = 1";
        //$params["search_string"] = "entity.template.code = '".$CI->config->item('displ_template')."' and \$f_entity_user_reference:".$isSuperior."person_number$='$user' and entity.deleted = 0 and entity.hidden = 0";
		$xml = new SimpleXMLElement($this->readEntity($params,false));
        $arr = $this->get_entity_result_values($xml, array('person_number'));

		$user_str="";
		
		//var_dump($isSuperiorTo);
		foreach($arr as $u){
			$user_str.="'".$u["person_number"][0]."',";
		}
		
		if ($user_str!=""){
			$user_str=substr($user_str,0,-1);
		}
		//echo "U:".$user_str."::";
		return $user_str;
	}
	
	public function search_user ($term = false, $login = false, $debug = false) {
		$CI =& get_instance();
		$CI->config->load('efecte');
		if ($login) {
			$search_criteria = $CI->config->item('user_username');
			$term = "= '$term'";
		} else {
			$search_criteria = $CI->config->item('user_fullname');
			$term = "like '*$term*'";
		}

		$params["search_string"] = "entity.template.code = '".$CI->config->item('user_template')."' and entity.deleted = 0 and $".$search_criteria."$ ".$term." and $".$CI->config->item('user_exclude')."$ != 'Yes'";

		$xml = new SimpleXMLElement($this->readEntity($params,$debug));

		$arr = $this->get_values($xml, $CI->config->item('user_fields'));
		if ($login) {
			foreach ($CI->config->item('user_ref_fields') as $val) {
				$arr2[$val] = $this->get_ref_value($xml, $val);
			}
			$arr = array_merge($arr, $arr2);
		}

		if ( ! $login) {
			foreach ($arr as $k => $ar) {
				if ($ar->email != '') { // has to have email (unique information)
					$arr[$k]->label = $ar->full_name;
					$arr[$k]->id = $ar->email;
					$arr[$k]->value = $ar->email;
				}
			}
			//this is was strange bug put had to remove array_pop($arr); // search string was also returned as last value of the array (remove that)
		}

		return $arr;
	}	
	
	public function get_user_data ($login = false, $debug = false) {
		$CI =& get_instance();
		$CI->config->load('efecte');
//		if ($login) {
//			$search_criteria = $CI->config->item('user_username');
//			$term = "= '$term'";
//		} else {
//			$search_criteria = $CI->config->item('user_fullname');
//			$term = "like '*$term*'";
//		}

		$params["search_string"] = "entity.template.code = '".$CI->config->item('person_template')."' and entity.deleted = 0 and $".$CI->config->item('person_account')."$ ='".$login."'";// and $".$CI->config->item('user_exclude')."$ != 'Yes'";

		$xml = new SimpleXMLElement($this->readEntity($params,$debug));

		$arr = $this->get_entity_values($xml, $CI->config->item('person_show'));
		//var_dump($arr);
/*		if ($login) {
			foreach ($CI->config->item('user_ref_fields') as $val) {
				$arr2[$val] = $this->get_ref_value($xml, $val);
			}
			$arr = array_merge($arr, $arr2);
		}*/

/*		if ( ! $login) {
			foreach ($arr as $k => $ar) {
				if ($ar->email != '') { // has to have email (unique information)
					$arr[$k]->label = $ar->full_name;
					$arr[$k]->id = $ar->email;
					$arr[$k]->value = $ar->email;
				}
			}
			//this is was strange bug put had to remove array_pop($arr); // search string was also returned as last value of the array (remove that)
		}
*/
		return $arr;
	}	
	
	function getLogin($debug = false){
		$CI =& get_instance();
		$CI->config->load('efecte');
		//$debug = true;

		$userlevel = 0;
		apache_reset_timeout();
		$headers = apache_request_headers();
//		echo "zzzz".print_r($headers);

		if (!isset($headers['Authorization'])){
			echo "1111";
			header('HTTP/1.1 401 Unauthorized');
			header('WWW-Authenticate: NTLM');
			exit;
		}

		$auth = $headers['Authorization'];
//	echo "auth:".$auth."<br><br>";
//		echo "auth_4:".substr($auth,0,4)."<br><br>";
		//echo "xx:".base64_decode("d2ViOndlYg");
		if (substr($auth,0,4) != 'NTLM') {
			header('HTTP/1.1 401 Unauthorized');
			header('WWW-Authenticate: NTLM');
			exit;
		}
		if (substr($auth,0,4) == 'NTLM') {
			$msg = base64_decode(substr($auth, 4));
//			echo "msg:::".$msg":::";
			if (substr($msg, 0, 8) != "NTLMSSP\x00"){
				echo "die";
				die('error header not recognised');
			}
			if ($msg[8] == "\x01") {
//				echo "msg8-1";
				
				$msg2 = "NTLMSSP\x00\x02"."\x00\x00\x00\x00". // target name len/alloc
					"\x00\x00\x00\x00". // target name offset
					"\x01\x02\x81\x01". // flags
					"\x00\x00\x00\x00\x00\x00\x00\x00". // challenge
					"\x00\x00\x00\x00\x00\x00\x00\x00". // context
					"\x00\x00\x00\x00\x30\x00\x00\x00"; // target info len/alloc/offset

				header('HTTP/1.1 401 Unauthorized');
				header('WWW-Authenticate: NTLM '.trim(base64_encode($msg2)));
				exit;
			}
			else if ($msg[8] == "\x03") {
//				echo "msg8-3:".$msg[8];
				function get_msg_str($msg, $start, $unicode = true) {
					$len = (ord($msg[$start+1]) * 256) + ord($msg[$start]);
					$off = (ord($msg[$start+5]) * 256) + ord($msg[$start+4]);
					if ($unicode)	
						return str_replace("\0", '', substr($msg, $off, $len));
					else
						return substr($msg, $off, $len);
				}
//				echo "user::".get_msg_str($msg, 36);
				//if (get_msg_str($msg, 36) != "web"){
				$HEADERUSER = get_msg_str($msg, 36);
				$DOMAIN = get_msg_str($msg, 28);
				//}
				$WORKSTATION = get_msg_str($msg, 44);

			}
		}

		if($_SESSION["current_user"]!=""){
			$USER=$_SESSION["current_user"];
		}else{
			$_SESSION["current_user"]=$HEADERUSER;
			$USER=$_SESSION["current_user"];
		}


		if($debug==true){echo "Username: ".$USER."<br>";}
		if($debug==true){echo "Domain: ".$DOMAIN."<br>";}
		if($debug==true){echo "Workstation: ".$WORKSTATION."<br>";}

		//var_dump($USER);

		return $USER;
	}				

	function getApprLogin($debug = false){
		$CI =& get_instance();
		$CI->config->load('efecte');

		$result=array();
		
		$userlevel = 0;
		$headers = apache_request_headers();
		if (!isset($headers['Authorization'])){
			header('HTTP/1.1 401 Unauthorized');
			header('WWW-Authenticate: NTLM');
			exit;
		}

		$auth = $headers['Authorization'];

		if (substr($auth,0,5) != 'NTLM ') {
			header('HTTP/1.1 401 Unauthorized');
			header('WWW-Authenticate: NTLM');
			exit;
		}
		if (substr($auth,0,5) == 'NTLM ') {
			$msg = base64_decode(substr($auth, 5));
			if (substr($msg, 0, 8) != "NTLMSSP\x00")
				die('error header not recognised');

			if ($msg[8] == "\x01") {
				$msg2 = "NTLMSSP\x00\x02"."\x00\x00\x00\x00". // target name len/alloc
					"\x00\x00\x00\x00". // target name offset
					"\x01\x02\x81\x01". // flags
					"\x00\x00\x00\x00\x00\x00\x00\x00". // challenge
					"\x00\x00\x00\x00\x00\x00\x00\x00". // context
					"\x00\x00\x00\x00\x30\x00\x00\x00"; // target info len/alloc/offset

				header('HTTP/1.1 401 Unauthorized');
				header('WWW-Authenticate: NTLM '.trim(base64_encode($msg2)));
				exit;
			}
			else if ($msg[8] == "\x03") {
				function get_msg_str($msg, $start, $unicode = true) {
					$len = (ord($msg[$start+1]) * 256) + ord($msg[$start]);
					$off = (ord($msg[$start+5]) * 256) + ord($msg[$start+4]);
					if ($unicode)
						return str_replace("\0", '', substr($msg, $off, $len));
					else
						return substr($msg, $off, $len);
				}
				$HEADERUSER = get_msg_str($msg, 36);
				$this->DOMAIN = get_msg_str($msg, 28);
				$this->WORKSTATION = get_msg_str($msg, 44);
			}
		}

		//$xmlstring = readAuthUser($USER);
		//if($debug==true){echo "xmlstring:".$xmlstring;};
		if ($this->session->userdata('current_user') != '')
			$this->USER = $this->session->userdata('current_user');
		else {
			$this->session->set_userdata('current_user',$HEADERUSER);
			$this->USER = $this->session->userdata('current_user');
		}
		/*
		if($_SESSION["current_user"]!=""){
			$USER=$_SESSION["current_user"];
		}else{
			$_SESSION["current_user"]=$HEADERUSER;
			$USER=$_SESSION["current_user"];
		}*/

		$userParams["search_string"]="entity.template.code = '".$CI->config->item('user_template')."' and \$".$CI->config->item('user_username')."\$='".$this->USER."'";

		$xml = new SimpleXMLElement($this->readEntity($userParams));
		//$xml = new SimpleXMLElement(readAuthUser($USER));
		//$userlevel = false;
		if($debug==true){log_message('debug',"xml:".$xml->asXML())};

		$groups = $this->getAttributeReferenceValues($xml,$PERSON_GROUPS_CODE);

		for($i=0;$i<count($groups);$i++){
			if(strtolower($groups[$i]) == strtolower($PERSON_DEV_ORD_GROUP)){
				$result["device"] = true;
				//echo "DEV - OK";
			}
			else if(strtolower($groups[$i]) == strtolower($PERSON_ACC_ORD_GROUP)){

				$result["account"] = true;
				//echo "ACC - OK";
			}

		}

		if($debug==true){echo "Username: ".$this->USER."<br>";}
		if($debug==true){echo "Domain: ".$this->DOMAIN."<br>";}
		if($debug==true){echo "Workstation: ".$this->WORKSTATION."<br>";}
		
		return $result;
	}




	function getLoginName($debug = false){
		require("conf.php");
		require("xmlFunctions.php");
		require("urlFunctions.php");
		
		$headers = apache_request_headers();
		if (!isset($headers['Authorization'])){
			header('HTTP/1.1 401 Unauthorized');
			header('WWW-Authenticate: NTLM');
			exit;
		}

		$auth = $headers['Authorization'];
		if (substr($auth,0,5) == 'NTLM ') {
			$msg = base64_decode(substr($auth, 5));
			if (substr($msg, 0, 8) != "NTLMSSP\x00")
				die('error header not recognised');

			if ($msg[8] == "\x01") {
				$msg2 = "NTLMSSP\x00\x02"."\x00\x00\x00\x00". // target name len/alloc
					"\x00\x00\x00\x00". // target name offset
					"\x01\x02\x81\x01". // flags
					"\x00\x00\x00\x00\x00\x00\x00\x00". // challenge
					"\x00\x00\x00\x00\x00\x00\x00\x00". // context
					"\x00\x00\x00\x00\x30\x00\x00\x00"; // target info len/alloc/offset

				header('HTTP/1.1 401 Unauthorized');
				header('WWW-Authenticate: NTLM '.trim(base64_encode($msg2)));
				exit;
			}
			else if ($msg[8] == "\x03") {
				function get_msg_str($msg, $start, $unicode = true) {
					$len = (ord($msg[$start+1]) * 256) + ord($msg[$start]);
					$off = (ord($msg[$start+5]) * 256) + ord($msg[$start+4]);
					if ($unicode)
						return str_replace("\0", '', substr($msg, $off, $len));
					else
						return substr($msg, $off, $len);
				}
				$USER = get_msg_str($msg, 36);
				$DOMAIN = get_msg_str($msg, 28);
				$WORKSTATION = get_msg_str($msg, 44);
			}
		}
		if($debug==true){echo "Username: ".$USER."<br>";}
		if($debug==true){echo "Domain: ".$DOMAIN."<br>";}
		if($debug==true){echo "Workstation: ".$WORKSTATION."<br>";}

	return $USER;
	}
}