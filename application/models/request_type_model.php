<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Request_type_model extends Efecte_model {

    function __construct()
    {
        parent::__construct();
    }

    function get_inputs_by_id($entityid = false,$req_field_type = false, $debug = false) {
        $CI =& get_instance();
        $CI->config->load('efecte');
        $params["search_string"] = "entity.template.code = '".$CI->config->item('type_fields_template')."' and $".$CI->config->item('type_fields_request_type')."$ = '".$entityid."' and $".$CI->config->item('type_fields_info_type')."$ = '".$req_field_type."'";
        $xml = new SimpleXMLElement($this->readEntity($params,$debug));
       
        $arr = $this->get_entity_values($xml, $CI->config->item('type_fields_show'));
        //$arr[0]->attachments = $this->get_attribute_ref_file_values($xml,$CI->config->item(   'helpdesk_attachments'));

        return $arr; // return the one incident
    }

   function get_type_by_id($entityid = false, $debug = false) {
        $CI =& get_instance();
        $CI->config->load('efecte');
        $params["search_string"] = "entity.template.code = '".$CI->config->item('type_template')."' and $".$CI->config->item('type_name')."$ = '".$entityid."'";
        $xml = new SimpleXMLElement($this->readEntity($params,$debug));
       
        $arr = $this->get_entity_values($xml, $CI->config->item('type_show_fields'));
        //$arr[0]->attachments = $this->get_attribute_ref_file_values($xml,$CI->config->item(   'helpdesk_attachments'));

        return $arr[0]; // return the one incident
   }

}