<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tlr_model extends Efecte_model {

    function __construct()
    {
        parent::__construct();
    }

    function get_all_devices() {
        $CI =& get_instance();
        $CI->config->load('efecte');
		//$ret=array();
		//Kaikki
		$search_attributes=$this->getAttributeSearchList($CI->config->item('tlr_report_show'));

//		$params["full_search_string"] = "select $search_attributes from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and entity.deleted = 0";// and entity.hidden = 0";// xx  order by \$".$CI->config->item('tlr_dev_resp')."\$";
//		$params["full_search_string"] = "select $search_attributes from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_resp').":person_number$='BELHER' and entity.deleted = 0";// and entity.hidden = 0";// xx  order by \$".$CI->config->item('tlr_dev_resp')."\$";
//			$params["full_search_string"] = "select $search_attributes from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_env').":".$CI->config->item('tlr_devy_resp').":person_number$='BELHER' and entity.deleted = 0 and entity.hidden = 0";// xx order by \$".$CI->config->item('tlr_dev_resp')."\$";		
			$params["full_search_string"] = "select $search_attributes from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_status')."\$='".$CI->config->item('tlr_dev_status_value_inuse')."' and \$".$CI->config->item('tlr_dev_noshow')."\$ is Null and entity.deleted = 0 and entity.hidden = 0";// xx order by \$".$CI->config->item('tlr_dev_resp')."\$";		

		//$s1 = microtime(true);
		$xml = $this->readEntity($params,false);
		//$s2 = microtime(true);
		$xml = new SimpleXMLElement($xml);
		//$s3 = microtime(true);
		$arr_1 = $this->get_entity_result_values2($xml, $CI->config->item('tlr_report_show'));
		//$s4 = microtime(true);
		//log_message('debug', 'times: s1..s2: '.($s2-$s1).', s2..s3: '.($s3-$s2).', s3..s4: '.($s4-$s3));

		//$arr_1 = $this->removeDuplicates($arr_1);
		
//var_dump($xml);
		
		//var_dump($ret);
		return $arr_1;
    }

	
    function get_forsale_devices($extended = false) {
        $CI =& get_instance();
        $CI->config->load('efecte');
		//$ret=array();
		
		$filter=" and $".$CI->config->item('tlr_dev_free_until')."$ > '".date("d.m.Y")."'";
		
		if ($extended){
			$filter="";
		}
		//Kaikki
		
		$search_attributes=$this->getAttributeSearchList($CI->config->item('tlr_forsale_show'));
	
		$params["full_search_string"] = "select $search_attributes from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."'".$filter." and \$".$CI->config->item('tlr_dev_forsale')."\$='".$CI->config->item('tlr_dev_forsale_internal_value')."' and entity.deleted = 0 and entity.hidden = 0";// xx order by \$".$CI->config->item('tlr_dev_resp')."\$";		
//		$params["full_search_string"] = "select entity from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."'".$filter." and \$".$CI->config->item('tlr_dev_forsale')."\$='".$CI->config->item('tlr_dev_forsale_internal_value')."' and entity.deleted = 0 and entity.hidden = 0";// xx order by \$".$CI->config->item('tlr_dev_resp')."\$";		
//var_dump($params);
		$xml = $this->readEntity($params,false);
		$xml = new SimpleXMLElement($xml);
		$arr_1 = $this->get_entity_result_values2($xml, $CI->config->item('tlr_forsale_show'));

		//var_dump($xml);
		
		//var_dump($ret);
		return $arr_1;
    }	
	
    function get_device_summary($user,$groups_summary=false,$isSuperiorTo=false,$isSuperiorToRetired=false,$quality_summary=false,$coord_summary=false) {
        $CI =& get_instance();
        $CI->config->load('efecte');
		$ret=array();
		$ret["specials"]=array();
		//Kaikki
		$search_attributes=$this->getAttributeSearchList($CI->config->item('tlr_dev_summary_show'));

		if($groups_summary){
			$search_string="IN (".$isSuperiorTo.")";
			$params["full_search_string"] = "select $search_attributes from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_resp').":person_number$ IN (".$isSuperiorTo.") and entity.deleted = 0";// and entity.hidden = 0"; // order by \$".$CI->config->item('tlr_dev_resp')."\$";		
		}else if($quality_summary){
			$params["full_search_string"] = "select $search_attributes from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_env').":".$CI->config->item('tlr_quality').":person_number$='$user' and entity.deleted = 0 and entity.hidden = 0"; // xx order by \$".$CI->config->item('tlr_dev_resp')."\$";
		}else if($coord_summary){
			$params["full_search_string"] = "select $search_attributes from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_env').":".$CI->config->item('tlr_devy_resp').":person_number$='$user' and entity.deleted = 0 and entity.hidden = 0";// xx order by \$".$CI->config->item('tlr_dev_resp')."\$";		
		}else{
			$params["full_search_string"] = "select $search_attributes from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_resp').":person_number$='$user' and entity.deleted = 0";// and entity.hidden = 0";// xx  order by \$".$CI->config->item('tlr_dev_resp')."\$";
		}
//echo "x:".$params["full_search_string"];			

		$xml = new SimpleXMLElement($this->readEntity($params,false));
//var_dump($xml);	
		$arr_1 = $this->get_entity_result_values($xml, $CI->config->item('tlr_dev_summary_show'));

		$arr_1 = $this->removeDuplicates($arr_1);
		
//var_dump($xml);
		$arr=array();
		$arr_risk=array();
		$arr_no_risk=array();
		$arr_lo=array();
		$arr_no_lo=array();
		$arr_del=array();
		$arr_kk=array();
		$arr_kk_act=array();
		$arr_ell=array();
		$arr_ell_entities=array();
		$arr_no_lvh=array();
		
		$risk=0;
		$lo=0;
		$del=0;
		$kk=0;
		$no_lvh=0;
		
		//var_dump($arr);die;
		

/*multivalues
	$mv_list=array();
	$mv=array();
//	echo "mv_count".count($arr_1);
//	echo "list".var_dump($arr_1);
	$co=0;
//		foreach($arr_1 as $k){
		for($t=0;$t<count($arr_1);$t++){
			if($co<500){
				//foreach($CI->config->item('tlr_dev_summary_mv_show') as $m){
					$params["full_search_string"] = "select entity from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_id')."$='".$arr_1[$t][$CI->config->item('tlr_dev_id')][0]."'";		
//					$params["full_search_string"] = "select \$".$m."$ from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_id')."$='".$arr_1[$t][$CI->config->item('tlr_dev_id')][0]."'";		
					$xml = new SimpleXMLElement($this->readEntity($params,false));
					$i=0;
					foreach($xml as $v){
						$v=$v."";
						if($v!=""){
							$arr_1[$t][$m][$i]=$v."";
							$i++;
						}
					}
				//}

			}
			$co++;
			//echo "mv_list:".var_dump($mv_list);
		}
//echo "out:".var_dump($arr_1);

//multivalues */
		
/* multivalues OLD
	$mv_list=array();
	$mv=array();
//	echo "mv_count".count($arr_1);
//	echo "list".var_dump($arr_1);
	$co=0;
//		foreach($arr_1 as $k){
		for($t=0;$t<count($arr_1);$t++){
			if($co<500){
				//foreach($CI->config->item('tlr_dev_summary_mv_show') as $m){
					$params["full_search_string"] = "select entity from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_id')."$='".$arr_1[$t][$CI->config->item('tlr_dev_id')][0]."'";		
//					$params["full_search_string"] = "select \$".$m."$ from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_id')."$='".$arr_1[$t][$CI->config->item('tlr_dev_id')][0]."'";		
					$xml = new SimpleXMLElement($this->readEntity($params,false));
					$i=0;
					foreach($xml as $v){
						$v=$v."";
						if($v!=""){
							$arr_1[$t][$m][$i]=$v."";
							$i++;
						}
					}
				//}

			}
			$co++;
			//echo "mv_list:".var_dump($mv_list);
			//echo "mv:".var_dump($mv);
		}

//multivalues OLD*/		
		foreach($arr_1 as $key){
			//var_dump($key);
			foreach($key as $k => $v){
				/*if($k==$CI->config->item('tlr_dev_id')){
					//array_push($arr,$key);
					$arr[$v[0].""]=$key;
				}*/				
				if($k==$CI->config->item('tlr_dev_rt_stat')){
					if($v[0]=="Tehty"){
						if($key[$CI->config->item('tlr_dev_status')][0]!=$CI->config->item('tlr_dev_status_value_del')){
							array_push($arr_risk,$key);
							$risk++;
						}
					}else{
						if($key[$CI->config->item('tlr_dev_status')][0]!=$CI->config->item('tlr_dev_status_value_del')){
							array_push($arr_no_risk,$key);
						}
					}
				}
				//else poistettu
				if($k==$CI->config->item('tlr_dev_status')){
					if($v[0]==$CI->config->item('tlr_dev_status_value_del')){
						$del++;
						array_push($arr_del,$key);					
					}else{
						if($v[0]==$CI->config->item('tlr_dev_status_value_kk')){
							//$kk++;
							array_push($arr_kk,$key);					
						}
						array_push($arr,$key);					
					}
				}
				if($k==$CI->config->item('apu_tlr_dev_laitevastuuhlo')){
					if($v[0]==""){
						$no_lvh++;
						array_push($arr_no_lvh,$key);					
					}
				}


				//else poistettu
				if($k==$CI->config->item('apu_tlr_dev_erit_luokka')){
					array_push($arr_ell,$key);					
					foreach($v as $dev){
						if($dev!=""){
						//echo "dev:".$dev;
							$ret["special"][$dev]++;
							if (!(is_array($ret["specials"][$dev]))){
								$ret["specials"][$dev]=array();
							}
							array_push($ret["specials"][$dev],$key);
						}
					}
					$ret["special_tot"]++;
				}else if($k==$CI->config->item('tlr_dev_kk')){
					if($v[0]!=""){
						array_push($arr_kk,$key);					
					}
				}
			}
		}
		
/*
		foreach($arr as $key){
			foreach($key as $k => $v){
				if($k==$CI->config->item('tlr_dev_rt_stat')){
					if($v[0]=="Tehty"){
						array_push($arr_risk,$key);
						$risk++;
					}else{
						array_push($arr_no_risk,$key);					
					}
				}
				//else poistettu
			if($k==$CI->config->item('tlr_dev_status')){
					if($v[0]=="Poistettu"){
						$del++;
						array_push($arr_del,$key);					
					}else{
						array_push($arr,$key);					
					}


				}
				//else poistettu
				if($k==$CI->config->item('tlr_dev_erit_luokka')){
					array_push($arr_ell,$key);					
					foreach($v as $dev){
						if($dev!=""){
							$ret["special"][$dev]++;
							if (!(is_array($ret["specials"][$dev]))){
								$ret["specials"][$dev]=array();
							}
							array_push($ret["specials"][$dev],$key);
						}
					}
					$ret["special_tot"]++;
				}else if($k==$CI->config->item('tlr_dev_kk')){
					if($v[0]!=""){
						array_push($arr_kk,$key);					
					}
				}
			}
		}*/
		
		
		
		
		/*
		foreach($arr_1 as $key){
			foreach($key as $k => $v){
				if($k==$CI->config->item('tlr_dev_id')){
					//array_push($arr,$key);
					$arr[$v[0].""]=$key;
				}				
				if($k==$CI->config->item('tlr_dev_rt_stat')){
					if($v[0]=="Tehty"){
						array_push($arr_risk,$key);
						$risk++;
					}else{
						array_push($arr_no_risk,$key);					
					}
				}else if($k==$CI->config->item('tlr_dev_status')){
					if($v[0]=="Poistettu"){
						$del++;
						array_push($arr_del,$key);					
					}
				}else if($k==$CI->config->item('tlr_dev_erit_luokka')){
					array_push($arr_ell,$key);					
					foreach($v as $dev){
						if($dev!=""){
							$ret["special"][$dev]++;
							if (!(is_array($ret["specials"][$dev]))){
								$ret["specials"][$dev]=array();
							}
							array_push($ret["specials"][$dev],$key);
						}
					}
					$ret["special_tot"]++;
				}else if($k==$CI->config->item('tlr_dev_kk')){
					if($v[0]!=""){
						array_push($arr_kk,$key);					
					}
				}

			}
		}

*/					
/*		
//multivalues
	$mv_list=array();
	$mv=array();
	//echo "mv_count".count($arr_1);
	$co=0;
		foreach($arr_1 as $k){
			if($co<200){
				foreach($CI->config->item('tlr_dev_summary_mv_show') as $m){
					$params["full_search_string"] = "select \$".$m."$ from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_id')."$='".$k[$CI->config->item('tlr_dev_id')][0]."'";		
					$xml = new SimpleXMLElement($this->readEntity($params,false));
					$i=0;
					foreach($xml as $v){
						$v=$v."";
						if($v!=""){
							if($m==$CI->config->item('tlr_dev_erit_luokka')){
								//if($v!=""){
									$ret["special"][$v.""]++;
									if (!(is_array($ret["specials"][$v.""]))){
										$ret["specials"][$v.""]=array();
									}
									array_push($ret["specials"][$v.""],$k);
								//}
							}
							//$ret["specials"][$v.""][$m][0]="<li>".$v."</li>";

							$mv_list[$k[$CI->config->item('tlr_dev_id')][0]][$m].="<li>".$v."</li>";
							$mv[$k[$CI->config->item('tlr_dev_id')][0]][$m][$i]=$v;
	//						$arr[$k[$CI->config->item('tlr_dev_id')][0]][$m][$i]=$v."";
							$arr[$k[$CI->config->item('tlr_dev_id')][0]][$m][0].="<li>".$v."</li>";
							$i++;
						}else{
							$mv_list[$k[$CI->config->item('tlr_dev_id')][0]][$m].="";					
						}
					}
				}

			}
			$co++;
			//echo "mv_list:".var_dump($mv_list);
			//echo "mv:".var_dump($mv);
		}

//multivalues
*/

		foreach($arr_kk as $key_kk){
			foreach($key_kk as $k_k => $v_v){
				if($k_k==$CI->config->item('tlr_dev_status')){
					if($v_k[0]!=$CI->config->item('tlr_dev_status_value_del')){
						$kk++;
						array_push($arr_kk_act,$key_kk);
					}
				}
			}
		}
		
		$lo=false;
		foreach($arr_ell as $key){
			foreach($key as $k => $v){
				if($k==$CI->config->item('tlr_dev_laitohj')){
					$lo=true;
					array_push($arr_lo,$key);
				}
			}
			if($lo){
				$lo=false;
			}else{
				array_push($arr_no_lo,$key);				
			}
		}
				
		
		//Laitteet jotka ovat jo poistuneilla henkilöillä
		$search_attributes=$this->getAttributeSearchList($CI->config->item('tlr_dev_summary_show'));
		if(($groups_summary)&&($isSuperiorToRetired)){
			$search_string="IN (".$isSuperiorToRetired.")";
			$params_a2["full_search_string"] = "select $search_attributes from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_resp').":person_number$ IN (".$isSuperiorToRetired.") and entity.deleted = 0 and entity.hidden = 0";// xx order by \$".$CI->config->item('tlr_dev_resp')."\$";		
			$xml_a2 = new SimpleXMLElement($this->readEntity($params_a2,false));
			$arr_2 = $this->get_entity_result_values($xml_a2, $CI->config->item('tlr_dev_summary_show'));
			$arr_2 = $this->removeDuplicates($arr_2);
		}else if($quality_summary){
			$params_a2["full_search_string"] = "select $search_attributes from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_env').":".$CI->config->item('tlr_quality').":person_number$='$user' and \$".$CI->config->item('tlr_dev_resp').":person_emp_state$ != 'Voimassa' and entity.deleted = 0 and entity.hidden = 0";// xx order by \$".$CI->config->item('tlr_dev_resp')."\$";		
			$xml_a2 = new SimpleXMLElement($this->readEntity($params_a2,false));		

			$arr_2 = $this->get_entity_result_values($xml_a2, $CI->config->item('tlr_dev_summary_show'));
			$arr_2 = $this->removeDuplicates($arr_2);

			$search_attributes3=$this->getAttributeSearchList($CI->config->item('tlr_devy_show'));
			$params_a3["full_search_string"] = "select $search_attributes3 from entity where entity.template.code = '".$CI->config->item('tlr_devy_template')."' and \$".$CI->config->item('tlr_devy_quality').":person_number$='$user' and \$".$CI->config->item('tlr_devy_koord_ref').":person_emp_state$ != 'Voimassa' and entity.deleted = 0 and entity.hidden = 0";// xx order by \$".$CI->config->item('tlr_dev_resp')."\$";		
			$xml_a3 = new SimpleXMLElement($this->readEntity($params_a3,false));		
			$arr_3 = $this->get_entity_result_values($xml_a3, $CI->config->item('tlr_devy_show'));
			//$arr_3 = $this->removeDuplicates($arr_3);
			
		}
		
		//Käsittelyä odottavat
		$search_attributes_ko=$this->getAttributeSearchList($CI->config->item('tlr_dev_summary_show'));

		if($groups_summary){
			$params_ko["full_search_string"] = "select $search_attributes_ko from entity where (folder.id=883 or folder.id=896) and entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_resp').":person_number$ IN (".$isSuperiorTo.") and entity.deleted = 0 and entity.hidden = 0"; // xx order by \$".$CI->config->item('tlr_dev_resp')."\$";
		}else if($quality_summary){
			$params_ko["full_search_string"] = "select $search_attributes_ko from entity where (folder.id=883 or folder.id=896) and entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_env').":".$CI->config->item('tlr_quality').":person_number$='$user' and entity.deleted = 0 and entity.hidden = 0"; // xx order by \$".$CI->config->item('tlr_dev_resp')."\$";
		}else if($coord_summary){
			$params_ko["full_search_string"] = "select $search_attributes_ko from entity where (folder.id=883 or folder.id=896) and entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_env').":".$CI->config->item('tlr_devy_resp').":person_number$='$user' and entity.deleted = 0 and entity.hidden = 0"; // xx order by \$".$CI->config->item('tlr_dev_resp')."\$";
		}else{
			$params_ko["full_search_string"] = "select $search_attributes_ko from entity where (folder.id=883 or folder.id=896) and entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_resp').":person_number$='$user' and entity.deleted = 0 and entity.hidden = 0"; // order by \$".$CI->config->item('tlr_dev_resp')."\$";
		}
		$xml_ko = new SimpleXMLElement($this->readEntity($params_ko,false));
        $arr_ko = $this->get_entity_result_values($xml_ko, $CI->config->item('tlr_dev_summary_show'));
		$ret["ko_xml"]=array();
	
	
//multivalues
/*
	$mv_list=array();
	$mv=array();
//	echo "mv_count".count($arr_1);
//	echo "list".var_dump($arr_1);
	$co=0;
//		foreach($arr_1 as $k){
		for($t=0;$t<count($arr_ko);$t++){
			if($co<500){
				foreach($CI->config->item('tlr_dev_summary_mv_show') as $m){
					$params["full_search_string"] = "select \$".$m."$ from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_id')."$='".$arr_ko[$t][$CI->config->item('tlr_dev_id')][0]."'";		
					$xml = new SimpleXMLElement($this->readEntity($params,false));
					$i=0;
					foreach($xml as $v){
						$v=$v."";
						if($v!=""){
							$arr_ko[$t][$m][$i]=$v."";
							$i++;
						}
					}
				}

			}
			$co++;
			//echo "mv_list:".var_dump($mv_list);
			//echo "mv:".var_dump($mv);
		}
*/
//multivalues
	
	
		$unassigned=0;		
		foreach($arr_ko as $key2){
			foreach($key2 as $k2 => $v2){
				if($k2==$CI->config->item('tlr_dev_colgr')){
					if(true){
					//if($v2[0]==""){
						array_push($ret["ko_xml"],$key2);
						$unassigned++;
					}
				}
			}
		}





		
		//Laiteohjelmat, jotka kuuluvat erityisluokkalaitteisiin
		$search_attributes_lo_r=$this->getAttributeSearchList($CI->config->item('tlr_devs_summary_show'));		

		if($groups_summary){
//			$params_lo_r["full_search_string"] = "select distinct entity from entity where entity.template.code = '".$CI->config->item('tlr_devs_template')."' and \$".$CI->config->item('tlr_devs_dev').":".$CI->config->item('tlr_dev_resp').":person_number$ IN (".$isSuperiorTo.") and \$".$CI->config->item('tlr_devs_dev').":rd_special_class$ is not Null and entity.deleted = 0 and entity.hidden = 0";
			$params_lo_r["full_search_string"] = "select distinct entity from entity where entity.template.code = '".$CI->config->item('tlr_devs_template')."' and \$".$CI->config->item('tlr_devs_dev').":".$CI->config->item('tlr_dev_resp').":person_number$ IN (".$isSuperiorTo.") and \$".$CI->config->item('tlr_devs_dev').":rd_special_class$ is not Null and entity.deleted = 0 and entity.hidden = 0";
		}else if($quality_summary){
			$params_lo_r["full_search_string"] = "select distinct entity from entity where entity.template.code = '".$CI->config->item('tlr_devs_template')."' and \$".$CI->config->item('tlr_dev_env').":".$CI->config->item('tlr_quality').":person_number$='$user' and \$".$CI->config->item('tlr_devs_dev').":rd_special_class$ is not Null and entity.deleted = 0 and entity.hidden = 0";
		}else if($coord_summary){
			$params_lo_r["full_search_string"] = "select distinct entity from entity where entity.template.code = '".$CI->config->item('tlr_devs_template')."' and \$".$CI->config->item('tlr_dev_env').":".$CI->config->item('tlr_devy_resp').":person_number$='$user' and \$".$CI->config->item('tlr_devs_dev').":rd_special_class$ is not Null and entity.deleted = 0 and entity.hidden = 0";
		}else{
			$params_lo_r["full_search_string"] = "select distinct entity from entity where entity.template.code = '".$CI->config->item('tlr_devs_template')."' and \$".$CI->config->item('tlr_devs_dev').":".$CI->config->item('tlr_dev_resp').":person_number$='$user' and \$".$CI->config->item('tlr_devs_dev').":rd_special_class$ is not Null and entity.deleted = 0 and entity.hidden = 0";
		}
		$xml_lo_r = new SimpleXMLElement($this->readEntity($params_lo_r,false));
        $arr_lo_r = $this->get_entity_values($xml_lo_r, $CI->config->item('tlr_devs_summary_show'));
		$ret["lo_xml"]=$arr_lo_r;

		foreach($arr_lo_r as $key_lo){
			foreach($key_lo as $k_lo => $v_lo){
				$ret["lo"]++;
			}
		}
		
		//Avoimet laitetapahtumat - kaikki
		$search_attributes_all_lt=$this->getAttributeSearchList($CI->config->item('tlr_deve_show'));		
		if($groups_summary){
			$params_all_lt["full_search_string"] = "select distinct entity from entity where entity.template.code = '".$CI->config->item('tlr_deve_template')."' and \$".$CI->config->item('tlr_deve_status')."$='Avoin' and \$".$CI->config->item('tlr_deve_resp').":person_number$ IN (".$isSuperiorTo.") and \$".$CI->config->item('tlr_deve_dev').":".$CI->config->item('tlr_dev_status')."$ != '".$CI->config->item('tlr_dev_status_value_del')."' and entity.deleted = 0 and entity.hidden = 0";
		}else if($quality_summary){
			$params_all_lt["full_search_string"] = "select distinct entity from entity where entity.template.code = '".$CI->config->item('tlr_deve_template')."' and \$".$CI->config->item('tlr_deve_status')."$='Avoin' and \$".$CI->config->item('tlr_deve_dev').".".$CI->config->item('tlr_dev_env').":".$CI->config->item('tlr_quality').":person_number$='$user' and \$".$CI->config->item('tlr_deve_dev').":".$CI->config->item('tlr_dev_status')."$ != '".$CI->config->item('tlr_dev_status_value_del')."' and entity.deleted = 0 and entity.hidden = 0";
		}else if($coord_summary){
			$params_all_lt["full_search_string"] = "select distinct entity from entity where entity.template.code = '".$CI->config->item('tlr_deve_template')."' and \$".$CI->config->item('tlr_deve_status')."$='Avoin' and \$".$CI->config->item('tlr_deve_dev').".".$CI->config->item('tlr_dev_env').":".$CI->config->item('tlr_devy_resp').":person_number$='$user' and \$".$CI->config->item('tlr_deve_dev').":".$CI->config->item('tlr_dev_status')."$ != '".$CI->config->item('tlr_dev_status_value_del')."' and entity.deleted = 0 and entity.hidden = 0";
		}else{
			$params_all_lt["full_search_string"] = "select distinct entity from entity where entity.template.code = '".$CI->config->item('tlr_deve_template')."' and \$".$CI->config->item('tlr_deve_status')."$='Avoin' and \$".$CI->config->item('tlr_deve_resp').":person_number$='$user' and \$".$CI->config->item('tlr_deve_dev').":".$CI->config->item('tlr_dev_status')."$ != '".$CI->config->item('tlr_dev_status_value_del')."' and entity.deleted = 0 and entity.hidden = 0";
		}
		$xml_all_lt = new SimpleXMLElement($this->readEntity($params_all_lt,false));

        $arr_all_lt = $this->get_entity_values($xml_all_lt, $CI->config->item('tlr_deve_show'));
//var_dump($params_all_lt["full_search_string"]);
//var_dump($arr_all_lt);

		$date_now=strtotime('now');

		$i=0;
		foreach($arr_all_lt as $key => $val){
			$datestr=$val['rdi_due_date'][0];
			$datearr=explode('.',$datestr);
			$date=strtotime($datearr[2].'-'.$datearr[1].'-'.$datearr[0]);
			if($date < $date_now){
				$arr_all_lt[$i]['rdi_due_date'][0]="<font color=\"red\">".$val['rdi_due_date'][0]."</font>";
			}
			$i++;
		}

		$ret["lt_all_xml"]=$arr_all_lt;
		//Avoimet laitetapahtumat - erityisluokkalaitteet
		if($groups_summary){
			$params_lt["full_search_string"] = "select distinct entity from entity where entity.template.code = '".$CI->config->item('tlr_deve_template')."' and \$".$CI->config->item('tlr_deve_status')."$='Avoin' and \$".$CI->config->item('tlr_deve_resp').":person_number$ IN (".$isSuperiorTo.") and \$".$CI->config->item('tlr_deve_dev').":rd_special_class$ is not Null and entity.deleted = 0 and entity.hidden = 0";
		}else if($quality_summary){
			$params_lt["full_search_string"] = "select distinct entity from entity where entity.template.code = '".$CI->config->item('tlr_deve_template')."' and \$".$CI->config->item('tlr_deve_status')."$='Avoin' and \$".$CI->config->item('tlr_deve_dev').".".$CI->config->item('tlr_dev_env').":".$CI->config->item('tlr_quality').":person_number$='$user' and \$".$CI->config->item('tlr_deve_dev').":rd_special_class$ is not Null and entity.deleted = 0 and entity.hidden = 0";
		}else if($coord_summary){
			//echo "coord:";
			$params_lt["full_search_string"] = "select distinct entity from entity where entity.template.code = '".$CI->config->item('tlr_deve_template')."' and \$".$CI->config->item('tlr_deve_status')."$='Avoin' and \$".$CI->config->item('tlr_deve_dev').".".$CI->config->item('tlr_dev_env').":".$CI->config->item('tlr_devy_resp').":person_number$='$user' and \$".$CI->config->item('tlr_deve_dev').":rd_special_class$ is not Null and entity.deleted = 0 and entity.hidden = 0";
		}else{
			$params_lt["full_search_string"] = "select distinct entity from entity where entity.template.code = '".$CI->config->item('tlr_deve_template')."' and \$".$CI->config->item('tlr_deve_status')."$='Avoin' and \$".$CI->config->item('tlr_deve_resp').":person_number$='$user' and \$".$CI->config->item('tlr_deve_dev').":rd_special_class$ is not Null and entity.deleted = 0 and entity.hidden = 0";
		}
		$xml_lt = new SimpleXMLElement($this->readEntity($params_lt,false));
		$arr_lt = $this->get_entity_values($xml_lt, $CI->config->item('tlr_deve_show'));
//var_dump($params_lt["full_search_string"]);
//var_dump($arr_lt);
        
		$ret["lt_xml"]=$arr_lt;

		$ret["tot"]=count($arr);
		$ret["risk"]=$risk;
		$ret["del"]=$del;
		$ret["no_lvh"]=$no_lvh;
		$ret["kk"]=$kk;
		$ret["unassigned"]=$unassigned;
		$ret["all_open_jobs"]=count($arr_all_lt);//$open_jobs;
		$ret["open_jobs"]=count($arr_lt);//$open_jobs;
		$ret["lo_count"]=count($arr_lo);
		$ret["no_lo_count"]=count($arr_no_lo);
		$ret["lt_count"]=count($arr_lt);
		$ret["lt_all_count"]=count($arr_all_lt);

		$ret["tot_xml"]=$arr;
		$ret["tot_not_in_service_count"]=count($arr_2);
		$ret["tot_not_in_service_xml"]=$arr_2;
		$ret["tot_unassigned_count"]=count($arr_3);
		$ret["tot_unassigned_xml"]=$arr_3;
		$ret["risk_xml"]=$arr_risk;		
		$ret["no_risk_xml"]=$arr_no_risk;		
		$ret["del_xml"]=$arr_del;		
		$ret["kk_xml"]=$arr_kk_act;		
		$ret["ell_xml"]=$arr_ell;		
		$ret["ell_lo_xml"]=$arr_lo;
		$ret["ell_no_lo_xml"]=$arr_no_lo;
		$ret["ell_entities_xml"]=$arr_ell_entities;
		$ret["arr_no_lvh_xml"]=$arr_no_lvh;
		$ret["mv"]=$mv;
		$ret["mv_list"]=$mv_list;
		
//		var_dump($ret["specials"]);
		return $ret;
    }


    function get_device_summary2($user,$groups_summary=false,$isSuperiorTo=false,$isSuperiorToRetired=false,$quality_summary=false,$coord_summary=false) {
        $CI =& get_instance();
        $CI->config->load('efecte');
		$ret=array();
		$ret["specials"]=array();
		//Kaikki
		$search_attributes=$this->getAttributeSearchList($CI->config->item('tlr_dev_summary_show'));

		if($groups_summary){
			$search_string="IN (".$isSuperiorTo.")";
			$params["full_search_string"] = "select $search_attributes from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_resp').":person_number$ IN (".$isSuperiorTo.") and entity.deleted = 0 and entity.hidden = 0"; // order by \$".$CI->config->item('tlr_dev_resp')."\$";		
		}else if($quality_summary){
			$params["full_search_string"] = "select $search_attributes from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_env').":".$CI->config->item('tlr_quality').":person_number$='$user' and entity.deleted = 0 and entity.hidden = 0"; // xx order by \$".$CI->config->item('tlr_dev_resp')."\$";		
		}else if($coord_summary){
			$params["full_search_string"] = "select $search_attributes from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_env').":".$CI->config->item('tlr_devy_resp').":person_number$='$user' and entity.deleted = 0 and entity.hidden = 0";// xx order by \$".$CI->config->item('tlr_dev_resp')."\$";		
		}else{
			$params["full_search_string"] = "select $search_attributes from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_resp').":person_number$='$user' and entity.deleted = 0 and entity.hidden = 0";// xx  order by \$".$CI->config->item('tlr_dev_resp')."\$";
		}

		$xml = new SimpleXMLElement($this->readEntity($params,false));

		$arr_1 = $this->get_entity_result_values($xml, $CI->config->item('tlr_dev_summary_show'));
/*
		$arr=array();
		$arr_risk=array();
		$arr_no_risk=array();
		$arr_lo=array();
		$arr_no_lo=array();
		$arr_del=array();
		$arr_kk=array();
		$arr_kk_act=array();
		$arr_ell=array();
		$arr_ell_entities=array();
		
		$risk=0;
		$lo=0;
		$del=0;
		$kk=0;
	*/	
		//var_dump($arr);die;
		
//multivalues
	$mv_list=array();
	$mv=array();
		foreach($arr_1 as $k){
			foreach($CI->config->item('tlr_dev_summary_mv_show') as $m){
				if($k==$CI->config->item('tlr_dev_id')){
					array_push($arr,$k);
				}
				$params["full_search_string"] = "select \$".$m."$ from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_id')."$='".$k[$CI->config->item('tlr_dev_id')][0]."'";		
				$xml = new SimpleXMLElement($this->readEntity($params,false));
				$i=0;
				foreach($xml as $v){
					if($v!=""){
						$mv_list[$k[$CI->config->item('tlr_dev_id')][0]][$m].="<li>".$v."</li>";
						$mv[$k[$CI->config->item('tlr_dev_id')][0]][$m][$i]=$v;
						$i++;
					}else{
						$mv_list[$k[$CI->config->item('tlr_dev_id')][0]][$m].="";					
					}
				}
			}
		}
//multivalues

//echo "mv_list:".var_dump($mv_list);
//echo "mv:".var_dump($mv);

/*		foreach($arr_1 as $key){
			foreach($key as $k => $v){
				if($k==$CI->config->item('tlr_dev_id')){
					array_push($arr,$key);
				}
				
				//echo "k:".$k." -- v:".$v[0]."<br>"; 
				
				if($k==$CI->config->item('tlr_dev_rt_stat')){
					if($v[0]=="Tehty"){
						array_push($arr_risk,$key);
						$risk++;
					}else{
						array_push($arr_no_risk,$key);					
					}
				}else if($k==$CI->config->item('tlr_dev_status')){
					if($v[0]=="Poistettu"){
						$del++;
						array_push($arr_del,$key);					
					}
				}else if($k==$CI->config->item('tlr_dev_erit_luokka')){
					array_push($arr_ell,$key);					
					foreach($v as $dev){
						if($dev!=""){
							$ret["special"][$dev]++;
							if (!(is_array($ret["specials"][$dev]))){
								$ret["specials"][$dev]=array();
							}
							array_push($ret["specials"][$dev],$key);
						}
					}
					$ret["special_tot"]++;
				}else if($k==$CI->config->item('tlr_dev_kk')){
					if($v[0]!=""){
						array_push($arr_kk,$key);					
					}
				}

			}
		}
					
	*/	
		foreach($arr_kk as $key_kk){
			foreach($key_kk as $k_k => $v_v){
				if($k_k==$CI->config->item('tlr_dev_status')){
					if($v_k[0]!=$CI->config->item('tlr_dev_status_value_del')){
						$kk++;
						array_push($arr_kk_act,$key_kk);
					}
				}
			}
		}
		
		$lo=false;
		foreach($arr_ell as $key){
			foreach($key as $k => $v){
				if($k==$CI->config->item('tlr_dev_laitohj')){
					$lo=true;
					array_push($arr_lo,$key);
				}
			}
			if($lo){
				$lo=false;
			}else{
				array_push($arr_no_lo,$key);				
			}
		}
		
		//echo "ret:".var_dump($ret);
		//echo "no_lo:".var_dump($arr_no_lo);


		
		
		
		//Laitteet jotka ovat jo poistuneilla henkilöillä
		$search_attributes=$this->getAttributeSearchList($CI->config->item('tlr_dev_summary_show'));
		if(($groups_summary)&&($isSuperiorToRetired)){
		//echo "ccc:";
			$search_string="IN (".$isSuperiorToRetired.")";
			$params_a2["full_search_string"] = "select $search_attributes from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_resp').":person_number$ IN (".$isSuperiorToRetired.") and entity.deleted = 0 and entity.hidden = 0";// xx order by \$".$CI->config->item('tlr_dev_resp')."\$";		
			$xml_a2 = new SimpleXMLElement($this->readEntity($params_a2,false));
		
			$arr_2 = $this->get_entity_result_values($xml_a2, $CI->config->item('tlr_dev_summary_show'));
		}else if($quality_summary){
			$params_a2["full_search_string"] = "select $search_attributes from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_env').":".$CI->config->item('tlr_quality').":person_number$='$user' and \$".$CI->config->item('tlr_dev_resp').":person_emp_state$ != 'Voimassa' and entity.deleted = 0 and entity.hidden = 0";// xx order by \$".$CI->config->item('tlr_dev_resp')."\$";		
			$xml_a2 = new SimpleXMLElement($this->readEntity($params_a2,false));
		
			$arr_2 = $this->get_entity_result_values($xml_a2, $CI->config->item('tlr_dev_summary_show'));
		}



		//Käsittelyä odottavat
		$search_attributes_ko=$this->getAttributeSearchList($CI->config->item('tlr_dev_summary_show'));

		if($groups_summary){
//			$params["search_string"] = "entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_resp').":person_number$ IN (".$isSuperiorTo.") and entity.deleted = 0 and entity.hidden = 0 order by \$".$CI->config->item('tlr_dev_resp')."\$";		
			$params_ko["full_search_string"] = "select $search_attributes_ko from entity where (folder.id=883 or folder.id=896) and entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_resp').":person_number$ IN (".$isSuperiorTo.") and entity.deleted = 0 and entity.hidden = 0"; // xx order by \$".$CI->config->item('tlr_dev_resp')."\$";
		}else if($quality_summary){
			$params_ko["full_search_string"] = "select $search_attributes_ko from entity where (folder.id=883 or folder.id=896) and entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_env').":".$CI->config->item('tlr_quality').":person_number$='$user' and entity.deleted = 0 and entity.hidden = 0"; // xx order by \$".$CI->config->item('tlr_dev_resp')."\$";

		}else if($coord_summary){
			$params_ko["full_search_string"] = "select $search_attributes_ko from entity where (folder.id=883 or folder.id=896) and entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_env').":".$CI->config->item('tlr_devy_resp').":person_number$='$user' and entity.deleted = 0 and entity.hidden = 0"; // xx order by \$".$CI->config->item('tlr_dev_resp')."\$";

		}else{
			$params_ko["full_search_string"] = "select $search_attributes_ko from entity where (folder.id=883 or folder.id=896) and entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_resp').":person_number$='$user' and entity.deleted = 0 and entity.hidden = 0"; // order by \$".$CI->config->item('tlr_dev_resp')."\$";
		}
		$xml_ko = new SimpleXMLElement($this->readEntity($params_ko,false));
        $arr_ko = $this->get_entity_result_values($xml_ko, $CI->config->item('tlr_dev_summary_show'));
		$ret["ko_xml"]=array();
	
		$unassigned=0;
		
		foreach($arr_ko as $key2){
			foreach($key2 as $k2 => $v2){
//				echo "k2:".$k2." -- v2:".$v2[0]."<br>"; 
				if($k2==$CI->config->item('tlr_dev_colgr')){
					if($v2[0]==""){
						array_push($ret["ko_xml"],$key2);
						$unassigned++;
					}
				}
			}
		}
		
		//Laiteohjelmat, jotka kuuluvat erityisluokkalaitteisiin
		$search_attributes_lo_r=$this->getAttributeSearchList($CI->config->item('tlr_devs_summary_show'));		
//		$params_lo_r["full_search_string"] = "select distinct $search_attributes_lo_r from entity where entity.template.code = '".$CI->config->item('tlr_devs_template')."' and \$".$CI->config->item('tlr_devs_dev').":".$CI->config->item('tlr_dev_resp').":person_number$='$user' and entity.deleted = 0 and entity.hidden = 0";

//org		$params_lo_r["full_search_string"] = "select distinct entity from entity where entity.template.code = '".$CI->config->item('tlr_devs_template')."' and \$".$CI->config->item('tlr_devs_dev').":".$CI->config->item('tlr_dev_resp').":person_number$='$user' and entity.deleted = 0 and entity.hidden = 0";

		if($groups_summary){
//			$params_ko["full_search_string"] = "select $search_attributes_ko from entity where (folder.id!=883 or folder.id!=896) and entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_resp').":person_number$ IN (".$isSuperiorTo.") and entity.deleted = 0 and entity.hidden = 0 order by \$".$CI->config->item('tlr_dev_resp')."\$";
			$params_lo_r["full_search_string"] = "select distinct entity from entity where entity.template.code = '".$CI->config->item('tlr_devs_template')."' and \$".$CI->config->item('tlr_devs_dev').":".$CI->config->item('tlr_dev_resp').":person_number$ IN (".$isSuperiorTo.") and \$".$CI->config->item('tlr_devs_dev').":rd_special_class$ is not Null and entity.deleted = 0 and entity.hidden = 0";
		}else if($quality_summary){
			$params_lo_r["full_search_string"] = "select distinct entity from entity where entity.template.code = '".$CI->config->item('tlr_devs_template')."' and \$".$CI->config->item('tlr_dev_env').":".$CI->config->item('tlr_quality').":person_number$='$user' and \$".$CI->config->item('tlr_devs_dev').":rd_special_class$ is not Null and entity.deleted = 0 and entity.hidden = 0";
		}else if($coord_summary){
			$params_lo_r["full_search_string"] = "select distinct entity from entity where entity.template.code = '".$CI->config->item('tlr_devs_template')."' and \$".$CI->config->item('tlr_dev_env').":".$CI->config->item('tlr_devy_resp').":person_number$='$user' and \$".$CI->config->item('tlr_devs_dev').":rd_special_class$ is not Null and entity.deleted = 0 and entity.hidden = 0";
		}else{
			$params_lo_r["full_search_string"] = "select distinct entity from entity where entity.template.code = '".$CI->config->item('tlr_devs_template')."' and \$".$CI->config->item('tlr_devs_dev').":".$CI->config->item('tlr_dev_resp').":person_number$='$user' and \$".$CI->config->item('tlr_devs_dev').":rd_special_class$ is not Null and entity.deleted = 0 and entity.hidden = 0";
		}
		$xml_lo_r = new SimpleXMLElement($this->readEntity($params_lo_r,false));
        $arr_lo_r = $this->get_entity_values($xml_lo_r, $CI->config->item('tlr_devs_summary_show'));
		$ret["lo_xml"]=$arr_lo_r;

		//var_dump($arr_lo_r);
		foreach($arr_lo_r as $key_lo){
			foreach($key_lo as $k_lo => $v_lo){
				$ret["lo"]++;
			}
		}
		
		//Avoimet laitetapahtumat - kaikki
		$search_attributes_all_lt=$this->getAttributeSearchList($CI->config->item('tlr_deve_show'));		
		//ORG $params_lt["full_search_string"] = "select distinct $search_attributes_lt from entity where entity.template.code = '".$CI->config->item('tlr_deve_template')."' and \$".$CI->config->item('tlr_deve_status')."$='Avoin' and \$".$CI->config->item('tlr_deve_resp').":person_number$='$user' and entity.deleted = 0 and entity.hidden = 0";
		if($groups_summary){
			$params_all_lt["full_search_string"] = "select distinct entity from entity where entity.template.code = '".$CI->config->item('tlr_deve_template')."' and \$".$CI->config->item('tlr_deve_status')."$='Avoin' and \$".$CI->config->item('tlr_deve_resp').":person_number$ IN (".$isSuperiorTo.") and entity.deleted = 0 and entity.hidden = 0";
			//$params_all_lt["debug"]=true;
			// new $params_all_lt["full_search_string"] = "select $search_attributes_lt from entity where entity.template.code = '".$CI->config->item('tlr_deve_template')."' and \$".$CI->config->item('tlr_deve_status')."$='Avoin' and \$".$CI->config->item('tlr_deve_resp').":person_number$ IN (".$isSuperiorTo.") and entity.deleted = 0 and entity.hidden = 0";
		}else if($quality_summary){
			//$params_all_lt["debug"]=true;
			$params_all_lt["full_search_string"] = "select distinct entity from entity where entity.template.code = '".$CI->config->item('tlr_deve_template')."' and \$".$CI->config->item('tlr_deve_status')."$='Avoin' and \$".$CI->config->item('tlr_deve_dev').".".$CI->config->item('tlr_dev_env').":".$CI->config->item('tlr_quality').":person_number$='$user' and entity.deleted = 0 and entity.hidden = 0";
		}else if($coord_summary){
			//$params_all_lt["debug"]=true;
			$params_all_lt["full_search_string"] = "select distinct entity from entity where entity.template.code = '".$CI->config->item('tlr_deve_template')."' and \$".$CI->config->item('tlr_deve_status')."$='Avoin' and \$".$CI->config->item('tlr_deve_dev').".".$CI->config->item('tlr_dev_env').":".$CI->config->item('tlr_devy_resp').":person_number$='$user' and entity.deleted = 0 and entity.hidden = 0";
		}else{
			$params_all_lt["full_search_string"] = "select distinct entity from entity where entity.template.code = '".$CI->config->item('tlr_deve_template')."' and \$".$CI->config->item('tlr_deve_status')."$='Avoin' and \$".$CI->config->item('tlr_deve_resp').":person_number$='$user' and entity.deleted = 0 and entity.hidden = 0";
			// new$params_all_lt["full_search_string"] = "select $search_attributes_lt from entity where entity.template.code = '".$CI->config->item('tlr_deve_template')."' and \$".$CI->config->item('tlr_deve_status')."$='Avoin' and \$".$CI->config->item('tlr_deve_resp').":person_number$='$user' and entity.deleted = 0 and entity.hidden = 0";
		}
		$xml_all_lt = new SimpleXMLElement($this->readEntity($params_all_lt,false));

        $arr_all_lt = $this->get_entity_values($xml_all_lt, $CI->config->item('tlr_deve_show'));
        // new $arr_all_lt = $this->get_values($xml_all_lt, $CI->config->item('tlr_deve_show'));

		$date_now=strtotime('now');
//test		$date_now=strtotime('2011-01-01');
//		echo "now:".$date_now."<br><br>";
		
//		var_dump($arr_all_lt);
		$i=0;
		foreach($arr_all_lt as $key => $val){
			$datestr=$val['rdi_due_date'][0];
			$datearr=explode('.',$datestr);
			//var_dump($datearr);
			$date=strtotime($datearr[2].'-'.$datearr[1].'-'.$datearr[0]);
	//		echo "key:".$key.":"."val:".$val['rdi_due_date'][0]."-".strtotime($datearr[2].'-'.$datearr[1].'-'.$datearr[0])."<br>";
			if($date < $date_now){
				$arr_all_lt[$i]['rdi_due_date'][0]="<font color='red'>".$val['rdi_due_date'][0]."</font>";
	//			echo "xx<br>";
			}
			$i++;
		}
		//var_dump($arr_all_lt);
		$ret["lt_all_xml"]=$arr_all_lt;
		//Avoimet laitetapahtumat - erityisluokkalaitteet
		//$search_attributes_lt=$this->getAttributeSearchList($CI->config->item('tlr_deve_show'));		
		//ORG $params_lt["full_search_string"] = "select distinct $search_attributes_lt from entity where entity.template.code = '".$CI->config->item('tlr_deve_template')."' and \$".$CI->config->item('tlr_deve_status')."$='Avoin' and \$".$CI->config->item('tlr_deve_resp').":person_number$='$user' and entity.deleted = 0 and entity.hidden = 0";
		if($groups_summary){
			$params_lt["full_search_string"] = "select distinct entity from entity where entity.template.code = '".$CI->config->item('tlr_deve_template')."' and \$".$CI->config->item('tlr_deve_status')."$='Avoin' and \$".$CI->config->item('tlr_deve_resp').":person_number$ IN (".$isSuperiorTo.") and \$".$CI->config->item('tlr_deve_dev').":rd_special_class$ is not Null and entity.deleted = 0 and entity.hidden = 0";
			//$params_lt["debug"]=true;
			// new $params_lt["full_search_string"] = "select $search_attributes_lt from entity where entity.template.code = '".$CI->config->item('tlr_deve_template')."' and \$".$CI->config->item('tlr_deve_status')."$='Avoin' and \$".$CI->config->item('tlr_deve_resp').":person_number$ IN (".$isSuperiorTo.") and \$".$CI->config->item('tlr_deve_dev').":rd_special_class$ is not Null and entity.deleted = 0 and entity.hidden = 0";
		}else if($quality_summary){
			$params_lt["full_search_string"] = "select distinct entity from entity where entity.template.code = '".$CI->config->item('tlr_deve_template')."' and \$".$CI->config->item('tlr_deve_status')."$='Avoin' and \$".$CI->config->item('tlr_deve_dev').".".$CI->config->item('tlr_dev_env').":".$CI->config->item('tlr_quality').":person_number$='$user' and \$".$CI->config->item('tlr_deve_dev').":rd_special_class$ is not Null and entity.deleted = 0 and entity.hidden = 0";
		}else if($coord_summary){
			$params_lt["full_search_string"] = "select distinct entity from entity where entity.template.code = '".$CI->config->item('tlr_deve_template')."' and \$".$CI->config->item('tlr_deve_status')."$='Avoin' and \$".$CI->config->item('tlr_deve_dev').":".$CI->config->item('tlr_dev_env').":".$CI->config->item('tlr_devy_resp').":person_number$='$user' and \$".$CI->config->item('tlr_deve_dev').":rd_special_class$ is not Null and entity.deleted = 0 and entity.hidden = 0";
		}else{
			$params_lt["full_search_string"] = "select distinct entity from entity where entity.template.code = '".$CI->config->item('tlr_deve_template')."' and \$".$CI->config->item('tlr_deve_status')."$='Avoin' and \$".$CI->config->item('tlr_deve_resp').":person_number$='$user' and \$".$CI->config->item('tlr_deve_dev').":rd_special_class$ is not Null and entity.deleted = 0 and entity.hidden = 0";
			//new $params_lt["full_search_string"] = "select $search_attributes_lt from entity where entity.template.code = '".$CI->config->item('tlr_deve_template')."' and \$".$CI->config->item('tlr_deve_status')."$='Avoin' and \$".$CI->config->item('tlr_deve_resp').":person_number$='$user' and \$".$CI->config->item('tlr_deve_dev').":rd_special_class$ is not Null and entity.deleted = 0 and entity.hidden = 0";
		}
		$xml_lt = new SimpleXMLElement($this->readEntity($params_lt,false));
		$arr_lt = $this->get_entity_values($xml_lt, $CI->config->item('tlr_deve_show'));
        
		//new $arr_lt = $this->get_values($xml_lt, $CI->config->item('tlr_deve_show'));
		$ret["lt_xml"]=$arr_lt;
		
 

		$ret["tot"]=count($arr);
		$ret["risk"]=$risk;
		$ret["del"]=$del;
		$ret["kk"]=$kk;
		$ret["unassigned"]=$unassigned;
		$ret["open_jobs"]=count($arr_lt);//$open_jobs;
		$ret["lo_count"]=count($arr_lo);
		$ret["no_lo_count"]=count($arr_no_lo);
		$ret["lt_count"]=count($arr_lt);
		$ret["lt_all_count"]=count($arr_all_lt);

		$ret["tot_xml"]=$arr;
		$ret["tot_not_in_service_count"]=count($arr_2);
		$ret["tot_not_in_service_xml"]=$arr_2;
		$ret["risk_xml"]=$arr_risk;		
		$ret["no_risk_xml"]=$arr_no_risk;		
		$ret["del_xml"]=$arr_del;		
		$ret["kk_xml"]=$arr_kk_act;		
		$ret["ell_xml"]=$arr_ell;		
		$ret["ell_lo_xml"]=$arr_lo;
		$ret["ell_no_lo_xml"]=$arr_no_lo;
		$ret["ell_entities_xml"]=$arr_ell_entities;
		$ret["mv"]=$mv;
		$ret["mv_list"]=$mv_list;
		
		//var_dump($ret);
		return $ret;
    }



    function get_tlr_devices($user=false,$group=false) {
		//$user="RTESRS";
        $CI =& get_instance();
        $CI->config->load('efecte');

		$search_attributes=$this->getAttributeSearchList($CI->config->item('tlr_dev_show'));
		//$params["full_search_string"] = "select $search_attributes from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$rd_id$='TL05835'";
//		$params["full_search_string"] = "select $search_attributes from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_resp').":person_number$='$user' and entity.deleted = 0 and entity.hidden = 0 order by \$".$CI->config->item('tlr_dev_resp')."\$";
		$params["full_search_string"] = "select $search_attributes from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_resp').":person_number$='$user' and entity.deleted = 0 and entity.hidden = 0"; // xx order by \$".$CI->config->item('tlr_dev_resp')."\$";
		//$params["distinct"] = true;
		//$params["search_string"] = "entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_resp').":person_number$='$user' and entity.deleted = 0 and entity.hidden = 0 order by \$".$CI->config->item('tlr_dev_resp')."\$";

		//		$params["search_string"] = "entity.template.code = '".$CI->config->item('pc_template')."' and \$user:".$isSuperior."person_number$='$user' and entity.deleted = 0 and entity.hidden = 0";
		$xml = new SimpleXMLElement($this->readEntity($params,false));
        
//        $arr = $this->get_entity_values($xml, $CI->config->item('pc_show'));
        $arr = $this->get_entity_result_values($xml, $CI->config->item('tlr_dev_show'));
        return $arr; 
    }

	
	
    function get_tlr_devices_superior_to($isSuperiorTo=false) {
		//$user="RTESRS";
        $CI =& get_instance();
        $CI->config->load('efecte');

		$search_attributes=$this->getAttributeSearchList($CI->config->item('tlr_dev_show'));
		//$params["full_search_string"] = "select $search_attributes from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$rd_id$='TL05835'"; \$rd_special_class\$=Null and 
		$params["full_search_string"] = "select entity from entity where folder.id!=883 and folder.id!=896 and entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_resp').":person_number$ IN (".$isSuperiorTo.") and entity.deleted = 0 and entity.hidden = 0 order by \$".$CI->config->item('tlr_dev_resp')."\$";
		//		$params["search_string"] = "entity.template.code = '".$CI->config->item('pc_template')."' and \$user:".$isSuperior."person_number$='$user' and entity.deleted = 0 and entity.hidden = 0";
		$xml = new SimpleXMLElement($this->readEntity($params,false));
        
        $arr = $this->get_entity_values($xml, $CI->config->item('tlr_dev_show'));
//		var_dump($arr);

		$search_attributes_tlry=$this->getAttributeSearchList($CI->config->item('tlr_devy_show'));
		$i=0;
		foreach($arr as $a){
			//echo "::".$a["###refdata###"]["rd_device_environment"][0]["entity_id"].";;";
//			echo "::".var_dump($a["###refdata###"]["rd_device_environment"]).";;";
			if ($a["###refdata###"]["rd_device_environment"][0]["entity_id"]!=""){
				$tlry_params["full_search_string"] = "select $search_attributes_tlry from entity where entity.id=".$a["###refdata###"]["rd_device_environment"][0]["reference_id"];
				//entity.template.code = '".$CI->config->item('tlr_devy_template')."' and \$".$CI->config->item('tlr_deve_dev').":".$CI->config->item('tlr_dev_id')."$ = '".$a[$CI->config->item('tlr_dev_id')][0]."' and entity.deleted = 0 and entity.hidden = 0 order by \$".$CI->config->item('tlr_deve_act_date')."\$";
				$tlry_xml = new SimpleXMLElement($this->readEntity($tlry_params,false));	
				$tlry_arr = $this->get_entity_result_values($tlry_xml, $CI->config->item('tlr_devy_show'));
				//var_dump($tlry_arr);

				//if($tlre_arr[0][$CI->config->item('tlr_deve_dev')][0]!=""){
					$arr[$i]["rde_on_duty"]=$tlry_arr[0]["rde_on_duty"];
				//}
			}
			$i++;
		}

		$search_attributes_tlre=$this->getAttributeSearchList($CI->config->item('tlr_deve_show'));
		$i=0;
		foreach($arr as $a){
			$tlre_params["full_search_string"] = "select $search_attributes_tlre from entity where entity.template.code = '".$CI->config->item('tlr_deve_template')."' and \$".$CI->config->item('tlr_deve_dev').":".$CI->config->item('tlr_dev_id')."$ = '".$a[$CI->config->item('tlr_dev_id')][0]."' and entity.deleted = 0 and entity.hidden = 0 order by \$".$CI->config->item('tlr_deve_act_date')."\$";
			$tlre_xml = new SimpleXMLElement($this->readEntity($tlre_params,false));	
			$tlre_arr = $this->get_entity_result_values($tlre_xml, $CI->config->item('tlr_deve_show'));

			if($tlre_arr[0][$CI->config->item('tlr_deve_dev')][0]!=""){
				$arr[$i]["events"]=$tlre_arr;
			}
			$i++;
		}

		$search_attributes_tlrs=$this->getAttributeSearchList($CI->config->item('tlr_devs_show'));
		$i=0;
		foreach($arr as $a){
			$tlrs_params["full_search_string"] = "select $search_attributes_tlrs from entity where entity.template.code = '".$CI->config->item('tlr_devs_template')."' and \$".$CI->config->item('tlr_devs_dev').":".$CI->config->item('tlr_dev_id')."$ = '".$a[$CI->config->item('tlr_dev_id')][0]."' and entity.deleted = 0 and entity.hidden = 0 order by \$".$CI->config->item('tlr_devs_next_date')."\$";
			$tlrs_xml = new SimpleXMLElement($this->readEntity($tlrs_params,false));	
			$tlrs_arr = $this->get_entity_result_values($tlrs_xml, $CI->config->item('tlr_devs_show'));

			if($tlrs_arr[0][$CI->config->item('tlr_devs_dev')][0]!=""){
				$arr[$i]["scheds"]=$tlrs_arr;
			}
			$i++;
		}
		//var_dump($arr);

        return $arr; 
    }
	
    function get_tlr_devices_risk($isSuperiorTo=false) {
		//$user="RTESRS";
        $CI =& get_instance();
        $CI->config->load('efecte');

		$search_attributes=$this->getAttributeSearchList($CI->config->item('tlr_dev_show'));
		//$params["full_search_string"] = "select $search_attributes from entity where entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$rd_id$='TL05835'"; \$rd_special_class\$=Null and 
		$params["full_search_string"] = "select entity from entity where folder.id!=883 and folder.id!=896 and \$".$CI->config->item('tlr_dev_status')."\$!='".$CI->config->item('tlr_dev_status_value_del')."' and entity.template.code = '".$CI->config->item('tlr_dev_template')."' and \$".$CI->config->item('tlr_dev_resp').":person_number$ IN (".$isSuperiorTo.") and entity.deleted = 0 and entity.hidden = 0 order by \$".$CI->config->item('tlr_dev_resp')."\$";
		//		$params["search_string"] = "entity.template.code = '".$CI->config->item('pc_template')."' and \$user:".$isSuperior."person_number$='$user' and entity.deleted = 0 and entity.hidden = 0";
		$xml = new SimpleXMLElement($this->readEntity($params,false));
        
        $arr = $this->get_entity_values($xml, $CI->config->item('tlr_dev_show'));
		//var_dump($arr);

		$search_attributes_tlry=$this->getAttributeSearchList($CI->config->item('tlr_devy_show'));
		$i=0;
		foreach($arr as $a){
			//echo "::".$a["###refdata###"]["rd_device_environment"][0]["entity_id"].";;";
//			echo "::".var_dump($a["###refdata###"]["rd_device_environment"]).";;";
			if ($a["###refdata###"]["rd_device_environment"][0]["entity_id"]!=""){
				$tlry_params["full_search_string"] = "select $search_attributes_tlry from entity where entity.id=".$a["###refdata###"]["rd_device_environment"][0]["reference_id"];
				//entity.template.code = '".$CI->config->item('tlr_devy_template')."' and \$".$CI->config->item('tlr_deve_dev').":".$CI->config->item('tlr_dev_id')."$ = '".$a[$CI->config->item('tlr_dev_id')][0]."' and entity.deleted = 0 and entity.hidden = 0 order by \$".$CI->config->item('tlr_deve_act_date')."\$";
				$tlry_xml = new SimpleXMLElement($this->readEntity($tlry_params,false));	
				$tlry_arr = $this->get_entity_result_values($tlry_xml, $CI->config->item('tlr_devy_show'));
				//var_dump($tlry_arr);

				//if($tlre_arr[0][$CI->config->item('tlr_deve_dev')][0]!=""){
					$arr[$i]["rde_on_duty"]=$tlry_arr[0]["rde_on_duty"];
				//}
			}
			$i++;
		}

		$search_attributes_tlre=$this->getAttributeSearchList($CI->config->item('tlr_deve_show'));
		$i=0;
		foreach($arr as $a){
			$tlre_params["full_search_string"] = "select $search_attributes_tlre from entity where entity.template.code = '".$CI->config->item('tlr_deve_template')."' and \$".$CI->config->item('tlr_deve_dev').":".$CI->config->item('tlr_dev_id')."$ = '".$a[$CI->config->item('tlr_dev_id')][0]."' and entity.deleted = 0 and entity.hidden = 0 order by \$".$CI->config->item('tlr_deve_act_date')."\$";
			$tlre_xml = new SimpleXMLElement($this->readEntity($tlre_params,false));	
			$tlre_arr = $this->get_entity_result_values($tlre_xml, $CI->config->item('tlr_deve_show'));

			if($tlre_arr[0][$CI->config->item('tlr_deve_dev')][0]!=""){
				$arr[$i]["events"]=$tlre_arr;
			}
			$i++;
		}

		$search_attributes_tlrs=$this->getAttributeSearchList($CI->config->item('tlr_devs_show'));
		$i=0;
		foreach($arr as $a){
			$tlrs_params["full_search_string"] = "select $search_attributes_tlrs from entity where entity.template.code = '".$CI->config->item('tlr_devs_template')."' and \$".$CI->config->item('tlr_devs_dev').":".$CI->config->item('tlr_dev_id')."$ = '".$a[$CI->config->item('tlr_dev_id')][0]."' and entity.deleted = 0 and entity.hidden = 0 order by \$".$CI->config->item('tlr_devs_next_date')."\$";
			$tlrs_xml = new SimpleXMLElement($this->readEntity($tlrs_params,false));	
			$tlrs_arr = $this->get_entity_result_values($tlrs_xml, $CI->config->item('tlr_devs_show'));

			if($tlrs_arr[0][$CI->config->item('tlr_devs_dev')][0]!=""){
				$arr[$i]["scheds"]=$tlrs_arr;
			}
			$i++;
		}
		//var_dump($arr);

        return $arr; 
    }	
	
	
	
}