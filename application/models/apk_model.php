<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apk_model extends Efecte_model {

    function __construct()
    {
        parent::__construct();
    }

    function get_pp($id=false) {
		//$user="RTESRS";
        $CI =& get_instance();
        $CI->config->load('efecte');

		$search_attributes=$this->getAttributeSearchList($CI->config->item('pp_show'));
		$params["full_search_string"] = "select $search_attributes from entity where entity.template.code = '".$CI->config->item('pp_template')."' and \$".$CI->config->item('pp_id')."$='$id'";
		$xml = new SimpleXMLElement($this->readEntity($params,false));
        
        $arr = $this->get_entity_result_values($xml, $CI->config->item('pp_show'));
        return $arr; 
    }
	
    function get_pp_attachments($id=false) {
		//$user="RTESRS";
        $CI =& get_instance();
        $CI->config->load('efecte');

        $search_attributes=$this->getAttributeSearchList($CI->config->item('pp_show'));
		$params["search_string"] = "entity.template.code = '".$CI->config->item('pp_template')."' and \$".$CI->config->item('pp_id')."$='$id'";
		$xml = new SimpleXMLElement($this->readEntity($params,false));
        
        $arr = $this->get_entity_values($xml, $CI->config->item('pp_show'));
		$att = $this->echoAttachments($arr[0]["###refdata###"][$CI->config->item('pp_files')]);

        return $att; 
    }
 
}