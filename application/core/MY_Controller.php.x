<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	//public static function get_ntlm_user_hash($user) {
		//$userdb = array('loune'=>'test', 'user1'=>'password');
	//	return ntlm_md4(ntlm_utf8_to_utf16le('test'));
		/*if (!isset($userdb[strtolower($user)]))
			return false;	
		return ntlm_md4(ntlm_utf8_to_utf16le($userdb[strtolower($user)]));*/
	//}

    function __construct() {
        parent::__construct();

        if ($this->session->userdata('user') === false) {
        	$this->load->helper('ntlm');

			//session_start();
			$auth = ntlm_prompt("helpdesk", "ad", "espsr184", "ad.vtt.fi", "espsr184.ad.vtt.fi", "get_ntlm_user_hash");
			log_message('debug', 'auth: '.print_r($auth, true));
			if ($auth['authenticated']) {
				//print "You are authenticated as $auth[username] from $auth[domain]/$auth[workstation]";
				$this->set_user($auth['username']);
			} else {
	            show_error('Not authorized', 401);
				exit;
	        }
        } else {
        	$this->set_user($this->session->userdata('user'));
        }

		
		// work around problem with session cookie and IE
        header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');

        // Let's serve only utf-8
        $this->output->set_header('Content-Type: text/html; charset=utf-8');

        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->output->set_header("Cache-Control: post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");


    }

    private function set_user($user) {
    	$this->session->set_userdata('user', $user);
        
        /*$this->load->model('person_model', 'person');
        $person = $this->person->get_person($this->session->userdata('user'));

        $this->template->set('person', $person);
        $this->template->set('person_first_name', $this->person->get_person_first_name($person));*/

        //$this->lang->load('myict', 'finnish');
        //$this->set_language($person);
    }

    /*private function set_language($person = false) {
        // Choose selected language
        if ($this->input->get('lang') == 'fi') {
            $this->session->set_userdata('language', 'fi');
            $this->lang->load('myict', 'finnish');
            $this->template->set('language', 'fi');
        } elseif ($this->input->get('lang') == 'en') {
            $this->session->set_userdata('language', 'en');
            $this->lang->load('myict', 'english');
            $this->template->set('language', 'en');
        } else {
            if ($this->session->userdata('language') == 'fi') {
                $this->lang->load('myict', 'finnish');
                $this->template->set('language', 'fi');
            } elseif ($this->session->userdata('language') == 'en') {
                $this->lang->load('myict', 'english');
                $this->template->set('language', 'en');
            } else {
                // If not selected, find users language and use that
                if ($this->person->customer_is_finnish($person)) {
                    $this->session->set_userdata('language', 'fi');
                    $this->lang->load('myict', 'finnish');
                    $this->template->set('language', 'fi');
                } else {
                    $this->session->set_userdata('language', 'en');
                    $this->lang->load('myict', 'english');
                    $this->template->set('language', 'en');
                }
            }
        }
    }*/


}
