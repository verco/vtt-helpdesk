<!--<div class="widget" id="last_widget_1">
Mikäli listaus sisältää laitteita, jotka eivät omassa käytössäsi, tai listalta puuttuu laitteitasi tai jos muissa tiedoissa on virheitä, ilmoita puutteelliset tiedot <a href="mailto:kayttotuki@vtt.fi">käyttötukeen</a>. Kiitos!
</div>-->
<div class="widget" id="last_widget">
</div>
<div class="widget" id="last_widget_3">
    <h3 class="handle">Riskitarkasteluraportti</h3>
    <div id="tlr_devices">
        <? if ($output): ?>
            <? $i = 0 ?>
<?
//LaiteID, tila, laitevastuuhlö, laitteen nimi, kategoria, valmistaja, Laiteympöaristö, laitekoordinaattori(laiteympäristön takaa), Viranomaisen reksiterinumero, Erityisluokka,
// rakennus, huone, laiteohjelmat, Laiteohjelma / Laitetapahtumat
?>
	<Br>		<table id="table" width="90%">
				<thead><tr>
				<th>LaiteID</th>
				<th>Laitteen tila</th>
				<th>Laitevastuuhenkilö</th>
				<th>Laitteen nimi</th>
				<th>Kategoria</th>
				<th>Valmistaja</th>
				<th>Laiteympäristö</th>
				<th>Laitekoordinaattori</th>
				<th>Viranomaisen rekisterinumero</th>
				<th>Erityisluokka</th>
				<th>Rakennus</th>
				<th>Huone</th>

				<th>RT-Tila</th>
				<th>RT-Päivämäärä</th>
				<th>RT-Tekijä</th>
				<th>RT-Tarkastelun tulos</th>
				<th>RT-doc</th>

				<!--th>&nbsp;</th-->
				<!--th>Laitetapahtumat</th-->
				</tr></thead>
				<tbody>
				<? foreach ($output as $p): ?>
				<tr>
                    <td><?=$p[config_item('tlr_dev_id')][0]?></td>
                    <td><?=$p[config_item('tlr_dev_status')][0]?></td>
                    <td><?=$p[config_item('tlr_dev_resp')][0]?></td>
                    <td><?=$p[config_item('tlr_dev_name')][0]?></td>
                    <td><?=$p[config_item('tlr_dev_cat')][0]?></td>
                    <td><?=$p[config_item('tlr_dev_man')][0]?></td>
                    <td><?=$p[config_item('tlr_dev_env')][0]?>
	
					
					</td>
                    <td><?=$p[config_item('tlr_devy_resp')][0]?></td>
                    <td><?=$p[config_item('tlr_dev_vir_rek')][0]?></td>
                    <td><?
						if ($p[config_item('tlr_dev_erit_luokka')][0] != ""){
							echo "<table>";
							//echo "<tr><th>Aktiviteetti</th><th>Status</th><th>Pvm.</td><th>Tekijä</th></tr>";
							foreach($p[config_item('tlr_dev_erit_luokka')] as $el){
								echo "<tr><td>".$el."</td></tr>";
							}
							/*foreach($p["scheds"] as $ev){
								echo "<tr><td>".$ev[config_item('tlr_devs_scope')][0]."</td><td>".$ev[config_item('tlr_devs_status')][0]."</td><td>".$ev[config_item('tlr_devs_next_date')][0]."</td><td>".$ev[config_item('tlr_devs_type')][0]."</td></tr>";
							}*/
							echo "</table>";
						}
					?></td>
                    <td><?=$p[config_item('tlr_dev_building')][0]?></td>
                    <td><?=$p[config_item('tlr_dev_room')][0]?></td>
                    <!--td><?
						if ($p[config_item('tlr_dev_erit_luokka')][0] != ""){
							echo "<table>";
							//echo "<tr><th>Aktiviteetti</th><th>Status</th><th>Pvm.</td><th>Tekijä</th></tr>";
							foreach($p[config_item('tlr_dev_erit_luokka')] as $el){
								echo "<tr><td>".$el."</td></tr>";
							}
							/*foreach($p["scheds"] as $ev){
								echo "<tr><td>".$ev[config_item('tlr_devs_scope')][0]."</td><td>".$ev[config_item('tlr_devs_status')][0]."</td><td>".$ev[config_item('tlr_devs_next_date')][0]."</td><td>".$ev[config_item('tlr_devs_type')][0]."</td></tr>";
							}*/
							echo "</table>";
						}
					?></td-->
					
                    <td><?=$p[config_item('tlr_dev_rt_stat')][0]?>
                    <td><?=$p[config_item('tlr_dev_rt_date')][0]?>
                    <td><?=$p[config_item('tlr_dev_rt_madeby')][0]?>
                    <td><?=$p[config_item('tlr_dev_rt_res')][0]?>
                    <td><a href="<?=$p[config_item('tlr_dev_rt_doc')][0]?>" target="_blank"><?=$p[config_item('tlr_dev_rt_doc')][0]?></a>
					
                    <!--td>
					<?
						if($p["scheds"]!=""){//[config_item('tlr_deve_id')][0]
							echo "<b>Laiteohjelmat:</b>";
							echo "<table>";
							echo "<tr><th>Aktiviteetti</th><th>Status</th><th>Tyyppi</th><th>Seuraava tapahtuma</td></tr>";
							foreach($p["scheds"] as $ev){
								echo "<tr><td>".$ev[config_item('tlr_devs_scope')][0]."</td><td>".$ev[config_item('tlr_devs_status')][0]."</td><td>".$ev[config_item('tlr_devs_type')][0]."</td><td>".explode(" ",$ev[config_item('tlr_devs_next_date')][0])[0]."</td></tr>";
							}
							echo "</table>";
						}
					
					//=$p[config_item('tlr_dev_laiteohj')][0]
					?>
					<?
						if($p["events"]!=""){//[config_item('tlr_deve_id')][0]
							echo "<b>Laitetapahtumat:</b>";
							echo "<table>";
							echo "<tr><th>Aktiviteetti</th><th>Status</th><th>Toteutunut</th><th>Tekijä</th></tr>";
							foreach($p["events"] as $ev){
								echo "<tr><td>".$ev[config_item('tlr_deve_act')][0]."</td><td>".$ev[config_item('tlr_deve_status')][0]."</td><td>".explode(" ",$ev[config_item('tlr_deve_act_date')][0])[0]."</td><td>".$ev[config_item('tlr_deve_performer')][0]."</td></tr>";
							}
							echo "</table>";
						}
					
					//=$p[config_item('tlr_dev_laitetap')][0]
					
					?>					
					
					
					</td-->
                    <!--td><?
						if($p["events"]!=""){//[config_item('tlr_deve_id')][0]
							echo "<table>";
							echo "<tr><th>Aktiviteetti</th><th>Status</th><th>Pvm.</th><th>Tekijä</th></tr>";
							foreach($p["events"] as $ev){
								echo "<tr><td>".$ev[config_item('tlr_deve_act')][0]."</td><td>".$ev[config_item('tlr_deve_status')][0]."</td><td>".$ev[config_item('tlr_deve_act_date')][0]."</td><td>".$ev[config_item('tlr_deve_performer')][0]."</td></tr>";
							}
							echo "</table>";
						}
					
					//=$p[config_item('tlr_dev_laitetap')][0]
					
					?></td-->
                </tr>
					<? //if ($i == 4) break; ?>
                          
            <? $i++ ?>                             
            <? endforeach; ?>
			</tbody>
			</table>
        <? else: ?>
            <h4>None found</h4>
        <? endif; ?>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $('#tablex').dataTable({
		"oLanguage": {
			"sUrl": "<?=base_url()?>resources/js/finnish1.txt"
			} 
		}	
	);} 
	);

</script>