<?$b = base_url(); $br=$b.'resources/';?>
<!--<div class="widget" id="last_widget_1">
Mikäli listaus sisältää laitteita, jotka eivät omassa käytössäsi, tai listalta puuttuu laitteitasi tai jos muissa tiedoissa on virheitä, ilmoita puutteelliset tiedot <a href="mailto:kayttotuki@vtt.fi">käyttötukeen</a>. Kiitos!
</div>-->

<div class="widgets" id="last_widget">
    <!--div id="tlr_devices"-->
	<div class="widget g2">
<script type="text/javascript">
    function show_hide(id) {
       var e = document.getElementById(id);
       if(e.style.display == '')
          e.style.display = 'none';
       else
          e.style.display = '';
    }

    function hideAll() {
		document.getElementById('own_table').style.display = 'none';
        document.getElementById('own_asc').style.display = 'none';
        document.getElementById('own_desc').style.display = '';

		document.getElementById('sup_table').style.display = 'none';
        document.getElementById('sup_asc').style.display = 'none';
        document.getElementById('sup_desc').style.display = '';

		document.getElementById('q_table').style.display = 'none';
        document.getElementById('q_asc').style.display = 'none';
        document.getElementById('q_desc').style.display = '';

		document.getElementById('c_table').style.display = 'none';
        document.getElementById('c_asc').style.display = 'none';
        document.getElementById('c_desc').style.display = '';
    }

</script>
	<?
	
	//echo "::".var_dump($usum["lt_xml"])."::";
	//Laiteohjelmat, jotka kuuluvat erityisluokkalaitteisiin? ["lo"]
	?>
    <h5 class="handle">
	
	<img onclick="show_hide('own_table');show_hide('own_desc');show_hide('own_asc');" id="own_desc" src="<?= $br ?>images/sort_desc.png">
	<img onclick="show_hide('own_table');show_hide('own_desc');show_hide('own_asc');" id="own_asc" style="display:none" src="<?= $br ?>images/sort_asc.png">
	Laitevastuuhenkilönäkymä
	</h5>
<div id="own_table" style="display:none">
        <? if ($usum["tot"]>0): ?>	
	<table class="summary_table">
			<tr>
			<td width="50"><a href="#" class="table_link" data-type="tot">Vastuullasi olevat tutkimuslaitteet</a></td>
			<td><a href="#" class="table_link" data-type="tot"><?=$usum["tot"]?></a></td>
			</tr>
			<tr>
			<td>Riskitarkastelu tehty / ei tehty</td>
			<td><a href="#" class="table_link" data-type="risk">
			<?=$usum["risk"]?>
			</a> / 
			<a href="#" class="table_link" data-type="risk_no">	
			<?if (($usum["tot"]-$usum["risk"]) > 10){?><font color="red"><?}?>			
				<?=$usum["tot"]-$usum["risk"]?>
			<?if (($usum["tot"]-$usum["risk"]) > 10){?></font><?}?>
			</a>
			</td>
			</tr>
			<tr>
			<td><a href="#" class="table_link" data-type="del">Poistetut</a></td>
			<td><a href="#" class="table_link" data-type="del"><?=$usum["del"]?></a></td>
			</tr>
			<tr>
			<td><a href="#" class="table_link" data-type="wait">Käsittelyä odottavat</a></td>
			<td><a href="#" class="table_link" data-type="wait"><?=$usum["unassigned"]?></a></td>
			</tr>
			<tr>
			<td><a href="#" class="table_link" data-type="kk">Käyttökiellossa</a></td>
			<td><a href="#" class="table_link" data-type="kk"><?=$usum["kk"]?></a></td>
			</tr>
			<tr>
			<td><a href="#" class="table_link" data-type="open_jobs_all">Avoimet laitetapahtumat</a></td>
			<td><a href="#" class="table_link" data-type="open_jobs_all"><?=$usum["lt_all_count"]?></a></td>
			</tr>
			<tr>
			<td colspan="2">&nbsp;</td>
			</tr>
			<!--tr>
			<td>Avoimet Laitetapahtumat</td>
			<td><?=$usum["all_open_jobs"]?></td>
			</tr-->
			<tr>
			<td>Erityisluokkalaitteet</td>
			<td>
            <? if (isset($usum["special"])){ ?>
					<table width="500px">
						<!--tr>
						<td><a href="#" class="table_link" data-type="ell">Yhteensä</a></td>
						<td><a href="#" class="table_link" data-type="ell"><?=$usum["special_tot"]?></a></td>
						</tr>
						<tr>
						<td><a href="#" class="table_link" data-type="ell_lo">Laiteohjelma löytyy</a></td>
						<td><a href="#" class="table_link" data-type="ell_lo"><?=$usum["lo_count"]?></a></td>
						</tr>
						<tr>
						<td><a href="#" class="table_link" data-type="ell_no_lo">Laiteohjelma puuttuu</a></td>
						<td><a href="#" class="table_link" data-type="ell_no_lo"><?=$usum["no_lo_count"]?></a></td>
						</tr>
						
						<tr>
						<td colspan="2">&nbsp;</td>
						</tr-->
						<? $tot_e=0; ?>
						<tr>
						<th>Tyyppi</th><th>lkm</th>
						</tr>
						<? foreach ($usum["special"] as $k => $v){ ?>			
							<tr>
							<td><a href="#" class="table_link" data-type="u_special_<?=$k?>"><?=$k?></a></td><td><?=$v?></td>
							</tr>
						<? $tot_e=$tot_e+$v}?>
						<tr>
						<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
						<td><a href="#" class="table_link" data-type="open_jobs">Avoimet Laitetapahtumat</a></td>
						<td><a href="#" class="table_link" data-type="open_jobs"><?=$usum["open_jobs"]?></a></td>
						</tr>

					</table>
				<? }else {?>
					<i>Ei laitteita</i>
				<? }?>
				
			</td>
			</tr>
	</table>
	
        <? else: ?>
            <p><i>Vastuullasi ei ole laitteita</i></p>
        <? endif; ?>	
</div>
<br>
        <? if ($isSuperior): ?>	
    <h5 class="handle">
	<img onclick="show_hide('sup_table');show_hide('sup_desc');show_hide('sup_asc');" id="sup_desc" src="<?= $br ?>images/sort_desc.png">
	<img onclick="show_hide('sup_table');show_hide('sup_desc');show_hide('sup_asc');" id="sup_asc" style="display:none" src="<?= $br ?>images/sort_asc.png"> 
Alaistesi vastuulla olevat laitteet 	
	</h5>
<div id="sup_table" style="display:none">

	<table class="summary_table">
			<tr>
			<td width="50"><a href="#" class="table_link" data-type="gr_tot">Alaistesi vastuulla olevat tutkimuslaitteet</a></td>
			<td><a href="#" class="table_link" data-type="gr_tot"><?=$group_usum["tot"]?></a></td>
			</tr>
			<tr>
			<td>Riskitarkastelu tehty / ei tehty</td>
			<td><?if (($group_usum["tot"]-$group_usum["risk"]) > 10){?><font color="red"><?}?>
			<a href="#" class="table_link" data-type="gr_risk">
			<?=$group_usum["risk"]?>
			</a>
			<?if (($group_usum["tot"]-$group_usum["risk"]) > 10){?></font><?}?> / 
			<a href="#" class="table_link" data-type="gr_risk_no">			
			<?=$group_usum["tot"]-$group_usum["risk"]?>
			</a>
			</td>
			</tr>

		    <? if ($group_usum["tot_not_in_service_count"]>0): ?>
			<tr>
			<td><a href="#" class="table_link" data-type="dev_rem"><font color="red">Ilman vastuuhenkilöä olevat laitteet</font></a></td>
			<td><a href="#" class="table_link" data-type="dev_rem"><font color="red"><?=$group_usum["tot_not_in_service_count"]?></font></a></td>
			</tr>
			<? endif; ?>
			
			<tr>
			<td><a href="#" class="table_link" data-type="gr_del">Poistetut</a></td>
			<td><a href="#" class="table_link" data-type="gr_del"><?=$group_usum["del"]?></a></td>
			</tr>
			<tr>
			<td><a href="#" class="table_link" data-type="gr_wait">Käsittelyä odottavat</a></td>
			<td><a href="#" class="table_link" data-type="gr_wait"><?=$group_usum["unassigned"]?></a></td>
			</tr>
			<tr>
			<td><a href="#" class="table_link" data-type="gr_kk">Käyttökiellossa</a></td>
			<td><a href="#" class="table_link" data-type="gr_kk"><?=$group_usum["kk"]?></a></td>
			</tr>
			<tr>
			<td><a href="#" class="table_link" data-type="gr_open_jobs_all">Avoimet laitetapahtumat</a></td>
			<td><a href="#" class="table_link" data-type="gr_open_jobs_all"><?=$group_usum["lt_all_count"]?></a></td>
			</tr>
			<tr>
			<td colspan="2">&nbsp;</td>
			</tr>
			<!--tr>
			<td>Avoimet Laitetapahtumat</td>
			<td><?=$group_usum["open_jobs"]?></td>
			</tr-->
			<tr>
			<td>Erityisluokkalaitteet</td>
			<td>
            <? if (isset($group_usum["special"])){ ?>
					<table width="500px">
						<!--tr>
						<td><a href="#" class="table_link" data-type="gr_ell">Yhteensä</a></td>
						<td><a href="#" class="table_link" data-type="gr_ell"><?=$group_usum["special_tot"]?></a></td>
						</tr>
						<tr>
						<td><a href="#" class="table_link" data-type="gr_ell_lo">Laiteohjelma löytyy</a></td>
						<td><a href="#" class="table_link" data-type="gr_ell_lo"><?=$group_usum["lo_count"]?></a></td>
						</tr>
						<tr>
						<td><a href="#" class="table_link" data-type="gr_ell_no_lo">Laiteohjelma puuttuu</a></td>
						<td><a href="#" class="table_link" data-type="gr_ell_no_lo"><?=$group_usum["no_lo_count"]?></a></td>
						</tr>
						
						<tr>
						<td colspan="2">&nbsp;</td>
						</tr-->
						<? $tot_e=0; ?>
						<tr>
						<th>Tyyppi</th><th>lkm</th>
						</tr>
						<? foreach ($group_usum["special"] as $k => $v){ ?>			
							<tr>
							<td><a href="#" class="table_link" data-type="g_special_<?=$k?>"><?=$k?></a></td><td><?=$v?></td>
							</tr>
						<? $tot_e=$tot_e+$v}?>
						<tr>
						<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
						<td><a href="#" class="table_link" data-type="gr_open_jobs">Avoimet Laitetapahtumat</a></td>
						<td><a href="#" class="table_link" data-type="gr_open_jobs"><?=$group_usum["open_jobs"]?></a></td>
						</tr>

					</table>
				<? }else {?>
					<i>Ei laitteita</i>
				<? }?>
				
			</td>
			</tr>
	</table>
	
</div>
<br>
        <? else: ?>
        <? endif; ?>
		
        <? if ($isQuality): ?>	
    <h5 class="handle">
	<img onclick="show_hide('q_table');show_hide('q_desc');show_hide('q_asc');" id="q_desc" src="<?= $br ?>images/sort_desc.png">
	<img onclick="show_hide('q_table');show_hide('q_desc');show_hide('q_asc');" id="q_asc" style="display:none" src="<?= $br ?>images/sort_asc.png"> 

	Laatuvastuunäkymä</h5>
<div id="q_table" style="display:none">

	<table class="summary_table">
			<tr>
			<td width="50"><a href="#" class="table_link" data-type="ql_tot">Vastuullasi olevat tutkimuslaitteet</a></td>
			<td><a href="#" class="table_link" data-type="ql_tot"><?=$quality_usum["tot"]?></a></td>
			</tr>
			<tr>
			<td>Riskitarkastelu tehty / ei tehty</td>
			<td><?if (($quality_usum["tot"]-$quality_usum["risk"]) > 10){?><font color="red"><?}?>
			<a href="#" class="table_link" data-type="ql_risk">
			<?=$quality_usum["risk"]?>
			</a>
			<?if (($quality_usum["tot"]-$quality_usum["risk"]) > 10){?></font><?}?> / 
			<a href="#" class="table_link" data-type="ql_risk_no">			
			<?=$quality_usum["tot"]-$quality_usum["risk"]?>
			</a>
			</td>
			</tr>
			
			<tr>
			<td><a href="#" class="table_link" data-type="ql_del">Poistetut</a></td>
			<td><a href="#" class="table_link" data-type="ql_del"><?=$quality_usum["del"]?></a></td>
			</tr>
			<tr>
			<td><a href="#" class="table_link" data-type="ql_wait">Käsittelyä odottavat</a></td>
			<td><a href="#" class="table_link" data-type="ql_wait"><?=$quality_usum["unassigned"]?></a></td>
			</tr>
			<tr>
			<td><a href="#" class="table_link" data-type="ql_kk">Käyttökiellossa</a></td>
			<td><a href="#" class="table_link" data-type="ql_kk"><?=$quality_usum["kk"]?></a></td>
			</tr>
			<tr>
			<td><a href="#" class="table_link" data-type="ql_no_lvh">Laitteet, joilla ei ole laitevastuuhenkilöä</a></td>
			<td><a href="#" class="table_link" data-type="ql_no_lvh"><?=$quality_usum["no_lvh"]?></a></td>
			</tr>
		    <? if ($quality_usum["tot_not_in_service_count"]>0): ?>
			<tr>
			<td><a href="#" class="table_link" data-type="ql_dev_rem"><font color="red">Poistuneille henkilöille osoitetut</font></a></td>
			<td><a href="#" class="table_link" data-type="ql_dev_rem"><font color="red"><?=$quality_usum["tot_not_in_service_count"]?></font></a></td>
			</tr>
			<? endif; ?>
			
		    <? if ($quality_usum["tot_unassigned_count"]>0): ?>
			<tr>
			<td><a href="#" class="table_link" data-type="ql_unass"><font color="red">Ilman laitekoordinaattoria olevat laiteympäristöt</font></a></td>
			<td><a href="#" class="table_link" data-type="ql_unass"><font color="red"><?=$quality_usum["tot_unassigned_count"]?></font></a></td>
			</tr>
			<? endif; ?>

			<tr>
			
			<tr>
			<td><a href="#" class="table_link" data-type="ql_open_jobs_all">Avoimet laitetapahtumat</a></td>
			<td><a href="#" class="table_link" data-type="ql_open_jobs_all"><?=$quality_usum["lt_all_count"]?></a></td>
			</tr>
			<tr>
			<td colspan="2">&nbsp;</td>
			</tr>
			<!--tr>
			<td>Avoimet Laitetapahtumat</td>
			<td><?=$quality_usum["open_jobs"]?></td>
			</tr-->
			<tr>
			<td>Erityisluokkalaitteet</td>
			<td>
            <? if (isset($quality_usum["special"])){ ?>
					<table width="500px">
						<!--tr>
						<td><a href="#" class="table_link" data-type="ql_ell">Yhteensä</a></td>
						<td><a href="#" class="table_link" data-type="ql_ell"><?=$quality_usum["special_tot"]?></a></td>
						</tr>
						<tr>
						<td><a href="#" class="table_link" data-type="ql_ell_lo">Laiteohjelma löytyy</a></td>
						<td><a href="#" class="table_link" data-type="ql_ell_lo"><?=$quality_usum["lo_count"]?></a></td>
						</tr>
						<tr>
						<td><a href="#" class="table_link" data-type="ql_ell_no_lo">Laiteohjelma puuttuu</a></td>
						<td><a href="#" class="table_link" data-type="ql_ell_no_lo"><?=$quality_usum["no_lo_count"]?></a></td>
						</tr>
						
						<tr>
						<td colspan="2">&nbsp;</td>
						</tr-->
						<? $tot_e=0; ?>
						<tr>
						<th>Tyyppi</th><th>lkm</th>
						</tr>
						<? foreach ($quality_usum["special"] as $k => $v){ ?>			
							<tr>
							<td><a href="#" class="table_link" data-type="q_special_<?=$k?>"><?=$k?></a></td><td><?=$v?></td>
							</tr>
						<? $tot_e=$tot_e+$v}?>
						<tr>
						<td colspan="2">&nbsp;</td>
						</tr>


						<tr>
						<td><a href="#" class="table_link" data-type="ql_open_jobs">Avoimet Laitetapahtumat</a></td>
						<td><a href="#" class="table_link" data-type="ql_open_jobs"><?=$quality_usum["open_jobs"]?></a></td>
						</tr>

					</table>
				<? }else {?>
					<i>Ei laitteita</i>
				<? }?>
				
			</td>
			</tr>
	</table>
</div>	
<br>
        <? else: ?>
        <? endif; ?>			
		
        <? if ($isCoord): ?>	
    <h5 class="handle">
	<img onclick="show_hide('c_table');show_hide('c_desc');show_hide('c_asc');" id="c_desc" src="<?= $br ?>images/sort_desc.png">
	<img onclick="show_hide('c_table');show_hide('c_desc');show_hide('c_asc');" id="c_asc" style="display:none" src="<?= $br ?>images/sort_asc.png"> 

	Laitekoordinaattorinäkymä</h5>
<div id="c_table" style="display:none">

	<table class="summary_table">
			<tr>
			<td width="50"><a href="#" class="table_link" data-type="co_tot">Vastuullasi olevat tutkimuslaitteet</a></td>
			<td><a href="#" class="table_link" data-type="co_tot"><?=$coord_usum["tot"]?></a></td>
			</tr>
			<tr>
			<td>Riskitarkastelu tehty / ei tehty</td>
			<td><?if (($coord_usum["tot"]-$coord_usum["risk"]) > 10){?><font color="red"><?}?>
			<a href="#" class="table_link" data-type="co_risk">
			<?=$coord_usum["risk"]?>
			</a>
			<?if (($coord_usum["tot"]-$coord_usum["risk"]) > 10){?></font><?}?> / 
			<a href="#" class="table_link" data-type="co_risk_no">			
			<?=$coord_usum["tot"]-$coord_usum["risk"]?>
			</a>
			</td>
			</tr>
			
			<tr>
			<td><a href="#" class="table_link" data-type="co_del">Poistetut</a></td>
			<td><a href="#" class="table_link" data-type="co_del"><?=$coord_usum["del"]?></a></td>
			</tr>
			<tr>
			<td><a href="#" class="table_link" data-type="co_wait">Käsittelyä odottavat</a></td>
			<td><a href="#" class="table_link" data-type="co_wait"><?=$coord_usum["unassigned"]?></a></td>
			</tr>
			<tr>
			<td><a href="#" class="table_link" data-type="co_kk">Käyttökiellossa</a></td>
			<td><a href="#" class="table_link" data-type="co_kk"><?=$coord_usum["kk"]?></a></td>
			</tr>
			<tr>
			<td><a href="#" class="table_link" data-type="co_no_lvh">Laitteet, joilla ei ole laitevastuuhenkilöä</a></td>
			<td><a href="#" class="table_link" data-type="co_no_lvh"><?=$coord_usum["no_lvh"]?></a></td>
			</tr>
			
			
			<tr>
			<td><a href="#" class="table_link" data-type="co_open_jobs_all">Avoimet laitetapahtumat</a></td>
			<td><a href="#" class="table_link" data-type="co_open_jobs_all"><?=$coord_usum["lt_all_count"]?></a></td>
			</tr>
			<tr>
			<td colspan="2">&nbsp;</td>
			</tr>
			<!--tr>
			<td>Avoimet Laitetapahtumat</td>
			<td><?=$coord_usum["open_jobs"]?></td>
			</tr-->
			<tr>
			<td>Erityisluokkalaitteet</td>
			<td>
            <? if (isset($coord_usum["special"])){ ?>
					<table width="500px">
						<!--tr>
						<td><a href="#" class="table_link" data-type="co_ell">Yhteensä</a></td>
						<td><a href="#" class="table_link" data-type="co_ell"><?=$coord_usum["special_tot"]?></a></td>
						</tr>
						<tr>
						<td><a href="#" class="table_link" data-type="co_ell_lo">Laiteohjelma löytyy</a></td>
						<td><a href="#" class="table_link" data-type="co_ell_lo"><?=$coord_usum["lo_count"]?></a></td>
						</tr>
						<tr>
						<td><a href="#" class="table_link" data-type="co_ell_no_lo">Laiteohjelma puuttuu</a></td>
						<td><a href="#" class="table_link" data-type="co_ell_no_lo"><?=$coord_usum["no_lo_count"]?></a></td>
						</tr>
						
						<tr>
						<td colspan="2">&nbsp;</td>
						</tr-->
						<? $tot_e=0; ?>
						<tr>
						<th>Tyyppi</th><th>lkm</th>
						</tr>
						<? foreach ($coord_usum["special"] as $k => $v){ ?>			
							<tr>
							<td><a href="#" class="table_link" data-type="c_special_<?=$k?>"><?=$k?></a></td><td><?=$v?></td>
							</tr>
						<? $tot_e=$tot_e+$v}?>
						<tr>
						<td colspan="2">&nbsp;</td>
						</tr>


						<!--tr>
						<th>Tyyppi</th><th>lkm</th>
						</tr>
						<? foreach ($coord_usum["special"] as $k => $v){ ?>			
							<tr>
							<td><?=$k?></td><td><?=$v?></td>
							</tr>
						<? $tot_e=$tot_e+$v}?>
						<tr>
						<td colspan="2">&nbsp;</td>
						</tr-->
						<tr>
						<td><a href="#" class="table_link" data-type="co_open_jobs">Avoimet Laitetapahtumat</a></td>
						<td><a href="#" class="table_link" data-type="co_open_jobs"><?=$coord_usum["open_jobs"]?></a></td>
						</tr>

					</table>
				<? }else {?>
					<i>Ei laitteita</i>
				<? }?>
				
			</td>
			</tr>
	</table>
</div>
        <? else: ?>
        <? endif; ?>			

	<table class="summary_table" border="1">
			<tr>
			<td colspan="3" width="100">

				<a href="#" onclick="hideAll();" class="all_table_link" data-type="browse_window">Tutkimuslaitehaku</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<!-- br><a href="<?=site_url('tlr/all_devices')?>" class="link">Tutkimuslaitehaku2</a -->	
			</td>
			</tr>

			<tr>
			<td width="100">

				<a href="#" class="forsale_table_link" onclick="hideAll();" data-type="browse_window">Tarjolla olevat laitteet</a>

			</td>
			</tr>
	</table>
<!-- br><a href="<?=site_url('tlr/forsale_devices')?>" class="link">Tutkimuslaitehaku2</a -->	

<br>		
<h6><a href="<?=site_url('tlr')?>">Etusivulle</a></h6>


	</div>
<div class="widget g9" id="results">
<h3>TLR-TUTKIMUSLAITERAPORTIT</h3>
<br>
<p>TLR-Tutkimuslaiteraportit on reaaliaikainen ote VTT Groupin käytössä olevan TLR-tutkimuslaiterekisterin laitetiedoista.</p>
<p>TLR-Tutkimuslaiteraportit sisältää yhteiset näkymät, joiden sisältö määräytyy käyttäjäryhmän perusteella:</p>
<li>Laitevastuuhenkilö – Näkee laitteet, joiden laitevastuuhenkilöksi hänet on nimetty tutkimuslaiterekisterissä</li>
<li>Esimies – Näkee laitteet, joiden laitevastuuhenkilöksi hänen alaisensa on nimetty tutkimuslaiterekisterissä</li>
<li>Laatuvastuuhenkilö – Näkee laitteet niiden laiteympäristöjen osalta, joiden laatuvastuuhenkilöksi hänet on nimetty tutkimuslaiterekisterissä</li>
<li>Laitekoordinaattori - Näkee laitteet niiden laiteympäristöjen osalta, joiden laitekoordinaattoriksi hänet on nimetty tutkimuslaiterekisterissä</li>
<br>
<p>Koska Laitevastuuhenkilö-, Esimies-, Laatuvastuuhenkilö- ja Laitekoordinaattori-raportit sisältävät mahdollisesti paljon tuloksia, on ne toteutettu<br>
välimuistitoteutuksella, joka päivittää aineiston vuorokauden sisällä Efecten päivityksestä. Jos haluat manuaalisesti tyhjentää välimuistin, voit<br>
tehdä sen <a href="<?=site_url('tlr/clearcache')?>">tästä</a></p>

<!--li>Laitelista</li>
<li>Riskitarkastelu tehty/ei tehty</li>
<li>Käytöstä poistetut laitteet 12 kk ajalta</li>
<li>Laitelistasta puuttuvat uudet laitteet</li>
<li>Käyttökiellossa olevat laitteet</li>
<li>Ilman vastuuhenkilöä olevat laitteet</li>
<li>Laiteylläpidon avoimet laitetapahtumat</li>
<br-->
<p>Työturvallisuuden ja viranomaisvaatimusten kannalta erityisluokkiin kuuluvat laitteet on listattu erikseen.</p>

<table class="home_table">
<tr><td>Laitelista</td><td>Laitteet, jotka on haettu tutkimuslaiterekisteristä käyttäjäryhmäsi perusteella</td></tr>
<tr><td>Riskitarkastelu tehty/ei tehty</td><td>Laiteet, joiden riskitarkastelu on kirjattu / kirjaamatta tutkimuslaiterekisteriin</td></tr>
<tr><td>Käytöstä poistetut laitteet 12 kk ajalta</td><td>Laitteet, joiden tilaksi tutkimuslaiterekisterissä on merkitty Poistettu viimeisen 12 kk aikana. Laitteet on romutettu tai myyty ja laitekortit arkistoitu.</td></tr>
<tr><td>Laitelistasta puuttuvat uudet laitteet</td><td>Uudet laitteet –kansiossa olevat laitteet, joiden tilaksi tutkimuslaiterekisterissä on merkitty Käyttöönotto kesken. Laitetiedot tulee täydentää  ja laitekortti siirtää oikeaan laiteympäristö-kansioon. Käyttöönotto odottaa toimenpiteitä laitevastuuhenkilöltä ja laitekoordinaattorilta.</td></tr>
<tr><td>Käyttökiellossa olevat laitteet</td><td>Laitteet, joiden tilaksi tutkimuslaiterekisterissä on merkitty Käyttökiellossa.</td></tr>
<tr><td>Ilman vastuuhenkilöä olevat laitteet</td><td>Laitteet, joiden laitevastuuhenkilöksi merkityn henkilön työsuhde on päättynyt tai laitteelle ei ole merkitty laitevastuuhenkilöä. Laitekoordinaattori päivittää laitteille uuden vastuuhenkilön laitevastuuhenkilön esimiehen ohjeiden mukaisesti.</td></tr>
<tr><td>Laiteylläpidon avoimet laitetapahtumat</td><td>Laitteet, joille on määritetty määrävälein tehtävä ylläpitotapahtuma (kalibrointi, tarkastus, huolto) ja järjestelmä on luonut tapahtuman kirjaamista varten laitetapahtumakortin. Laitetapahtumakortit odottavat tapahtuman päivittämistä, vähintäänkin kirjattava: tekijä, toteumapäivä, tapahtuman tila = Suljettu.</td></tr>

<tr><td>Tutkimuslaitehaku</td><td>Tutkimuslaitehaku on käytettävissä kaikilla VTT Groupin henkilöillä. Haulla löytyvät VTT Groupin tutkimuslaiterekisterin laitteiden perustiedot. </td></tr>

<tr><td>Tarjolla olevat laitteet</td><td>Tarjolla olevat laitteet on listaus tutkimuslaiterekisterin laitteista, jotka ovat VTT:llä sisäisesti tarjolla. Tapauksesta riippuen laitteet otetaan käyttöön toisaalla VTT:llä, myydään tai romutetaan. 
</td></tr>



</table>


	<!--table class="result_table">
			<tr>
			<th>Vastuullasi olevat tutkimuslaitteet</th>
			<td><?=$usum["tot"]?></td>
			</tr>
			<tr>
			<th>Riskitarkastelu tehty</th>
			<td><?if (($usum["tot"]-$usum["risk"]) > 10){?><font color="red"><?}?><?=$usum["risk"]?><?if (($usum["tot"]-$usum["risk"]) > 10){?></font><?}?> / <?=$usum["tot"]?></td>
			</tr>
			<tr>
			<th>Poistetut</th>
			<td><?=$usum["del"]?></td>
			</tr>
			<tr>
			<th>Käsittelyä odottavat</th>
			<td><?=$usum["unassigned"]?></td>
			</tr>
			<tr>
			<th>Käyttökiellossa</th>
			<td><?=$usum["kk"]?></td>
			</tr>
			<tr>
			<th>Avoimet Laitetapahtumat</th>
			<td><?=$usum["open_jobs"]?></td>
			</tr>
			<tr>
			<th>Erityisluokkalaitteet</th>
			<td>
            <? if (isset($usum["special"])){ ?>
					<table width="500px">
						<tr>
						<td><b>Yhteensä</b></td><td><b><?=$usum["special_tot"]?></b></td>
						</tr>
						<tr>
						<td colspan="2">&nbsp;</td>
						</tr>
						
						<? $tot_e=0; ?>
						<tr>
						<th>Tyyppi</th><th>lkm</th>
						</tr>
						<? foreach ($usum["special"] as $k => $v){ ?>			
							<tr>
							<td><?=$k?></td><td><?=$v?></td>
							</tr>
						<? $tot_e=$tot_e+$v}?>

					</table>
				<? }else {?>
					<i>Ei laitteita</i>
				<? }?>
				
			</td>
			</tr>
</table-->
</div>

<?
//LaiteID, tila, laitevastuuhlö, laitteen nimi, kategoria, valmistaja, Laiteympöaristö, laitekoordinaattori(laiteympäristön takaa), Viranomaisen reksiterinumero, Erityisluokka,
// rakennus, huone, laiteohjelmat, Laiteohjelma / Laitetapahtumat
?>
	<Br style="clear:left">		
	<!--/div-->
</div>
<script type="text/javascript">
$(function() {
	$(".all_table_link").on('click', function() {
		$("#results").html('<table class="dtable" id="dtable"><thead><tr><th>Organisaatio</th><th>Kategoria</th><th>LaiteID</th><th>Laitteen nimi</th><th>Laitteen malli</th><th>Valmistaja</th><th>Käyttötarkoitus</th><th>Laitteen mittakaava</th><th>Avainsanat</th><th>Tekniset tiedot</th><th>Laitteen tila</th><th>Rakennus</th><th>Laiteympäristö</th><th>Laitevastuuhenkilö(t)</th></tr></thead><tfoot><tr><th>Organisaatio</th><th>Kategoria</th><th>LaiteID</th><th>Laitteen malli</th><th>Laitteen nimi</th><th>Valmistaja</th><th>Käyttötarkoitus</th><th>Laitteen mittakaava</th><th>Avainsanat</th><th>Tekniset tiedot</th><th>Laitteen tila</th><th>Rakennus</th><th>Laiteympäristö</th><th>Laitevastuuhenkilö(t)</th></tr></tfoot></table>');

		$('.summary_table tr').removeClass('selected_row');
		$('.summary_table td').removeClass('selected_row');
		$(this).parent().parent().addClass('selected_row');
		
		var table = $("#dtable").dataTable({
			"sScrollX": "100%",
			"sScrollY": "100%",
			"ajax": "<?= site_url('ajax/tlr_all_devices') ?>",
			"columns": [
				{ "data": "tlr_report_org" },
				{ "data": "rd_category" },
				{ "data": "rd_id" },
				{ "data": "rd_name" },
				{ "data": "rd_model" },
				{ "data": "rd_manufacturer" },
				{ "data": "rd_usage" },
				{ "data": "rd_scale" },
				{ "data": "rd_keywords" },
				{ "data": "rd_technical_info" },
				{ "data": "rd_status" },
				{ "data": "rd_building" },
				{ "data": "rd_device_environment" },
				{ "data": "tlr_report_lvh" }
			],
			"lengthMenu": [ 10, 25, 50, 100, 200, 500 ],
		
			"dom": '<"filters">Tirft<"clear"><"bottom"lp>',
			buttons: [
				{
					extend: 'colvis',
					collectionLayout: 'fixed two-column'
				}
			],
			"tableTools": {
				"sSwfPath": "<?=base_url()?>resources/swf/copy_csv_xls_pdf.swf",
				"sRowSelect": "multi",
				"aButtons": [
					{
						"sExtends": "csv",
						"bSelectedOnly": true,
						"sCharSet": "utf16le",
						"fnComplete": function ( nButton, oConfig, oFlash, sFlash ) {
							var oTT = TableTools.fnGetInstance( 'dtable' );
							var nRow = $('#dtable tbody tr');
							oTT.fnDeselect(nRow);
						}
					}
				
				]
			},
			"deferRender": true,
			initComplete: function () {
				var count = 1;
				this.api().columns().every( function () {
					var column = this;
					if(count % 4 == 0){ var br = "<br>"; }else { var br = "";}
					//console.log(column.header());
					if ($.inArray(column.index(), [2, 3, 4, 6, 8, 9, 13]) > 0) return;
					var select = $('<select style="margin-right:5px;margin-top:5px;width:200px;"><option value="">'+$(column.header()).text()+'</option></select>'+br)
						.appendTo( $(".filters") )
						.on( 'change', function () {
							var val = $.fn.dataTable.util.escapeRegex(
								$(this).val()
							);
 
							column
								.search( val ? '^'+val+'$' : '', true, false )
								.draw();
						} );
 
					column.data().unique().sort().each( function ( d, j ) {
						select.append( '<option value="'+d+'">'+d+'</option>' )
					} );
					count++;
				} );
			},
			"language": { "loadingRecords": "Aineistoa haetaan. Odota hetki... <img src='<?=base_url()?>/resources/img/ajax-loader.gif\'>" }
		});

	    $('a.DTTT_button_csv').mousedown(function(){
			var oTT = TableTools.fnGetInstance( 'dtable' );
			var nRow = $('#dtable tbody tr');
			oTT.fnSelect(nRow);
		});
		
		/* temporary removed $('#dtable tfoot th').each(function () {
			var title = $('#dtable thead th').eq( $(this).index() ).text();
			$(this).html('<input type="text" placeholder="Filter by '+title+'" />' );
		});*/
		
		// Apply the search
		table.columns().every( function () {
			var that = this;
 
			$( 'input', this.footer() ).on( 'keyup change', function () {
				that
					.search( this.value )
					.draw();
			});
			
		});


	});
});
$(document).ready(function() {
    $('#xtable').dataTable({
		dom: 'T<"clear">lfrtip',
		tableTools: {
            "sSwfPath": "<?=base_url()?>resources/swf/copy_csv_xls_pdf.swf"
        }
		}	
	);} 
	);
$(function() {
	$(".table_link").on('click', function() {
		$("#results").html("Aineistoa haetaan. Odota hetki...<img src='<?=base_url()?>/resources/img/ajax-loader.gif\'>");
		$.get("<?=site_url('ajax/tlr_default_report')?>", {type: $(this).data('type')}, function(data) {
			$("#results").html(data);
		});
		$('.browse_table tr').removeClass('selected_row');
		$('.browse_table td').removeClass('selected_row');

		$('.summary_table tr').removeClass('selected_row');
		$('.summary_table td').removeClass('selected_row');
		$(this).parent().parent().addClass('selected_row');

	});
});

$(function() {
	$(".forsale_table_link").on('click', function() {
		$("#results").html("Aineistoa haetaan. Odota hetki...<img src='<?=base_url()?>/resources/img/ajax-loader.gif\'>");
		$.get("<?=site_url('ajax/tlr_forsale_devices')?>", {type: $(this).data('type')}, function(data) {
			$("#results").html(data);
		});
		$('.summary_table tr').removeClass('selected_row');
		$('.summary_table td').removeClass('selected_row');
		$(this).parent().parent().addClass('selected_row');

	});
});


$(document).ready(function() {
    $('#table').dataTable({
		"oLanguage": {
			"sUrl": "<?=base_url()?>resources/js/finnish.txt"
			} 
		}	
	);} 
	);
</script>