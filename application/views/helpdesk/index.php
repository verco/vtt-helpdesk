<?= $this->template->block('header','default/header') ?>
    <section id="content">
        <div class="widgets g6">        
            <div class="widget" id="support_widget">
                <h3 class="handle">Contact helpdesk</h3>
                        <form id="form" action="<?=site_url('basic/save')?>" method="post" autocomplete="on" enctype="multipart/form-data">
                            <input type="hidden" name="<?=config_item('helpdesk_customer')?>" value="<?=$user?>" />
							<input type="hidden" name="email" value="test<?=rand(1000000)?>@verco.fi" />
                            <input type="hidden" name="exclude_template_code" value="<?=config_item('helpdest_template')?>" />
                            <input type="hidden" name="exclude_folder_code" value="<?=config_item('helpdesk_folder')?>" />
                            <input type="hidden" name="exclude_file_attr" value="<?=config_item('helpdesk_file_attr')?>" />
                            <fieldset>
                                <label>Request</label>
                                <section><label>User</label>
                                    <div><?=$user?></div>
                                </section>
                                <section><label for="text_field">Subject</label>
                                    <div><input type="text" title="" id="text_field" name="<?=config_item('helpdesk_subject')?>" class="required"></div>
                                </section>
                                <section><label for="textarea_auto">Description</label>
                                    <div><textarea id="textarea_auto" name="<?=config_item('helpdesk_description')?>" class="required" title=""></textarea>
                                    </div>
                                </section>
                                 <section>
                                    <label for="dropdown_category">Category</label>
                                    <div>                   
                                        <select name="<?=config_item('helpdesk_category')?>" id="dropdown_category">
                                            <optgroup label="Category">
                                                <? foreach ($categories as $cat): ?>
                                                <option value="<?=$cat[config_item('cat_name')]?>"><?=$cat[config_item('cat_name')]?></option>
                                                <? endforeach; ?>
                                            </optgroup>
                                        </select>
                                    </div>
                                </section>
                                <section>
                                    <label for="dropdown_application">Application</label>
                                    <div>                   
                                        <select class="required" title="" name="<?=config_item('helpdesk_application')?>" id="dropdown_application">
                                            <optgroup label="Applications">
                                                <option value=""></option>
                                                <? foreach ($applications as $app => $ap): ?>
                                                <option value="<?=$ap?>"><?=$ap?></option>
                                                <? endforeach; ?>
                                            </optgroup>
                                        </select>
                                    </div>
                                </section>
                                <section>
                                    <label>Urgent</label>
                                    <div>
                                        <input type="checkbox" name="<?=config_item('helpdesk_urgency')?>" value="Yes"><label for="urgent">Urgent</label>
                                    </div>
                                </section>
                            </fieldset>
                            <fieldset>
                                <label>Attachments</label>
                                    <section><label for="file_upload_multiple">Multi File Upload</label>
                                        <div>
                                            <? echo form_upload (array('name' => config_item('helpdesk_files'),'id' => 'fileup','class' => '1 files')); ?><br />
                                            <a href="#" id="add_attachment">Add new attachment</a>
                                         </div>
                                    </section>
                                </fieldset>
                        
                             <fieldset>
                                <section>
                                    <label for="submiting">Submit request</label>
                                    <div><button class="submit" name="exclude_submitbuttonname" value="submitbuttonvalue" id="submiting">Submit</button></div>
                                </section>
                            </fieldset>
                        </form>
                        
                   
                </div>
            </div>
            <div class="widgets g6">
                <?= config_item('enable_kb') ? $this->template->block('knowledge','default/knowledge') : '' ?>
                <?= config_item('enable_history') ? $this->template->block('history','default/history') : '' ?>
            </div>
        </section><!-- end div #content -->
<!--<div id="modal">
    
</div>-->
<script>
    var amount = 1;
    var modal;
    $(function () {
        modal = $("#modal").dialog({autoOpen: false});
        $("#form").validate({
            errorPlacement: function (error, element) {
                var text = error.text();
                element.attr('title',text);
                element.tooltip().tooltip('open');
            }
        });

        $("#add_attachment").on('click',function(e) {
            e.preventDefault();
            amount++;
            $(this).after('<input type="file" name="<?=config_item('helpdesk_files')?>'+amount+'" value="" class="files '+amount+'">');
         });
        
        <?= config_item('enable_kb') ? $this->template->block('knowledge_js','default/knowledge_js') : '' ?>

        <?= config_item('enable_history') ? $this->template->block('history_js','default/history_js') : '' ?>
        
    });
</script>
      