<div class="widget" id="last_widget">
    <h3 class="handle">Last service requests</h3>
    <div id="requests">
        <? if ($servrequests): ?>
            <? $i = 0 ?>
            <? foreach ($servrequests as $req): ?>
                <div class="<?=$req->entityid?>">
                    <h3>Request type: <?=$req[config_item('request_type')]?></h3>
                    <h4>Subject: <?=$req[config_item('request_subject')]?></h4>
                    <p>Description: <?=$req[config_item('request_description')]?></p>
                    <hr />
                </div>  
            <? if ($i == 4) break; ?>
            <? $i++ ?>                             
            <? endforeach; ?>
        <? else: ?>
            <h4>No previous requests found</h4>
        <? endif; ?>
    </div>
</div>
<!--<div id="modal">
</div>-->
<script type="text/javascript">
$(function() {
        var modal = $("#modal").dialog({ autoOpen: false})
        <?= config_item('enable_history') ? $this->template->block('history_js','default/history_js') : '' ?>
});

</script>