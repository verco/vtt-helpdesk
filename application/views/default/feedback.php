<div class="hidden" id="feedback_modal">
	<h2>Give us feedback on our service.</h2>
	<form id="feedback_form" action="<?=site_url('ajax/insert_feedback')?>" method="post" accept-charset="UTF-8">
		<input type="hidden" id="<?=config_item('fscfeedback_fsc_request')?>" name="<?=config_item('fscfeedback_fsc_request')?>" value="">
		<? 
		$i = 1;
		foreach ($feedbacks as $feedback): ?>
		
		<? if (is_numeric(substr($feedback->string, 0,1)) && strpos(current_url(), 'myrequests/sho') > 0): ?>
		<p><?=$feedback->string?></p>
		<label for="q<?=$i?>a1">1.</label><input type="radio" value="1" name="<?=config_item("fscfeedback_question$i")?>" id="q<?=$i?>a1">&nbsp;
		<label for="q<?=$i?>a2">2.</label><input type="radio" value="2" name="<?=config_item("fscfeedback_question$i")?>" id="q<?=$i?>a2">&nbsp;
		<label for="q<?=$i?>a3">3.</label><input type="radio" value="3" name="<?=config_item("fscfeedback_question$i")?>" id="q<?=$i?>a3">&nbsp;
		<label for="q<?=$i?>a4">4.</label><input type="radio" value="4" name="<?=config_item("fscfeedback_question$i")?>" id="q<?=$i?>a4">&nbsp;
		<? elseif (! is_numeric(substr($feedback->string, 0,1))): ?>
		<p><?=$feedback->string?></p>
		<textarea rows="8" name="<?=config_item("fscfeedback_open_text")?>"></textarea>
		<? endif;
		$i++;
		endforeach; ?>
	</form>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$("#feedback_modal").dialog({
			modal: true,
			width: 400,
			autoOpen: false,
			buttons: {
				"Submit feedback": function (e) {
					$(this).attr('disabled','disabled');
					$.post($("#feedback_form").attr('action'),$("#feedback_form").serialize(), function(data) {
						data = data + '<br /> <br />Window closing automaticly after 3 seconds';
						$("#feedback_modal").html(data);
						setTimeout(function() {
							$("#feedback_modal").dialog('close');
							},3000
						);
					});
				}
			}
		});
	});
</script>