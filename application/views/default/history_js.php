$("#requests div").on('click',function(e) {
            modal.dialog('open');
            $.post('<?=site_url("ajax/search_request")?>',{entityid: $(this).attr('class') }, function(data) {
                modal.empty();
                modal.html(data);
                if (data.indexOf('<?=config_item("helpdesk_close")?>') == -1) {
                    modal.append('Close reason:<br /><textarea id="reason"></textarea>');
                    modal.dialog({
                        buttons: {"Close request": function() {
                                $.post('<?=site_url("ajax/save_close")?>',{incidentStatus: '<?=config_item("helpdesk_close")?>',resolution: $("#reason").val(), efecteid:$("#efecteid").attr('title')}, function (data) {
                                    modal.dialog('close');
                                }, 'html');
                            },
                        }
                    });
                } else {
                   // $(".ui-dialog-buttonset").hide();
                }
                
            });
        });