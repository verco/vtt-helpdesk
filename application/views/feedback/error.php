<? if($accepted_ok === false): ?>
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"><?= lang('error_title') ?></h3>
        </div>
        <div class="panel-body">
            <span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span> <?= lang('error_text') ?>
        </div>
    </div>
</div>
<? else: ?>
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <? if($type === 'sr'): ?>
                <h3 class="panel-title"><?= lang('accepted_title') ?></h3>
            <? else: ?> 
                <h3 class="panel-title"><?= lang('accepted_title_inc') ?></h3>
            <?endif; ?>
        </div>
        <div class="panel-body" style="color:red; font-weight:bold">
            <? if($type === 'sr'): ?>
                <span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span> <?= lang('accepted_text') ?>
            <? else: ?>
                <span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span> <?= lang('accepted_text_inc') ?>
            <?endif; ?>    
        </div>
    </div>
</div>
<?endif; ?>