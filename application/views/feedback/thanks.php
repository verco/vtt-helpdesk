<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"><?= lang('thanks_title') ?></h3>
        </div>
        <div class="panel-body">
             <p><?= lang('thanks_text') ?> </p>
        </div>
    </div>
</div>
