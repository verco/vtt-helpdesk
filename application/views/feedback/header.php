<? $b = base_url(); $br = $b . 'resources/'; ?>
<? if (isset($announcement['text']) && $announcement['text'] != ''): ?>
    <div class="announcement center">
        <?= $announcement['text']; ?>
    </div>
<? endif; ?>
<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="row">
            <div class="col-md-4 navbar-header">
                <a href="http://vtt.fi" class="navbar-brand" style="padding-top:11px"><img src="<?= $br ?>img/VTT_Orange_Logo.png" height="40px" alt="VTT"></a>
            </div>
            <? /*<? if (true || isset($user)): ?>
                <p class="navbar-right navbar-text"><?= $this->session->userdata('user')['contact_full_name'] ?>
                <? /* | <a href="<?= site_url('logout') ?>" class="navbar-link">Log out</a></p> */ ?>
            <? /*endif; */ ?> 
            <div class="col-md-4">
                <h1 class="h2 navbar-text"><?= lang('feedback_title') ?></h1>
                
            </div>
            <div class="col-md-4">
                <h1 class="h2 navbar-text pull-right" style="white-space: nowrap;"><?= lang('feedback_title2') ?></h1>
            </div>
        </div>
    </div>
</nav>
