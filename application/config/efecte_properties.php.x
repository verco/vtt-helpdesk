<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//TEMP USER
//$config['temp_user'] = 'BELUML';
//$config['temp_user'] = 'RTEVJS';
//$config['temp_user'] = 'PVPASI';//KK
//$config['temp_user'] = 'PROKFH';//poistuneiden esimies
//$config['temp_user'] = 'BELHHE';
//$config['temp_user'] = 'PROPVT';
//$config['temp_user'] = 'BELHMR';//tupla
//$config['temp_user'] = 'MHMIKAH'; //PROPVT esimies vanha
//$config['temp_user'] = 'PROATL'; //PROPVT esimies
//$config['temp_user'] = 'AKARI';//Ari Koskinen 
//--$config['temp_user'] = 'PROTVA';//Timo Vanttola 
//$config['temp_user'] = 'ELETPS';//Timo Salo
//$config['temp_user'] = 'THELAJ'; //MHMIKAH esimies teknologiapäällikkö
//$config['temp_user'] = 'TTELEP'; //Laitekoordinaattori
//$config['temp_user'] = 'BELKHS'; //laatuvastaava - paljon
//$config['temp_user'] = 'PROPJS';
//$config['temp_user'] = 'PROPJP'; //käyttökielto
//$config['temp_user'] = 'pvpasi'; //erityisluokka
//
$config['temp_user'] = 'konpth'; //Pasi Hopia


// General settings
$config['enable_helpdesk'] = false;
$config['enable_services'] = false;
$config['enable_kb'] = false;
$config['enable_history'] = true;
//$config['enable_device_report'] = true;

// TLR Access log
$config['tlr_access_log_template'] = 'tlr_access_log';
$config['tlr_access_log_code'] = 'tlr_access_logs';
$config['tlr_access_log_person'] = 'tlr_access_person';
$config['tlr_access_log_date'] = 'tlr_access_date';
$config['tlr_access_log_index'] = 'tlr_access_index';

// Template request process

$config['process_template'] = 'request_process';
$config['process_name'] = 'process_name';
$config['process_desc'] = 'process_description';
$config['process_subcat'] = 'subcategories';
$config['process_show_fields'] = array('entityid','process_name', 'process_description','types');

// Template request type 

$config['type_template'] = 'request_type';
$config['type_customer'] = 'customer';
$config['type_name'] = 'category_name';
$config['type_desc'] = 'category_description';
$config['type_parent'] = 'parent_category';
$config['type_documents'] = 'document_templates';
$config['type_origin'] = 'request_origin';

$config['type_origin_web'] = 'Web form initiated';

$config['type_flow_approval_needed'] = 'Approval needed';
$config['type_flow_QD'] = 'Q/D';
$config['type_flow_review_and_approval'] = 'Review and Approval needed';
$config['type_flow_task_support'] = 'Task Support';

$config['type_flow_required_company'] = 'company_required';
$config['type_flow_required_value'] = 'Yes';

$config['type_site_required'] = 'site_required';
$config['type_site_default_value'] = 'Yes';

$config['type_show_fields'] = array($config['type_name'],$config['type_desc'],$config['type_parent'],$config['type_origin']);
$config['type_show_form_fields'] = array('required_data','category_name','category_description','entityid','process_flow','company_required','site_required','prefilled_subject','prefilled_description');
$config['type_document_template_external'] = 'document_template_external';

$config['type_request_origin'] = 'request_origin';
$config['type_request_origin_web'] = 'Web form initiated';

// Tempalte: Request type fields

$config['type_fields_template'] = 'request_type_fields';
$config['type_fields_name'] = 'field_name';
$config['type_fields_id'] = 'field_id';
$config['type_fields_value'] = 'field_value';
$config['type_fields_class'] = 'field_class';
$config['type_fields_request_type'] = 'ref_request_type';
$config['type_fields_info_type'] = 'field_info_type';
$config['type_fields_type'] = 'field_type';
$config['type_fields_multivalue_values'] = 'field_multivalue_values';
$config['type_fields_info_type/general'] = 'General';
$config['type_fields_info_type/additional'] = 'Additional';

$config['type_fields_show'] = array($config['type_fields_name'], $config['type_fields_id'], $config['type_fields_value'], $config['type_fields_class'], $config['type_fields_request_type'],$config['type_fields_info_type'],$config['type_fields_type']);

// Template: Helpdesk

$config['helpdesk_template'] = 'helpdesk';
$config['helpdesk_customer'] = 'hd_user';
$config['helpdesk_subject'] = 'hd_subject';
$config['helpdesk_description'] = 'hd_desc';
$config['helpdesk_files'] = 'hd_file';
$config['helpdesk_application'] = 'hd_system';
$config['helpdesk_urgency'] = 'hd_urgent';
$config['helpdesk_category'] = 'hd_category';
$config['helpdesk_customer'] = 'hd_contact';
$config['helpdesk_created'] = 'created';

$config['helpdesk_attachments'] = 'hd_file';
$config['helpdesk_email_to_helpdesk'] = 'hd_email';

$config['helpdesk_show'] = array($config['helpdesk_template'],$config['helpdesk_subject'],$config['helpdesk_description'],$config['helpdesk_files'],$config['helpdesk_application'],$config['helpdesk_urgency'],$config['helpdesk_category'],$config['helpdesk_customer'],$config['helpdesk_created']);

$config['helpdesk_ref_show'] = array('category','contact','person');

$config['helpdesk_close'] = '9 - Incident closed'; // Closed status.. affects if the close option is showed to the user in own tickets

// Template: Request

$config['request_template'] = 'request';
$config['request_customer'] = 'customer';
$config['request_created'] = 'creation_time';
$config['request_description'] = 'description';
$config['request_subject'] = 'subject';
$config['request_type'] = 'category_name';

$config['request_show'] = array($config['request_created'],$config['request_description'],$config['request_subject'],$config['request_type']);
// Teplate: Knowledge

$config['kb_template'] = 'knowledge_base';
$config['kb_name'] = 'kb_name';
$config['kb_description'] = 'kb_description';
$config['kb_attachments'] = 'kb_attachments';
$config['kb_external_links'] = 'kb_external_links';
$config['kb_publication'] = 'kb_publication';
$config['kb_group'] = 'kb_group';

$config['kb_external'] = 'External';

$config['kb_show'] = array('kb_name','kb_description','entityid');
$config['kb_ref_show'] = array('kb_external_links');

// Template: Person

$config['person_template'] = 'client';
$config['person_email'] = 'email';
$config['person_no'] = 'person_number';
$config['person_username'] = 'username';
$config['person_manager'] = 'person_manager';

$config['person_account'] = 'person_number';
$config['person_fullname'] = 'full_name';
$config['person_title'] = 'title';

$config['person_show'] = array($config['person_account'],$config['person_fullname'],$config['person_title']);


// Tempalte: Category

$config['cat_template'] = 'category';
$config['cat_name'] = 'name';
$config['cat_level'] = 'categoryLevel';

$config['cat_show'] = array('name');

// Template pc
$config['pc_template'] = 'computer';
$config['pc_hostname'] = 'computer_name';
$config['pc_serial'] = 'entity_serial';
$config['pc_f'] = 'f_asset_number';
$config['pc_user'] = 'user';
$config['pc_model'] = 'computer_model';
$config['pc_pdate'] = 'delivery_date';
$config['pc_scandate'] = 'scanning_date';
$config['pc_billing_type'] = 'billing_type';
$config['pc_status'] = 'state';

$config['pc_show'] = array($config['pc_hostname'],$config['pc_serial'],$config['pc_f'],$config['pc_user'],$config['pc_model'],$config['pc_pdate'],$config['pc_scandate'],$config['pc_billing_type'],$config['pc_status']);

// Template display
$config['displ_template'] = 'f_display';
$config['displ_fno'] = 'f_asset_number';
$config['displ_model'] = 'f_display_model';
$config['displ_size'] = 'f_display_size';
$config['displ_user'] = 'f_entity_user_reference';
$config['displ_loc'] = 'f_location_building';
$config['displ_status'] = 'f_state';
$config['displ_pdate'] = 'display_purchase_date';

$config['displ_show'] = array($config['displ_fno'],$config['displ_model'],$config['displ_size'],$config['displ_user'],$config['displ_loc'],$config['displ_status'],$config['displ_pdate']);

// Template mobile
$config['mob_template'] = 'phones';
$config['mob_sim'] = 'puh_liit_nro';
$config['mob_model'] = 'puh_malliviite';
$config['mob_serial'] = 'puh_imei';
$config['mob_user'] = 'puh_kayt';
$config['mob_status'] = 'puh_tila';
$config['mob_pdate'] = 'puh_ostopvm';

$config['mob_show'] = array($config['mob_sim'],$config['mob_model'],$config['mob_serial'],$config['mob_user'],$config['mob_status'],$config['mob_pdate']);

// Template APK - palvelupyyntö

$config['pp_template'] = 'service_request';
$config['pp_id'] = 'sr_id';
$config['pp_client'] = 'sr_client';
$config['pp_email'] = 'sr_email';
$config['pp_nat'] = 'sr_nationality';
$config['pp_phone'] = 'sr_phone';
$config['pp_company'] = 'sr_company';
$config['pp_title'] = 'sr_title';
$config['pp_desc'] = 'sr_description';
$config['pp_files'] = 'job_attachments';
$config['pp_files_id'] = '1955';


$config['pp_show'] = array($config['pp_id'],$config['pp_client'],$config['pp_email'],$config['pp_nat'],$config['pp_phone'],$config['pp_company'],$config['pp_title'],$config['pp_desc'],$config['pp_files'] 	);
// Template TLR - Tutkimuslaiteympäristö
$config['tlr_devy_template'] = 'research_environment';
$config['tlr_devy_id'] = 'rde_unique_id';
$config['tlr_devy_name'] = 'rde_name';
$config['tlr_devy_resp'] = 'rde_on_duty';
$config['tlr_quality'] = 'rde_quality_on_duty';
$config['tlr_devy_status'] = 'rde_status';
	
$config['tlr_devy_show'] = array($config['tlr_devy_id'],$config['tlr_devy_name'],$config['tlr_devy_resp'],$config['tlr_devy_status']);

// Template TLR - Tutkimuslaite
$config['tlr_dev_template'] = 'research_device';
$config['tlr_dev_id'] = 'rd_id';
$config['tlr_dev_model'] = 'rd_model';
$config['tlr_dev_resp'] = 'rd_on_duty';
$config['tlr_dev_resp_team'] = 'rd_team';
$config['tlr_dev_name'] = 'rd_name';
$config['tlr_dev_cat'] = 'rd_category';
$config['tlr_dev_man'] = 'rd_manufacturer';
$config['tlr_dev_usage'] = 'rd_usage';
$config['tlr_dev_scale'] = 'rd_scale';
$config['tlr_dev_keyw'] = 'rd_keywords';
$config['tlr_dev_techinfo'] = 'rd_technical_info';
$config['tlr_dev_status'] = 'rd_status';
$config['tlr_dev_unit'] = 'rd_unit';
$config['tlr_dev_colgr'] = 'rd_collection_group';
$config['tlr_dev_building'] = 'rd_building';
$config['tlr_dev_room'] = 'rd_room';
$config['tlr_dev_env'] = 'rd_device_environment';
$config['tlr_dev_kk'] = 'rd_no_usage';
$config['tlr_dev_laitohj'] = 'rd_programs_br';
$config['tlr_dev_pur_date'] = 'rd_purchase_date';
$config['tlr_dev_pur_price'] = 'rd_purchase_price';
$config['tlr_dev_pur_supl'] = 'rd_vendor';
$config['tlr_dev_noshow'] = 'rd_do_not_show_in_kb';

$config['tlr_dev_status_change'] = 'rd_status_change_date';

$config['tlr_dev_kas_laitenro'] = 'rd_asset_id';
$config['tlr_dev_kas_kohdenro'] = 'rd_asset_row';

//config['tlr_dev_unit_laiteymp'] = 'rd_device_environment';
$config['tlr_dev_laitekoord'] = 'rd_device_environment';//viitteen takaa
$config['tlr_dev_laitekoordinaattori'] = 'rd_device_environment:';//viitteen takaa
$config['tlr_dev_laitekoordinaattori_ref'] = 'rd_device_environment:'.$config['tlr_devy_resp'];//viitteen takaa
$config['tlr_dev_vir_rek'] = 'rd_authority_registry_number';
$config['tlr_dev_erit_luokka'] = 'rd_special_class';
$config['tlr_dev_laiteohj'] = 'rd_authority_registry_number'; //kooste

$config['tlr_dev_forsale']='rd_forsale';
$config['tlr_dev_forsale_internal_value']='Sisäisesti';

$config['tlr_dev_free_from']='rd_usage_free_from';
$config['tlr_dev_free_until']='rd_forsale_adv_until';
$config['tlr_dev_free_desc']='rd_usage_free_desc';
$config['tlr_dev_handler']='rd_forsale_handler';


//apukentät mv-attribuuteille
$config['apu_tlr_dev_erit_luokka'] = 'tlr_report_erityisluokka';
$config['apu_tlr_dev_laitevastuuhlo'] = 'tlr_report_lvh';
$config['apu_tlr_dev_org'] = 'tlr_report_org';
$config['apu_tlr_authority_registered_person'] = 'tlr_report_virano';


$config['tlr_dev_laitetap'] = '';

$config['tlr_dev_rt_stat'] = 'rd_risk_assessment_status';
$config['tlr_dev_rt_stat_by'] = 'rd_risk_assessment_made_by';
$config['tlr_dev_rt_date'] = 'rd_risk_assessment_date';
$config['tlr_dev_rt_madeby'] = 'rd_risk_assessment_made_by';
$config['tlr_dev_rt_res'] = 'rd_risk_assessment_result';
$config['tlr_dev_rt_doc'] = 'rd_risk_assessment_document';
	
/*$config['tlr_dev_show'] = array($config['tlr_dev_id'],$config['tlr_dev_resp'],$config['tlr_dev_name'],$config['tlr_dev_cat'],$config['tlr_dev_man']
							,$config['tlr_dev_status'],$config['tlr_dev_unit'],$config['tlr_dev_colgr'],$config['tlr_dev_building'],$config['tlr_dev_room']
							,$config['tlr_dev_env'],$config['tlr_dev_env'].":rde_on_duty",$config['tlr_dev_laitekoord'],$config['tlr_dev_vir_rek'],$config['tlr_dev_erit_luokka'],$config['tlr_dev_laiteohj']
							,$config['tlr_dev_rt_stat'],$config['tlr_dev_rt_date'],$config['tlr_dev_rt_madeby'],$config['tlr_dev_rt_res'],$config['tlr_dev_rt_doc'],$config['tlr_dev_resp_team']);
*/
// $config['tlr_dev_summary_show'] = array($config['tlr_dev_id']); // 
$config['tlr_dev_summary_show'] = array($config['tlr_dev_id'],$config['tlr_dev_name'],$config['tlr_dev_cat'],
											$config['tlr_dev_status'],$config['tlr_dev_rt_stat'],$config['tlr_dev_building'],$config['tlr_dev_room'],
											$config['tlr_dev_env'],
											$config['tlr_dev_rt_res'],$config['tlr_dev_rt_date'],$config['tlr_dev_rt_doc'],$config['tlr_dev_rt_madeby'],$config['tlr_dev_colgr'],
											$config['tlr_dev_rt_stat_by'],$config['tlr_dev_status_change'],$config['tlr_dev_pur_date'],
											$config['tlr_dev_pur_price'],$config['tlr_dev_pur_supl'],$config['tlr_dev_kas_laitenro'],$config['tlr_dev_kas_kohdenro'],
											$config['tlr_dev_env'].":tlr_raportit_laitekoordinaattorit",$config['apu_tlr_dev_erit_luokka'],$config['apu_tlr_dev_laitevastuuhlo'],
											$config['apu_tlr_authority_registered_person'],
											$config['apu_tlr_dev_org']
											);//,$config['tlr_dev_laitekoord'] --$config['apu_tlr_dev_erit_luokka'],,$config['apu_tlr_dev_laitevastuuhlo'],$config['tlr_dev_resp_team'],

$config['tlr_report_show'] = array($config['apu_tlr_dev_org'],$config['tlr_dev_cat'],$config['tlr_dev_id'],
											$config['tlr_dev_model'],$config['tlr_dev_name'],$config['tlr_dev_man'],$config['tlr_dev_usage'],
											$config['tlr_dev_scale'],$config['tlr_dev_keyw'],$config['tlr_dev_techinfo'],
											$config['tlr_dev_status'],$config['tlr_dev_building'],$config['tlr_dev_env'],
											$config['apu_tlr_dev_laitevastuuhlo']
											);//,$config['tlr_dev_laitekoord'] --$config['apu_tlr_dev_erit_luokka'],,$config['apu_tlr_dev_laitevastuuhlo']
$config['tlr_forsale_show'] = array(
										$config['tlr_dev_id'],
										$config['tlr_dev_name'],
										$config['tlr_dev_cat'],
										$config['apu_tlr_dev_erit_luokka'],
										$config['tlr_dev_building'],
										$config['tlr_dev_room'],
										$config['tlr_dev_status'],
										$config['tlr_dev_env'],
									//	$config['tlr_dev_laitekoordinaattori_ref'],
										$config['apu_tlr_dev_laitevastuuhlo'],
										$config['tlr_dev_kas_kohdenro'],
										$config['tlr_dev_kas_laitenro'],
										$config['tlr_dev_pur_date'],
										$config['tlr_dev_free_from'],
										$config['tlr_dev_free_until'],
										$config['tlr_dev_handler'],
										$config['tlr_dev_free_desc']
										);
//
											
$config['tlr_dev_summary_all_show'] = array($config['tlr_dev_id'],$config['tlr_dev_name'],$config['tlr_dev_cat'],
											$config['tlr_dev_status'],$config['tlr_dev_rt_stat']
											);
$config['tlr_dev_summary_mv_show'] = array($config['tlr_dev_laitohj'],$config['tlr_dev_resp'],$config['tlr_dev_erit_luokka'],$config['tlr_dev_env'].':'.$config['tlr_devy_resp']);
											//multivaluet:,$config['tlr_dev_laitohj'],$config['tlr_dev_resp'],$config['tlr_dev_erit_luokka'],$config['tlr_dev_env'].':'.$config['tlr_devy_resp'],
											
											//,$config['tlr_dev_man'],$config['tlr_dev_usage'],$config['tlr_dev_laitohj'],,);

											
											//xx ORG $config['tlr_dev_summary_show'] = array($config['tlr_dev_id'],$config['tlr_dev_resp'],$config['tlr_dev_name'],$config['tlr_dev_cat'],$config['tlr_dev_rt_stat'],$config['tlr_dev_status'],$config['tlr_dev_man'],$config['tlr_dev_usage'],$config['tlr_dev_erit_luokka'],$config['tlr_dev_laitohj'],$config['tlr_dev_room'],$config['tlr_dev_resp_team'],$config['tlr_dev_building'],$config['tlr_dev_rt_res'],$config['tlr_dev_rt_date'],$config['tlr_dev_rt_doc'],$config['tlr_dev_rt_madeby'],$config['tlr_dev_colgr'],$config['tlr_dev_rt_stat_by'],$config['tlr_dev_laitekoord'],$config['tlr_dev_status_change'],$config['tlr_dev_pur_date'],$config['tlr_dev_env'],$config['tlr_dev_pur_price'],$config['tlr_dev_pur_supl'],$config['tlr_dev_kas_laitenro'],$config['tlr_dev_kas_kohdenro']);

/*,$config['tlr_dev_kk'],$config['tlr_dev_env'].":rde_on_duty"
							,$config['tlr_dev_status'],$config['tlr_dev_unit'],$config['tlr_dev_colgr'],$config['tlr_dev_building'],$config['tlr_dev_room']
							,$config['tlr_dev_env'],$config['tlr_dev_env'].":rde_on_duty",$config['tlr_dev_laitekoord'],$config['tlr_dev_vir_rek'],$config['tlr_dev_erit_luokka'],$config['tlr_dev_laiteohj']
							,$config['tlr_dev_rt_stat'],$config['tlr_dev_rt_date'],$config['tlr_dev_rt_madeby'],$config['tlr_dev_rt_res'],$config['tlr_dev_rt_doc']);
	*/						
							
// Template TLR - Tutkimuslaitetapahtuma
$config['tlr_deve_template'] = 'research_device_incident';
$config['tlr_deve_id'] = 'rdi_unique_id';
$config['tlr_deve_dev'] = 'rdi_device_id';
$config['tlr_deve_act_date'] = 'rdi_actual_date';
$config['tlr_deve_performer'] = 'rdi_performed_by';
$config['tlr_deve_act'] = 'rdi_activity';
$config['tlr_deve_status'] = 'rdi_status';
$config['tlr_deve_resp'] = 'rdi_device_on_duty';
$config['tlr_deve_target_time'] = 'rdi_due_date';
$config['tlr_deve_building'] = 'rdi_device_building';
$config['tlr_deve_room'] = 'rdi_device_room';
$config['tlr_deve_dev_name'] = 'rdi_device_name';
$config['tlr_deve_dev_env'] = 'rdi_device_environment';
	
$config['tlr_deve_show'] = array($config['tlr_deve_id'],$config['tlr_deve_dev'],$config['tlr_deve_act_date']
								,$config['tlr_deve_performer'],$config['tlr_deve_act'],$config['tlr_deve_status']
								,$config['tlr_deve_resp'],$config['tlr_deve_target_time'],$config['tlr_deve_dev_name'],$config['tlr_deve_dev_env'],$config['tlr_deve_room'],$config['tlr_deve_building']);

// Template TLR - Tutkimuslaiteohjelma
$config['tlr_devs_template'] = 'research_device_program';
$config['tlr_devs_id'] = 'rdp_unique_id';
$config['tlr_devs_dev'] = 'rdp_device_ref';
$config['tlr_devs_next_date'] = 'rdp_next';
$config['tlr_devs_type'] = 'rdp_type';
$config['tlr_devs_scope'] = 'rdp_scope';
$config['tlr_devs_status'] = 'rdp_status';
	
$config['tlr_devs_show'] = array($config['tlr_devs_id'],$config['tlr_devs_dev'],$config['tlr_devs_next_date'],$config['tlr_devs_type'],$config['tlr_devs_scope'],$config['tlr_devs_status']);
$config['tlr_devs_summary_show'] = array($config['tlr_devs_id'],$config['tlr_devs_dev'],$config['tlr_devs_dev'].":".$config['tlr_dev_erit_luokka'],$config['tlr_devs_next_date'],$config['tlr_devs_type'],$config['tlr_devs_scope'],$config['tlr_devs_status']);
							
// Template TLR - Tutkimuslaiteympäristö
$config['tlr_devy_template'] = 'research_environment';
$config['tlr_devy_id'] = 'rde_unique_id';
$config['tlr_devy_name'] = 'rde_name';
$config['tlr_devy_desc'] = 'rde_description';
$config['tlr_devy_count'] = 'rde_count_of_devices';
$config['tlr_devy_quality'] = 'rde_quality_on_duty';
$config['tlr_devy_toim'] = 'rde_org_unit';
$config['tlr_devy_tutk'] = 'rde_research_area';
$config['tlr_devy_koord_ref'] = 'rde_on_duty';
$config['tlr_devy_koord'] = 'tlr_raportit_laitekoordinaattorit';
	
$config['tlr_devy_show'] = array($config['tlr_devy_name'],$config['tlr_devy_desc'],$config['tlr_devy_count'],$config['tlr_devy_toim'],$config['tlr_devy_tutk'],$config['tlr_devy_koord']);


							