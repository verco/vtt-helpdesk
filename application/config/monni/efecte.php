<?
/*
* Efecte configs
*/
$ip = "88.208.223.63"; // domain / ip
$folder = 'efecte_demo';
$config['ip'] = $ip;
$config['folder'] = $folder;
$config['efecte_url'] = "http://$ip/$folder";
$config['app_url'] = "http://$ip/$folder/helpdesk/index.php/";
$config['file_dl'] = "http://$ip/$folder/fileDownload.ws";
$config['file_ul'] = "http://$ip/$folder/fileUpload.ws";

$config['file_dl_redir'] = $config['app_url']."download/file";

$config['webapi_user'] = 'webapi';
$config['webapi_pw'] = 'webapi';

$config['helpdesk_application_attr'] = 461;

$config['helpdesk_file_attr'] = 496;

$config['helpdesk_folder'] = 'request';

$config['helpdest_template'] = 'helpdesk';
