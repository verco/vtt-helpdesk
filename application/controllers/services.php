<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Services extends CI_Controller {

    public function index() {
        $this->session->set_userdata('language','EN');

        //$this->output->cache(60);
        $this->load->model('process_model','process');
        $this->template->set('processes',$this->process->get_all());
        $this->template->set('process_show_fields', $this->config->item('process_show_fields'));

        $this->load->model('feedback_model','feedback');
        $feedback = $this->feedback->get_all_questions();

        $this->template->set('feedbacks',$feedback);

        $this->template->render();
    }

    public function form($category_name = false, $user = false) {
        $category_name = urldecode($category_name);
        $this->load->model('process_model','process');
        $this->template->set('processes',$this->process->get_all());
        $this->template->set('process_show_fields', $this->config->item('process_show_fields'));
        
        $this->load->model('user_model','user');
        $user = $this->user->getLogin();

        $this->template->set('user',$user   );
        if (!$category_name) {
            echo "Error";
            die;
        } else {
            $this->load->model('request_type_model','reqtype');

            $this->load->model('request_model','request');

            $entity = $this->reqtype->get_type_by_id($category_name);

            $inputs_general = $this->reqtype->get_inputs_by_id($category_name,'General');
            $inputs_type = $this->reqtype->get_inputs_by_id($category_name,'Additional');

            $this->template->set('entity',$entity);
            $this->template->set('inputs_general',$inputs_general);
            $this->template->set('inputs_type',$inputs_type);
            $this->template->render();
        }
    }
}