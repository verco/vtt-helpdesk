<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tlr extends CI_Controller {

	public function clearcache(){
		$this->load->model('user_model','user');
		$this->load->helper('file');
		$user = $this->user->getLogin();
		//echo "clear".$user;
		
		if (file_exists($this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp')) { 
			unlink($this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp');
			log_message('debug', $this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp'.'-cleared');
			echo $this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp'.'-cleared';
			
		}
		if (file_exists($this->config->item('tlr_cache_folder').'/tmp_quality_'.$user.'.tmp')) { 
			unlink($this->config->item('tlr_cache_folder').'/tmp_quality_'.$user.'.tmp');
			log_message('debug', $this->config->item('tlr_cache_folder').'/tmp_quality_'.$user.'.tmp'.'-cleared');
			echo $this->config->item('tlr_cache_folder').'/tmp_quality_'.$user.'.tmp'.'-cleared';
		}
		if (file_exists($this->config->item('tlr_cache_folder').'/tmp_coord_'.$user.'.tmp')) { 
			unlink($this->config->item('tlr_cache_folder').'/tmp_coord_'.$user.'.tmp');
			log_message('debug', $this->config->item('tlr_cache_folder').'/tmp_coord_'.$user.'.tmp'.'-cleared');
			echo $this->config->item('tlr_cache_folder').'/tmp_coord_'.$user.'.tmp'.'-cleared';
		}		
		redirect('tlr');
		
	}
	
	

    public function index($page=false) {
        $this->load->model('user_model','user');
        $this->load->model('efecte_model','efecte');
        // --temp-- 
		$user = $this->user->getLogin();
		$this->user->set_access_log($user);
		
		//$user="RTESRS";
		//$user="PROHJM";
		//$user="PROPVT";
		
		//$user="BELALE";
		//$user="PROJTP";
		//$user="PROPSV";
		//$user="PROPVT";
		
		//echo "p:".$page;
		//$user="BELLOH";
		//$user="BELUOS";

		
		
		if($this->config->item('temp_user')){
			$user=$this->config->item('temp_user');
		}
		
		$user_data = $this->user->get_user_data($user);
		$this->template->set('user_data' ,$user_data);

		/*$this->load->model('tlr_model','tlr');
		$summary=$this->tlr->get_device_summary($user);	
        $this->template->set('usum' ,$summary);

		$user_data = $this->user->get_user_data($user);
        $isSuperior = $this->user->isSuperior($user);
	//	echo "sup:".$isSuperior;
        $isQuality = $this->user->isQuality($user);
        $isCoord = $this->user->isCoord($user);
	
        $this->template->set('page' ,$page);
        $this->template->set('isSuperior' ,$isSuperior);
        $this->template->set('isQuality' ,$isQuality);
        $this->template->set('isCoord' ,$isCoord);
        $this->template->set('user_data' ,$user_data);
		$this->load->helper('file');

		$isSuperiorTo = $this->user->getSuperiorTo($user); 
		$isSuperiorToRetired = $this->user->getSuperiorToRetired($user);
//		echo "xx:".var_dump($isSuperiorToRetired);
		if($isSuperior){
			if (file_exists($this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp') && (date ("d-m-Y H:i:s.", filemtime($this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp')+3600) > date("d-m-Y H:i:s"))) { //3600
			//if (false) {
				//echo $this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp';
//$group_summary_file = fopen($this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp',"r");

//$group_summary = unserialize(fread($group_summary_file, filesize($this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp')));
//				$group_summary = unserialize(read_file(ltrim($this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp')));
				//$group_summary = unserialize(read_file('./'.$this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp'));
				//$group_summary = read_file("/".$this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp');
				//$group_summary = json_decode(read_file($this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp'));
				$group_summary = unserialize(read_file($this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp'));
				//echo "file:".var_dump($group_summary);
				//fclose($group_summary_file);
			} else {
				$group_summary = $this->tlr->get_device_summary($user,true,$isSuperiorTo,$isSuperiorToRetired);
//				var_dump($group_summary["tot_xml"]["TL00612"]);//
				//var_dump($group_summary["special"]);//
//				write_file($this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp',json_encode($group_summary));
				write_file($this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp',serialize($group_summary));
			}
			$this->template->set('group_usum' ,$group_summary);
		}	

//		$isQuality=true;
		if($isQuality){
			if (file_exists($this->config->item('tlr_cache_folder').'/tmp_quality_'.$user.'.tmp') && (date ("d-m-Y H:i:s.", filemtime($this->config->item('tlr_cache_folder').'/tmp_quality_'.$user.'.tmp')+3600) > date("d-m-Y H:i:s"))) {
			//if (false) {
				$quality_summary = unserialize(read_file($this->config->item('tlr_cache_folder').'/tmp_quality_'.$user.'.tmp'));
			} else {
				$quality_summary = $this->tlr->get_device_summary($user,false,false,false,true);
			}

			$this->template->set('quality_usum' ,$quality_summary);
		}	

		//$isCoord=true;
		if($isCoord){
			if (file_exists($this->config->item('tlr_cache_folder').'/tmp_coord_'.$user.'.tmp') && (date ("d-m-Y H:i:s.", filemtime($this->config->item('tlr_cache_folder').'/tmp_coord_'.$user.'.tmp')+3600) > date("d-m-Y H:i:s"))) {
			//if (false) {
				$coord_tlr_data = unserialize(read_file($this->config->item('tlr_cache_folder').'/tmp_coord_'.$user.'.tmp'));
			} else {
				$coord_tlr_data = $this->tlr->get_device_summary($user,false,false,false,false,true);
			}

			$this->template->set('coord_usum' ,$coord_tlr_data);
		}	

		//	var_dump($quality_summary["lt_all_xml"]);
		//echo "XX:".var_dump($quality_summary);
		
		if($isSuperiorTo==""){
			$isSuperiorTo="'".$user."'";
		}else{
			$isSuperiorTo.=",'".$user."'";		
		}
		//var_dump($isSuperiorTo);
		
		
		$type = $this->input->get('type');
		$this->template->set('type', $type);*/
			
        $this->template->render("application_tlr");
    }

	public function forsale() {
		$this->load->model('user_model','user');
		$user = $this->user->getLogin();	
//	echo "user:".$user;
	
		$extendedView=$this->user->hasExtendedList($user);
		
		$this->load->model('tlr_model', 'tlr');
		$devices = $this->tlr->get_forsale_devices($extendedView);

		$header=array("Organisaatio","Kategoria","LaiteID","Laitteen malli","Laitteen nimi","Valmistaja","Käyttötarkoitus","Laitteen mittakaava","Avainsanat","Tekniset tiedot","Laitteen tila
","Rakennus","Laiteympäristö","Laitevastuuhenkilö(t)"); //,,"Riskien arviointi","Rakennus","Huone","Laiteympäristö","Laitekoordinaattori","Laitevastuuhenkilö"
		$cols=$this->config->item('tlr_forsale_show');

		$this->template->set('header', $header);
		$this->template->set('cols', $cols);
		$this->template->set('output', $devices);
							
		$this->template->set('data', $devices);
		$this->template->render();	
	}
	
	public function all_devices() {
        $this->load->model('user_model','user');
        $this->load->model('efecte_model','efecte');
        // --temp-- 
		$user = $this->user->getLogin();
		//$user="RTESRS";
		//$user="PROHJM";
		//$user="PROPVT";
		
		//$user="BELALE";
		//$user="PROJTP";
		//$user="PROPSV";
		//$user="PROPVT";
		
		//echo "p:".$page;
		//$user="BELLOH";
		//$user="BELUOS";

		if($this->config->item('temp_user')){
			$user=$this->config->item('temp_user');
		}
		
		$user_data = $this->user->get_user_data($user);
		$this->template->set('user_data' ,$user_data);	
	
        $this->template->render();
	}
	
	public function home() {
		 $this->load->model('user_model','user');
        $this->load->model('efecte_model','efecte');
        // --temp-- 
		$user = $this->user->getLogin();
		//$user="RTESRS";
		//$user="PROHJM";
		//$user="PROPVT";
		
		//$user="BELALE";
		//$user="PROJTP";
		//$user="PROPSV";
		//$user="PROPVT";
		
		//echo "p:".$page;
		//$user="BELLOH";
		//$user="BELUOS";

		if($this->config->item('temp_user')){
			$user=$this->config->item('temp_user');
		}

		$this->load->model('tlr_model','tlr');
		$summary=$this->tlr->get_device_summary($user);	
        $this->template->set('usum' ,$summary);

		$user_data = $this->user->get_user_data($user);
        $isSuperior = $this->user->isSuperior($user);
	//	echo "sup:".$isSuperior;
        $isQuality = $this->user->isQuality($user);
        $isCoord = $this->user->isCoord($user);
	
        $this->template->set('page' ,$page);
        $this->template->set('isSuperior' ,$isSuperior);
        $this->template->set('isQuality' ,$isQuality);
        $this->template->set('isCoord' ,$isCoord);
        $this->template->set('user_data' ,$user_data);
		$this->load->helper('file');

		$isSuperiorTo = $this->user->getSuperiorTo($user); 
		$isSuperiorToRetired = $this->user->getSuperiorToRetired($user);
//		echo "xx:".var_dump($isSuperiorToRetired);
		if($isSuperior){
			if (file_exists($this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp') && (filectime($this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp')+86400 >  time() )) { //3600
			//--if (file_exists($this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp') && (date ("d-m-Y H:i:s.", filemtime($this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp')+86400) > date("d-m-Y H:i:s"))) { //3600
			//if (false) {
				//echo "s-cache";
				$group_summary = unserialize(read_file($this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp'));
			} else {
				//echo "s-no-cache";
				$group_summary = $this->tlr->get_device_summary($user,true,$isSuperiorTo,$isSuperiorToRetired);
				write_file($this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp',serialize($group_summary));
			}
			$this->template->set('group_usum' ,$group_summary);
		}	

//		$isQuality=true;
		if($isQuality){
//			if (file_exists($this->config->item('tlr_cache_folder').'/tmp_quality_'.$user.'.tmp') && (date ("d-m-Y H:i:s.", filemtime($this->config->item('tlr_cache_folder').'/tmp_quality_'.$user.'.tmp')+86400) > date("d-m-Y H:i:s"))) {
			if (file_exists($this->config->item('tlr_cache_folder').'/tmp_quality_'.$user.'.tmp') && (filectime ($this->config->item('tlr_cache_folder').'/tmp_quality_'.$user.'.tmp')+86400 > time())) {
			//if (false) {
				//echo "q-cache";
				$quality_summary = unserialize(read_file($this->config->item('tlr_cache_folder').'/tmp_quality_'.$user.'.tmp'));
			} else {
				//echo "q-no-cache";
				$quality_summary = $this->tlr->get_device_summary($user,false,false,false,true);
				write_file($this->config->item('tlr_cache_folder').'/tmp_quality_'.$user.'.tmp',serialize($quality_summary));
			}
			$this->template->set('quality_usum' ,$quality_summary);
		}	

		//$isCoord=true;
		if($isCoord){
//			if (file_exists($this->config->item('tlr_cache_folder').'/tmp_coord_'.$user.'.tmp') && (date ("d-m-Y H:i:s.", filemtime($this->config->item('tlr_cache_folder').'/tmp_coord_'.$user.'.tmp')+86400) > date("d-m-Y H:i:s"))) {
			if (file_exists($this->config->item('tlr_cache_folder').'/tmp_coord_'.$user.'.tmp') && (filectime ($this->config->item('tlr_cache_folder').'/tmp_coord_'.$user.'.tmp')+86400 > time())) {
			//if (false) {
				//echo "c-cache";
				$coord_tlr_data = unserialize(read_file($this->config->item('tlr_cache_folder').'/tmp_coord_'.$user.'.tmp'));
			} else {
				//echo "c-no-cache";
				$coord_tlr_data = $this->tlr->get_device_summary($user,false,false,false,false,true);
				write_file($this->config->item('tlr_cache_folder').'/tmp_coord_'.$user.'.tmp',serialize($coord_tlr_data));
			}

			$this->template->set('coord_usum' ,$coord_tlr_data);
		}	

		//	var_dump($quality_summary["lt_all_xml"]);
		//echo "XX:".var_dump($quality_summary);
		
		if($isSuperiorTo==""){
			$isSuperiorTo="'".$user."'";
		}else{
			$isSuperiorTo.=",'".$user."'";		
		}
		//var_dump($isSuperiorTo);
		
		
		$type = $this->input->get('type');
		$this->template->set('type', $type);
	
		$this->template->render();
	}

}