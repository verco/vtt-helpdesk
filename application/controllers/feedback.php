<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Feedback extends MY_Controller {

    function __construct() {
        parent::__construct();
        log_message('debug', 'Controller Feedback');

        //$this->load->model('ticket_model', 'ticket');
        $this->load->model('feedback_model', 'feedback');
    }

    private function get_ticket($ticket_id) {
        if ($ticket_id === false) {
            // if we don't have ticket ID
            // $this->template->set('ticket', $ticket);
            // $this->template->render();
            show_404('feedback');
        }
       
        $ticket = $this->feedback->get_ticket($ticket_id);

        if ($ticket === false) {
            // ticket was not found (wrong id) or otherwise not eligible for feedback
            //show_error('Invalid ticket number');
            redirect('feedback/error/');
        }

        return $ticket;
    }

    private function set_language($ticket = false) {

        // Choose selected language
        if ($this->input->get('lang') == 'fi') {
            $this->lang->load('feedback', 'finnish');
            $this->template->set('language', 'fi');
        } elseif ($this->input->get('lang') == 'en') {
            $this->lang->load('feedback', 'english');
            $this->template->set('language', 'en');
        } else { // If not selected, find users language and use that
            /*if ($this->ticket->customer_is_finnish($ticket)) {
                $this->lang->load('feedback', 'finnish');
                $this->template->set('language', 'fi');
            } else {
                $this->lang->load('feedback', 'english');
                $this->template->set('language', 'en');
            }*/
            $this->lang->load('feedback', 'english');
            $this->template->set('language', 'en');
        }
    }

    /**
     * Feedback form
     * @param $ticket_id
     */
    public function index($ticket_id = false) {
        if ($ticket_id){
            $ticket = $this->get_ticket($ticket_id);
        }

        $req = 'SR-';

        if ($this->feedback->has_positive_feedback($ticket_id)) {
            // ticket already has feedback with positive acceptance
            //show_error('Ticket has been already accepted');
            $accepted_ok = true;
            if (strpos($ticket_id, $req) === 0) {
                redirect('feedback/error/'.$accepted_ok.'/sr');
            } else redirect('feedback/error/'.$accepted_ok.'/inc'); 
        }  
    
        $this->load->model('user_model','user');
        $user = $this->user->getLogin(false);
        $this->template->set('user',$user);
        
        $this->template->set('ticket', $ticket);
        $this->set_language($ticket,$lang);
        $this->template->render();
    }

    /**
     * Create new feedback
     */
    public function save() {
        //var_dump($this->input->post());die;
        $ticket_id = $this->input->post('ticket_id');

        if($ticket_id){
            $ticket = $this->get_ticket($ticket_id);
        }

        if ($this->feedback->has_positive_feedback($ticket_id)) {
            // ticket already has feedback with positive acceptance
            //show_error('Ticket has been already accepted');
            $accepted_ok = true;
			redirect('feedback/error/'.$accepted_ok);
        }

        $this->set_language($ticket);
    
        if ($this->input->post('accept') == 'no') {
            $this->feedback->create($ticket_id, 
                $this->config->item('feedback_resolution_accepted/no'), 
                false,
                $this->input->post('additional_comment'),
                false
            );
            $this->feedback->reopen_and_add_comment($ticket_id, $this->input->post('additional_comment'), $ticket);
        } elseif ($this->input->post('accept') == 'yes') {
            $this->feedback->create($ticket_id, 
                $this->config->item('feedback_resolution_accepted/yes'), 
                $this->input->post('numerical_evaluation'), 
                $this->input->post('free_comment'), 
                $this->input->post('meet_expectation'));
        } /*elseif ($this->input->post('exclude_feedback') == 'regular_feedback') {
            $this->feedback->create(false, false, $this->config->item('feedback_customer_satisfaction/'.$this->input->post('meet_expectation')),
                $this->input->post('numerical_evaluation'), $this->input->post('free_comment'));  
        } */else {
            show_error('Required fields missing');
            return;
        }

        //$lang = '?lang=fi';
        if ($this->input->get('lang') == 'fi') {
            $lang = '?lang=fi';
        } elseif ($this->input->get('lang') == 'en') {
            $lang = '?lang=en';
        }

        redirect('feedback/thanks/'.$ticket_id.$lang);

    }

    /**
     * Show "thanks" page
     */
    public function thanks($ticket_id = false) {
        if($ticket_id){
            $ticket = $this->get_ticket($ticket_id);
            
            $this->set_language($ticket);
        }
        $this->set_language($ticket);
        $this->template->render();
    }

    public function error($accepted_ok = false, $type) {
        $this->template->set('accepted_ok', $accepted_ok);
        $this->template->set('type', $type);
        $this->set_language($ticket);
        $this->template->render();
    }
}

/* End of file feedback.php */
/* Location: ./application/controllers/feedback.php */
