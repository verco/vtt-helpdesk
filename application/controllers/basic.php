<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Basic extends CI_Controller {

	private function fileup ($file = false, $entityId = false, $attributeId = false, $debug = false) {

		import java.io.File;
		import org.apache.commons.httpclient.HttpClient;
		import org.apache.commons.httpclient.HttpStatus;
		import org.apache.commons.httpclient.methods.PostMethod;
		import org.apache.commons.httpclient.methods.multipart.StringPart;
		import org.apache.commons.httpclient.methods.multipart.FilePart;
		import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
		import org.apache.commons.httpclient.methods.multipart.Part;
		import org.apache.commons.httpclient.methods.multipart.PartSource;
		import org.apache.commons.httpclient.params.HttpMethodParams;
		import org.apache.commons.httpclient.auth.AuthScope;
		import org.apache.commons.httpclient.UsernamePasswordCredentials;

		if ($debug) {
			log_message('error', "attributeId: " . $attributeId);
			log_message('error', "entityId: " . $entityId);
			log_message('error', "file1: " . $file["name"]);
			log_message('error', "file1 tmpname: " . $file["tmp_name"] );
			log_message('error', "webapi user: ".$this->config->item('webapi_user'));
			log_message('error', "webapi pw: ".$this->config->item('webapi_pw'));
			log_message('error', 'ip: '.$this->config->item('ip'));
		}
		
		$originalFileName = $file["name"];
		$fileName = $file["tmp_name"];
		
		$targetFile = new File($fileName);
		$targetURL = $this->config->item('file_ul');

		$filePost = new PostMethod($targetURL);

		if ($debug)	log_message('error', "Uploading " . $fileName . " to " . $targetURL);

		$filePart = new FilePart($file["name"], $originalFileName, $targetFile);
		$filePart->setTransferEncoding(null);

		//	Part[] parts;

		$entityIdPart = new StringPart("entityId", $entityId);
		$entityIdPart->setTransferEncoding(null);

		$attributeIdPart = new StringPart("attributeId", $attributeId);
		$attributeIdPart->setTransferEncoding(null);

		$operationTypePart = new StringPart("operationType", 'add');
		$operationTypePart->setTransferEncoding(null);

		$parts = array($entityIdPart, $attributeIdPart, $operationTypePart, $filePart);

		$filePost->setRequestEntity(
			new MultipartRequestEntity($parts, $filePost->getParams())
		);
		$client = new HttpClient();
		$client->getState()->setCredentials(
			new AuthScope($this->config->item('ip'), $this->config->item('port'), "Efecte Web Services"),
			new UsernamePasswordCredentials($this->config->item('webapi_user'), $this->config->item('webapi_pw'))
		);

		$client->getHttpConnectionManager()->getParams()->setConnectionTimeout(5000);
		$status = $client->executeMethod($filePost);
		if ($status == HttpStatus::SC_OK) {
			log_message('error',"Upload complete, response=" . $filePost->getResponseBodyAsString());
		} else {
			log_message('error',"Upload failed, response=" . HttpStatus::getStatusText($status));
		}
		return true;
		
	}

	public function save() {
		
		//$this->load->model('user_model','user');
		//$user = $this->user->getLogin(); // fetch the session username from browser
		
		$this->load->model('efecte_model','efecte');
        $p = $_POST;

//		echo "post:".var_dump($_POST);
//		echo "files:".var_dump($_FILES);
		//die;
        $params = array();

        // Create save params from folder info
		$this->efecte->createSaveParams('template_code',false, $p['exclude_template_code'],$params);
		$template_code = $p['exclude_template_code'];
        $this->efecte->createSaveParams('folder_code',false,$p['exclude_folder_code'],$params);
        
       // $fileattr = $p['exclude_file_attr'];
		$fileattr = $this->config->item('pp_files_id');
 
        foreach ($p as $k => $v) {
        	if (substr($k, 0,8) == 'exclude_')
        		unset($p[$k]);
        }

        foreach ($p as $k => $v) {
            $this->efecte->createSaveParams($k,'value',$v,$params);
        }

        $saveResult = $this->efecte->saveEntity($params);
        unset($params);

        $params["search_string"] = "entity.template.code = '$template_code' and  \$".$this->config->item('pp_id')."\$ = '".$p[$this->config->item('pp_id')]."'";
        $latest = $this->efecte->get_values(new SimpleXMLElement($this->efecte->readEntity($params)), array('entityid'));

		//die;      

        log_message('debug',$saveResult);
		$newfiles = array(); 

	    if ($_FILES[$this->config->item('pp_files')]["name"] != '') {
		    foreach($_FILES as $fieldname => $fieldvalue) 
		        foreach($fieldvalue as $paramname => $paramvalue) 
		            foreach((array)$paramvalue as $index => $value)
		   				if (($paramname == 'name' && $value != '') OR isset($newfiles[$fieldname][$index]['name'])) 
		                	$newfiles[$fieldname][$index][$paramname] = $value; 
		                
			if ($newfiles) {
				foreach($newfiles[$this->config->item('pp_files')] as $file) {
					$this->fileup($file,$latest[0]['entityid'], $fileattr,true);
				}
			}
		}

        $this->output->set_header('Cache-Control: no-cache, must-revalidate');
		$this->output->set_header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');

		$this->template->render();
        redirect('basic/thank_you');
	}

	public function thank_you() {
		$this->template->render();
	}

	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */