<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index() {	
		$this->load->model('group_model','group');
		$categories = $this->group->get_all_main();
		
		$this->template->set('categories',$categories);

		$this->load->model('user_model','user');
		$user = $this->user->getLogin();

		$this->template->set('user',$user	);
		
		$applications = $this->efecte_model->get_template_static_dropdown_attribute_values($this->config->item('helpdesk_application_attr'));
		//fred($applications);

		$this->template->set('applications',$applications);

		$this->load->model('request_model','request');
		$requests = $this->request->get_requests($user);
	
		$this->template->set('requests' ,$requests);

		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Cache-Control: post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		$this->output->set_header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		$this->template->render();
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */