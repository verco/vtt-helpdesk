<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apk extends CI_Controller {

    public function index($group=false) {
        $this->load->model('apk_model','apk');
        $pp = $this->apk->get_pp($_GET["sr"]);
        
		$attachments=$this->apk->get_pp_attachments($_GET["sr"]);
		
        $this->template->set('pp' ,$pp);
        $this->template->set('attachments' ,$attachments);

        $this->template->render('application_apk');
    }

}