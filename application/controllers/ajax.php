<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

	public function tlr_all_devices() {
		$this->load->model('tlr_model', 'tlr');
		$this->load->helper('file');

		if (file_exists($this->config->item('tlr_cache_folder').'/tmp_all_devices.tmp') && (filectime ($this->config->item('tlr_cache_folder').'/tmp_all_devices.tmp')+86400 > time())) {
		//if (true){
			log_message('debug', 'all tlr devices -FROM CACHE-');
			$devices = unserialize(read_file($this->config->item('tlr_cache_folder').'/tmp_all_devices.tmp'));
		} else {
			$devices = $this->tlr->get_all_devices();
			log_message('debug', 'all tlr devices -CACHE REFRESHED-');
			write_file($this->config->item('tlr_cache_folder').'/tmp_all_devices.tmp',serialize($devices));
		}
		
		$this->template->set('data', $devices);
		$this->template->render();
	}

	public function tlr_forsale_devices() {
		$this->load->model('user_model','user');
		$user = $this->user->getLogin();	
//	echo "user:".$user;
	
		$extendedView=$this->user->hasExtendedList($user);
		
		$this->load->model('tlr_model', 'tlr');
		$devices = $this->tlr->get_forsale_devices($extendedView);

		$header=array("Organisaatio","Kategoria","LaiteID","Laitteen malli","Laitteen nimi","Valmistaja","Käyttötarkoitus","Laitteen mittakaava","Avainsanat","Tekniset tiedot","Laitteen tila
","Rakennus","Laiteympäristö","Laitevastuuhenkilö(t)"); //,,"Riskien arviointi","Rakennus","Huone","Laiteympäristö","Laitekoordinaattori","Laitevastuuhenkilö"
		$cols=$this->config->item('tlr_forsale_show');

		$this->template->set('header', $header);
		$this->template->set('cols', $cols);
		$this->template->set('output', $devices);
							
		$this->template->set('data', $devices);
		$this->template->render();
	}	
	
	public function tlr_default_report() {
		if ($this->input->is_ajax_request() === true) {
			$this->load->model('user_model','user');
			$user = $this->user->getLogin();
			
			if($this->config->item('temp_user')){
				$user=$this->config->item('temp_user');
			}
			
			$isSuperior = $this->user->isSuperior($user);
			$isSuperiorTo = $this->user->getSuperiorTo($user);
			$isSuperiorToRetired = $this->user->getSuperiorToRetired($user);
			$isQuality = $this->user->isQuality($user);
	        $isCoord = $this->user->isCoord($user);


			if($isSuperiorTo==""){
				$isSuperiorTo="'".$user."'";
			}else{
				$isSuperiorTo.=",'".$user."'";		
			}
			
			$type = $this->input->get('type');
				
			$this->load->model('efecte_model','efecte');
			$this->load->model('tlr_model','tlr');
			$this->load->helper('file');
	
			if($type=='browse_window') {
				$this->template->set('all', true);

				//echo "xx";
				$header=array("Organisaatio","Kategoria","LaiteID","Laitteen malli","Laitteen nimi","Valmistaja","Käyttötarkoitus","Laitteen mittakaava","Avainsanat","Tekniset tiedot","Laitteen tila
","Rakennus","Laiteympäristö","Laitevastuuhenkilö(t)"); //,,"Riskien arviointi","Rakennus","Huone","Laiteympäristö","Laitekoordinaattori","Laitevastuuhenkilö"
				$cols=array($this->config->item('apu_tlr_dev_org')
							,$this->config->item('tlr_dev_cat')
							,$this->config->item('tlr_dev_id')
							,$this->config->item('tlr_dev_model')
							,$this->config->item('tlr_dev_name')
							,$this->config->item('tlr_dev_man')
							,$this->config->item('tlr_dev_usage')
							,$this->config->item('tlr_dev_scale')
							,$this->config->item('tlr_dev_keyw')
							,$this->config->item('tlr_dev_techinfo')
							,$this->config->item('tlr_dev_status')
							,$this->config->item('tlr_dev_building')
							,$this->config->item('tlr_dev_env')
							,$this->config->item('apu_tlr_dev_laitevastuuhlo')
							);

							/*
				$header=array("Organisaatio","Kategoria","LaiteID"); //,,"Riskien arviointi","Rakennus","Huone","Laiteympäristö","Laitekoordinaattori","Laitevastuuhenkilö"
				$cols=array($this->config->item('apu_tlr_dev_org')
							,$this->config->item('tlr_dev_cat')
							,$this->config->item('tlr_dev_id')
							);

				*/			
							//14
/*
							,$this->config->item('tlr_dev_building')
							,$this->config->item('tlr_dev_room')
							,$this->config->item('tlr_dev_env')
							,$this->config->item('tlr_dev_env').":tlr_raportit_laitekoordinaattorit"
							,$this->config->item('apu_tlr_dev_laitevastuuhlo')

*/				
				//if (file_exists($this->config->item('tlr_cache_folder').'/tmp_all_devices.tmp') && (filectime($this->config->item('tlr_cache_folder').'/tmp_all_devices.tmp')+86400 >  time() )) { //3600
				//if (file_exists($this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp') && (date ("d-m-Y H:i:s.", filemtime($this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp')+86400) > date("d-m-Y H:i:s"))) {
				if (true){
					//echo "file löyty";
					//$output_file = read_file($this->config->item('tlr_cache_folder').'/tmp_all_devices.tmp');
					//$this->template->set('output_file', $output_file);
				} else {
					//echo "kannasta".$this->config->item('tlr_cache_folder');
					$time1 = microtime(true);
					$output= $this->tlr->get_all_devices();
					$time2 = microtime(true);
					//echo "x:".count($output);
					//					$group_tlr_data = $this->tlr->get_device_summary($user,true,$isSuperiorTo,$isSuperiorToRetired);
					echo "count:".count($output);
					//var_dump($output);
					$fileout='<table class="dtable" id="dtable" style="width:500px;">';
					
					$fileout.='<thead><tr>';//<td>&nbsp;</td>';
					foreach ($header as $h){
						$fileout.='<th>'.$h.'</th>';	 
					}					
					$fileout.='</tr></thead>';

					$fileout.='<tfoot><tr>';//<td>&nbsp;</td>';
					foreach ($header as $h){
						$fileout.='<th>'.$h.'</th>';	 
					}					
					$fileout.='</tr></tfoot>';
					
					$fileout.='<tbody>';
				//$r=0;
	//				for($ii=0;$ii<count($output);$ii++){
					for($ii=0;$ii<5000;$ii++){
//					foreach ($output as $p){
						$fileout.='<tr>';//<td>'.($i+1).'</td>';
						foreach ($cols as $c){
							$fileout.='<td>';
							
							
							foreach ($output[$ii][$c] as $x){
//							foreach ($p[$c] as $x){
								//if (substr($x,0,7)=='http://'){
							//		$fileout.= "<a href=\"".$x."\" target=\"_blank\">".$x."</a>";						
							//	}else{
							//		$time_strip=array(" 00:00:00 EEST"," 00:00:00 EET");
							//		$fileout.= str_replace($time_strip,"",$x)."<br>";
									$fileout.= $x;
									//$fileout.= $x."<br>";
							//	}
							}

							$fileout.='</td>';
						}
						$fileout.='</tr>';       
						//$i++;                             
					}
					$fileout.='</tbody></table>';
					$time3 = microtime(true);
					$output=false;
					write_file($this->config->item('tlr_cache_folder').'/tmp_all_devices.tmp',$fileout);
					$time4 = microtime(true);
					echo "<br>haku:".($time2-$time1);
					echo "<br>kasaus:".($time3-$time2);
					echo "<br>kirjoitus:".($time4-$time3);
				}
				//$this->template->render();

			}else{
				$tlr_data = $this->tlr->get_device_summary($user);

				//$isQuality=true;
				if($isSuperior){
					//---if (file_exists($this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp') && (filectime($this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp')+86400 >  time() )) { //3600
					if (file_exists($this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp') && (date ("d-m-Y H:i:s.", filemtime($this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp')+86400) > date("d-m-Y H:i:s"))) {
					//if (false){
						$group_tlr_data = unserialize(read_file($this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp'));
					} else {
						$group_tlr_data = $this->tlr->get_device_summary($user,true,$isSuperiorTo,$isSuperiorToRetired);
						write_file($this->config->item('tlr_cache_folder').'/tmp_'.$user.'.tmp',serialize($group_tlr_data));
					}
					//var_dump($group_tlr_data);
				}
				
				if($isQuality){
		//			if (file_exists($this->config->item('tlr_cache_folder').'/tmp_quality_'.$user.'.tmp') && (date ("d-m-Y H:i:s.", filemtime($this->config->item('tlr_cache_folder').'/tmp_quality_'.$user.'.tmp')+86400) > date("d-m-Y H:i:s"))) {
					if (file_exists($this->config->item('tlr_cache_folder').'/tmp_quality_'.$user.'.tmp') && (filectime ($this->config->item('tlr_cache_folder').'/tmp_quality_'.$user.'.tmp')+86400 > time())) {
					//if (false){
						$quality_tlr_data = unserialize(read_file($this->config->item('tlr_cache_folder').'/tmp_quality_'.$user.'.tmp'));
					} else {
						$quality_tlr_data = $this->tlr->get_device_summary($user,false,false,false,true);
						write_file($this->config->item('tlr_cache_folder').'/tmp_quality_'.$user.'.tmp',serialize($quality_tlr_data));
					}
				}			
				if($isCoord){
					//if (file_exists($this->config->item('tlr_cache_folder').'/tmp_coord_'.$user.'.tmp') && (date ("d-m-Y H:i:s.", filemtime($this->config->item('tlr_cache_folder').'/tmp_coord_'.$user.'.tmp')+86400) > date("d-m-Y H:i:s"))) {
					if (file_exists($this->config->item('tlr_cache_folder').'/tmp_coord_'.$user.'.tmp') && (filectime ($this->config->item('tlr_cache_folder').'/tmp_coord_'.$user.'.tmp')+86400 > time())) {
					//if (false){
						$coord_tlr_data = unserialize(read_file($this->config->item('tlr_cache_folder').'/tmp_coord_'.$user.'.tmp'));
					} else {
						$coord_tlr_data = $this->tlr->get_device_summary($user,false,false,false,false,true);
						write_file($this->config->item('tlr_cache_folder').'/tmp_coord_'.$user.'.tmp',serialize($coord_tlr_data));
					}
				}			
				
				
				if(substr($type,0,10)=="u_special_"){
					$isSpecialDev=true;
					$specialType=substr($type,10);
					$special=$tlr_data["specials"];
				}else if(substr($type,0,10)=="g_special_"){
					$isSpecialDev=true;
					$specialType=substr($type,10);
					$special=$group_tlr_data["specials"];
				}else if(substr($type,0,10)=="q_special_"){
					$isSpecialDev=true;
					$specialType=substr($type,10);
					$special=$quality_tlr_data["specials"];
				}else if(substr($type,0,10)=="c_special_"){
					$isSpecialDev=true;
					$specialType=substr($type,10);
					$special=$coord_tlr_data["specials"];
				}
			}
			
			
//			var_dump($quality_tlr_data["specials"]);
//			var_dump($quality_tlr_data);
			//var_dump($group_tlr_data);
			//defaults
			$header=array("LaiteID","Laitteen nimi","Kategoria","Erityisluokka","Tila","Riskien arviointi","Rakennus","Huone","Laiteympäristö","Laitekoordinaattori","Laitevastuuhenkilö");
            $cols=array($this->config->item('tlr_dev_id')
						,$this->config->item('tlr_dev_name')
						,$this->config->item('tlr_dev_cat')
						,$this->config->item('apu_tlr_dev_erit_luokka')
						,$this->config->item('tlr_dev_status')
						,$this->config->item('tlr_dev_rt_stat')
						,$this->config->item('tlr_dev_building')
						,$this->config->item('tlr_dev_room')
						,$this->config->item('tlr_dev_env')
						,$this->config->item('tlr_dev_env').":tlr_raportit_laitekoordinaattorit"
						,$this->config->item('apu_tlr_dev_laitevastuuhlo')
						);
			
			$pass=true;

			if($isSpecialDev) {
				//echo "special:".$isSpecialDev;//substr($type,10); otsikko
				//echo "<br>";//substr($type,10); otsikko
				//echo "type:".$specialType;//substr($type,10); otsikko
				//echo "out:".var_dump($special[$specialType])."---end---DIE";
				$output=$special[$specialType];
				$this->template->set('type', $type);
				$this->template->set('header', $header);
				$this->template->set('cols', $cols);
				$this->template->set('output', $output);
				$this->template->render();		
				//die;
			}
			//				$pass=false;
//				$output=$tlr_data["tot_xml"];
				//$output = $this->tlr->get_tlr_devices($user,$isSuperiorTo);




			if($type=='tot') {
				$pass=false;
				$output=$tlr_data["tot_xml"];
				//$output = $this->tlr->get_tlr_devices($user,$isSuperiorTo);
			}else if($type=='risk') {
				$pass=false;
//				$header=array("LaiteID","Tila","Laitteen nimi","Laiteympäristö","Kategoria","Valmistaja","Käyttötarkoitus","Riskitarkastelu","Riskitarkastelun tekijä");
  /*              $cols=array($this->config->item('tlr_dev_id')
						,$this->config->item('tlr_dev_status')
						,$this->config->item('tlr_dev_name')
						,$this->config->item('tlr_dev_laitekoord')
						,$this->config->item('tlr_dev_cat')
						,$this->config->item('tlr_dev_man')
						,$this->config->item('tlr_dev_name')
						,$this->config->item('tlr_dev_rt_stat')
						,$this->config->item('tlr_dev_rt_stat_by'));
*/
				$header=array("LaiteID","Laitteen nimi","Kategoria","Erityisluokka","Tila","Riskien arviointi","Rakennus","Huone","Laitevastuuhenkilö","Riskitarkastelun tekijä","Riskitarkastelun tulos","Riskitarkastelun päivämäärä","Linkki riskitarkasteludokumenttiin");
				$cols=array($this->config->item('tlr_dev_id')
							,$this->config->item('tlr_dev_name')
							,$this->config->item('tlr_dev_cat')
							,$this->config->item('apu_tlr_dev_erit_luokka')
							,$this->config->item('tlr_dev_status')
							,$this->config->item('tlr_dev_rt_stat')
							,$this->config->item('tlr_dev_building')
							,$this->config->item('tlr_dev_room')
							,$this->config->item('apu_tlr_dev_laitevastuuhlo')
							,$this->config->item('tlr_dev_rt_madeby')
							,$this->config->item('tlr_dev_rt_res')
							,$this->config->item('tlr_dev_rt_date')
							,$this->config->item('tlr_dev_rt_doc')
							);

				$output=$tlr_data["risk_xml"];
			}else if($type=='risk_no') {
				$pass=false;
				$header=array("LaiteID","Laitteen nimi","Kategoria","Erityisluokka","Tila","Riskien arviointi","Rakennus","Huone","Laitevastuuhenkilö","Riskitarkastelun tekijä","Riskitarkastelun tulos","Riskitarkastelun päivämäärä","Linkki riskitarkasteludokumenttiin");
				$cols=array($this->config->item('tlr_dev_id')
							,$this->config->item('tlr_dev_name')
							,$this->config->item('tlr_dev_cat')
							,$this->config->item('apu_tlr_dev_erit_luokka')
							,$this->config->item('tlr_dev_status')
							,$this->config->item('tlr_dev_rt_stat')
							,$this->config->item('tlr_dev_building')
							,$this->config->item('tlr_dev_room')
							,$this->config->item('apu_tlr_dev_laitevastuuhlo')
							,$this->config->item('tlr_dev_rt_madeby')
							,$this->config->item('tlr_dev_rt_res')
							,$this->config->item('tlr_dev_rt_date')
							,$this->config->item('tlr_dev_rt_doc')
							);
				$output=$tlr_data["no_risk_xml"];
			}else if($type=='del') {
/*				$header=array("LaiteID","Tila","Laitteen nimi","Laiteympäristö","Kategoria","Valmistaja","Käyttötarkoitus");
				$cols=array($this->config->item('tlr_dev_id')
							,$this->config->item('tlr_dev_status')
							,$this->config->item('tlr_dev_name')
							,$this->config->item('tlr_dev_laitekoord')
							,$this->config->item('tlr_dev_cat')
							,$this->config->item('tlr_dev_man')
							,$this->config->item('tlr_dev_name')
							);*/
				$header=array("LaiteID","Laitteen nimi","Kategoria","Erityisluokka","Ostopäivä","Poistopäivä","Riskien arviointi","Rakennus","Huone","Laiteympäristö","Laitevastuuhenkilö","Laiterekisterinumero (Kasperi)","Kohdenumero (Kasperi)");//,"LVH:n tiimi"
				$cols=array($this->config->item('tlr_dev_id')
							,$this->config->item('tlr_dev_name')
							,$this->config->item('tlr_dev_cat')
							,$this->config->item('apu_tlr_dev_erit_luokka')
							,$this->config->item('tlr_dev_pur_date')
							,$this->config->item('tlr_dev_status_change')
							,$this->config->item('tlr_dev_rt_stat')
							,$this->config->item('tlr_dev_building')
							,$this->config->item('tlr_dev_room')
							,$this->config->item('tlr_dev_env')
							,$this->config->item('apu_tlr_dev_laitevastuuhlo')
							//,$this->config->item('tlr_dev_resp_team')
							,$this->config->item('tlr_dev_kas_laitenro')
							,$this->config->item('tlr_dev_kas_kohdenro')
							);
							
				
				$pass=false;
				$output=$tlr_data["del_xml"];
			}else if($type=='wait') {
				$header=array("LaiteID","Laitteen nimi","Tila","Laitevastuuhenkilö","Rakennus","Laiterekisterinumero(Kasperi)","Kohdenumero(Kasperi)","Ostopäivä","Hinta","Toimittaja");//,"LVH:n tiimi"
				$cols=array($this->config->item('tlr_dev_id')
							,$this->config->item('tlr_dev_name')
							,$this->config->item('tlr_dev_status')
							,$this->config->item('apu_tlr_dev_laitevastuuhlo')
							//,$this->config->item('tlr_dev_resp_team')
							,$this->config->item('tlr_dev_building')
							,$this->config->item('tlr_dev_kas_laitenro')
							,$this->config->item('tlr_dev_kas_kohdenro')
							,$this->config->item('tlr_dev_pur_date')
							,$this->config->item('tlr_dev_pur_price')
							,$this->config->item('tlr_dev_pur_supl')
							);
				$pass=false;
				$output=$tlr_data["ko_xml"];
			}else if($type=='kk') {
				$pass=false;
				$output=$tlr_data["kk_xml"];
			}else if($type=='u_special_EX-laite') {
				$output=$tlr_data["specials"]['EX-laite'];
			}else if($type=='ell') {
				$pass=false;
				$output=$tlr_data["ell_xml"];
			}else if($type=='open_jobs') {
				$pass=false;
				$header=array("Laitetapahtuman ID","Tila","Laitekoodi","Laitteen nimi","Tekijä/yhdyshenkilö","Vastuuhenkilö","Aktiviteetti","Laiteymäristö","Tavoiteajankohta","Toteutumisajankohta");
                //$cols=$this->config->item('tlr_deve_show');
				$cols=array($this->config->item('tlr_deve_id')
						,$this->config->item('tlr_deve_status')
						,$this->config->item('tlr_deve_dev')
						,$this->config->item('tlr_deve_dev_name')
						,$this->config->item('tlr_deve_performer')
						,$this->config->item('tlr_deve_resp')
						,$this->config->item('tlr_deve_act')
						,$this->config->item('tlr_deve_dev_env')
						,$this->config->item('tlr_deve_target_time')
						,$this->config->item('tlr_deve_act_date')
						);

				$output=$tlr_data["lt_xml"];
			}else if($type=='open_jobs_all') {
				$pass=false;
/*				$header=array("Laitetapahtuman ID","Tila","Laitekoodi","Laitteen nimi","Tekijä/yhdyshenkilö","Vastuuhenkilö","Aktiviteetti","Laiteymäristö","Tavoiteajankohta","Toteutumisajankohta");
                //$cols=$this->config->item('tlr_deve_show');
				$cols=array($this->config->item('tlr_deve_id')
						,$this->config->item('tlr_deve_status')
						,$this->config->item('tlr_deve_dev')
						,$this->config->item('tlr_deve_dev_name')
						,$this->config->item('tlr_deve_performer')
						,$this->config->item('tlr_deve_resp')
						,$this->config->item('tlr_deve_act')
						,$this->config->item('tlr_deve_dev_env')
						,$this->config->item('tlr_deve_target_time')
						,$this->config->item('tlr_deve_act_date')
						);
*/
				$header=array("Laitekoodi","Laitteen nimi","Laiteympäristö","Rakennus","Huone","Vastuuhenkilö","Aktiviteetti","Tavoiteajankohta","Tekijä/yhteyshenkilö");
                //$cols=$this->config->item('tlr_deve_show');
				$cols=array(
						$this->config->item('tlr_deve_dev')
						,$this->config->item('tlr_deve_dev_name')
						,$this->config->item('tlr_deve_dev_env')
						,$this->config->item('tlr_deve_building')
						,$this->config->item('tlr_deve_room')
						,$this->config->item('tlr_deve_resp')
						,$this->config->item('tlr_deve_act')
						,$this->config->item('tlr_deve_target_time')
						,$this->config->item('tlr_deve_performer')

						);

				$output=$tlr_data["lt_all_xml"];
			}else if($type=='ell_lo') {
				$pass=false;
				$output=$tlr_data["ell_lo_xml"];
			}else if($type=='ell_no_lo') {
				$pass=false;
				$output=$tlr_data["ell_no_lo_xml"];
			}

			//group
			//Group defaults
			if($pass){
				$pass=false;
			/*	$header=array("LaiteID","Laitevastuuhenkilö","Tila","Laitteen nimi","Laiteympäristö","Kategoria","Valmistaja","Käyttötarkoitus","Riskitarkastelu");
				$cols=array($this->config->item('tlr_dev_id')
						,$this->config->item('tlr_dev_resp')
						,$this->config->item('tlr_dev_status')
						,$this->config->item('tlr_dev_name')
						,$this->config->item('tlr_dev_laitekoord')
						,$this->config->item('tlr_dev_cat')
						,$this->config->item('tlr_dev_man')
						,$this->config->item('tlr_dev_name')
						,$this->config->item('tlr_dev_rt_stat'));*/
				$header=array("LaiteID","Laitteen nimi","Kategoria","Erityisluokka","Tila","Riskien arviointi","Rakennus","Huone","Laiteympäristö","Laitekoordinaattori","Laitevastuuhenkilö","Yhteyshenkilö viranomaiseen");
				$cols=array($this->config->item('tlr_dev_id')
							,$this->config->item('tlr_dev_name')
							,$this->config->item('tlr_dev_cat')
							,$this->config->item('apu_tlr_dev_erit_luokka')
							,$this->config->item('tlr_dev_status')
							,$this->config->item('tlr_dev_rt_stat')
							,$this->config->item('tlr_dev_building')
							,$this->config->item('tlr_dev_room')
							,$this->config->item('tlr_dev_env')
							,$this->config->item('tlr_dev_env').":tlr_raportit_laitekoordinaattorit"
							,$this->config->item('apu_tlr_dev_laitevastuuhlo')
							,$this->config->item('apu_tlr_authority_registered_person')
							);
						
			}

			if($type=='gr_tot') {
				$output=$group_tlr_data["tot_xml"];
				//$output = $this->tlr->get_tlr_devices($user,$isSuperiorTo);
			}else if($type=='gr_risk') {
/*				$header=array("LaiteID","Laitevastuuhenkilö","Tila","Laitteen nimi","Laiteympäristö","Kategoria","Valmistaja","Käyttötarkoitus","Riskitarkastelu","Riskitarkastelun tekijä");
                $cols=array($this->config->item('tlr_dev_id')
						,$this->config->item('tlr_dev_resp')
						,$this->config->item('tlr_dev_status')
						,$this->config->item('tlr_dev_name')
						,$this->config->item('tlr_dev_laitekoord')
						,$this->config->item('tlr_dev_cat')
						,$this->config->item('tlr_dev_man')
						,$this->config->item('tlr_dev_name')
						,$this->config->item('tlr_dev_rt_stat')
						,$this->config->item('tlr_dev_rt_stat_by')
						);
*/
				$header=array("LaiteID","Laitteen nimi","Kategoria","Erityisluokka","Tila","Riskien arviointi","Rakennus","Huone","Laitevastuuhenkilö","Riskitarkastelun tekijä","Riskitarkastelun tulos","Riskitarkastelun päivämäärä","Linkki riskitarkasteludokumenttiin");
				$cols=array($this->config->item('tlr_dev_id')
							,$this->config->item('tlr_dev_name')
							,$this->config->item('tlr_dev_cat')
							,$this->config->item('apu_tlr_dev_erit_luokka')
							,$this->config->item('tlr_dev_status')
							,$this->config->item('tlr_dev_rt_stat')
							,$this->config->item('tlr_dev_building')
							,$this->config->item('tlr_dev_room')
							,$this->config->item('apu_tlr_dev_laitevastuuhlo')
							,$this->config->item('tlr_dev_rt_madeby')
							,$this->config->item('tlr_dev_rt_res')
							,$this->config->item('tlr_dev_rt_date')
							,$this->config->item('tlr_dev_rt_doc')
							);

				$output=$group_tlr_data["risk_xml"];
			}else if($type=='gr_risk_no') {
				$header=array("LaiteID","Laitteen nimi","Kategoria","Erityisluokka","Tila","Riskien arviointi","Rakennus","Huone","Laitevastuuhenkilö","Riskitarkastelun tekijä","Riskitarkastelun tulos","Riskitarkastelun päivämäärä","Linkki riskitarkasteludokumenttiin");
				$cols=array($this->config->item('tlr_dev_id')
							,$this->config->item('tlr_dev_name')
							,$this->config->item('tlr_dev_cat')
							,$this->config->item('apu_tlr_dev_erit_luokka')
							,$this->config->item('tlr_dev_status')
							,$this->config->item('tlr_dev_rt_stat')
							,$this->config->item('tlr_dev_building')
							,$this->config->item('tlr_dev_room')
							,$this->config->item('apu_tlr_dev_laitevastuuhlo')
							,$this->config->item('tlr_dev_rt_madeby')
							,$this->config->item('tlr_dev_rt_res')
							,$this->config->item('tlr_dev_rt_date')
							,$this->config->item('tlr_dev_rt_doc')
							);

				$output=$group_tlr_data["no_risk_xml"];
			//}else if($type=='gr_special_EX-laite') {
				//$output=$tlr_data["specials"]['gr_special_EX-laite'];
			}else if($type=='gr_del') {
/*				$header=array("LaiteID","Tila","Laitteen nimi","Laiteympäristö","Kategoria","Valmistaja","Käyttötarkoitus");
				$cols=array($this->config->item('tlr_dev_id')
							,$this->config->item('tlr_dev_status')
							,$this->config->item('tlr_dev_name')
							,$this->config->item('tlr_dev_laitekoord')
							,$this->config->item('tlr_dev_cat')
							,$this->config->item('tlr_dev_man')
							,$this->config->item('tlr_dev_name')
							);*/
				$header=array("LaiteID","Laitteen nimi","Kategoria","Erityisluokka","Ostopäivä","Poistopäivä","Riskien arviointi","Rakennus","Huone","Laiteympäristö","Laitevastuuhenkilö");//,"LVH:n tiimi"
				$cols=array($this->config->item('tlr_dev_id')
							,$this->config->item('tlr_dev_name')
							,$this->config->item('tlr_dev_cat')
							,$this->config->item('apu_tlr_dev_erit_luokka')
							,$this->config->item('tlr_dev_pur_date')
							,$this->config->item('tlr_dev_status_change')
							,$this->config->item('tlr_dev_rt_stat')
							,$this->config->item('tlr_dev_building')
							,$this->config->item('tlr_dev_room')
							,$this->config->item('tlr_dev_env')
							,$this->config->item('apu_tlr_dev_laitevastuuhlo')
							//,$this->config->item('tlr_dev_resp_team')
							);
							
				$output=$group_tlr_data["del_xml"];
			}else if($type=='gr_wait') {
/*				$header=array("LaiteID","Laitevastuuhenkilö","Tila","Laitteen nimi","Kategoria","Valmistaja","Käyttötarkoitus","Riskitarkastelu");
                $cols=array($this->config->item('tlr_dev_id')
						,$this->config->item('tlr_dev_resp')
						,$this->config->item('tlr_dev_status')
						,$this->config->item('tlr_dev_name')
						,$this->config->item('tlr_dev_cat')
						,$this->config->item('tlr_dev_man')
						,$this->config->item('tlr_dev_name')
						,$this->config->item('tlr_dev_rt_stat'));
*/
				$header=array("LaiteID","Laitteen nimi","Tila","Laitevastuuhenkilö","Rakennus","Laiterekisterinumero(Kasperi)","Kohdenumero(Kasperi)","Ostopäivä","Hinta","Toimittaja");//"LVH:n tiimi",
				$cols=array($this->config->item('tlr_dev_id')
							,$this->config->item('tlr_dev_name')
							,$this->config->item('tlr_dev_status')
							,$this->config->item('apu_tlr_dev_laitevastuuhlo')
							//,$this->config->item('tlr_dev_resp_team')
							,$this->config->item('tlr_dev_building')
							,$this->config->item('tlr_dev_kas_laitenro')
							,$this->config->item('tlr_dev_kas_kohdenro')
							,$this->config->item('tlr_dev_pur_date')
							,$this->config->item('tlr_dev_pur_price')
							,$this->config->item('tlr_dev_pur_supl')
							);

				$output=$group_tlr_data["ko_xml"];
			}else if($type=='gr_kk') {
				$output=$group_tlr_data["kk_xml"];
			}else if($type=='gr_ell') {
				$output=$group_tlr_data["ell_xml"];
			}else if($type=='gr_open_jobs') {
/*				$header=array("Laitetapahtuman ID","Tila","Laitevastuuhenkilö","Laitekoodi","Laitteen nimi","Tekijä/yhdyshenkilö","Vastuuhenkilö","Aktiviteetti","Vastuuhenkilö","Tavoiteajankohta","Toteutumisajankohta");
                //$cols=$this->config->item('tlr_deve_show');
				$cols=array($this->config->item('tlr_deve_id')
						,$this->config->item('tlr_deve_status')
						,$this->config->item('tlr_deve_resp')
						,$this->config->item('tlr_deve_dev')
						,$this->config->item('tlr_deve_dev_name')
						,$this->config->item('tlr_deve_performer')
						,$this->config->item('tlr_deve_resp')
						,$this->config->item('tlr_deve_act')
						,$this->config->item('tlr_deve_dev_env')
						,$this->config->item('tlr_deve_target_time')
						,$this->config->item('tlr_deve_act_date')
						);*/
				$header=array("Laitekoodi","Laitteen nimi","Laiteympäristö","Rakennus","Huone","Vastuuhenkilö","Aktiviteetti","Tavoiteajankohta","Tekijä/yhteyshenkilö");
                //$cols=$this->config->item('tlr_deve_show');
				$cols=array(
						$this->config->item('tlr_deve_dev')
						,$this->config->item('tlr_deve_dev_name')
						,$this->config->item('tlr_deve_dev_env')
						,$this->config->item('tlr_deve_building')
						,$this->config->item('tlr_deve_room')
						,$this->config->item('tlr_deve_resp')
						,$this->config->item('tlr_deve_act')
						,$this->config->item('tlr_deve_target_time')
						,$this->config->item('tlr_deve_performer')

						);

				$output=$group_tlr_data["lt_xml"];
			}else if($type=='gr_open_jobs_all') {
/*				$header=array("Laitetapahtuman ID","Tila","Laitevastuuhenkilö","Laitekoodi","Laitteen nimi","Tekijä/yhdyshenkilö","Vastuuhenkilö","Aktiviteetti","Vastuuhenkilö","Tavoiteajankohta","Toteutumisajankohta");
                //$cols=$this->config->item('tlr_deve_show');
				$cols=array($this->config->item('tlr_deve_id')
						,$this->config->item('tlr_deve_status')
						,$this->config->item('tlr_deve_resp')
						,$this->config->item('tlr_deve_dev')
						,$this->config->item('tlr_deve_dev_name')
						,$this->config->item('tlr_deve_performer')
						,$this->config->item('tlr_deve_resp')
						,$this->config->item('tlr_deve_act')
						,$this->config->item('tlr_deve_dev_env')
						,$this->config->item('tlr_deve_target_time')
						,$this->config->item('tlr_deve_act_date')
						);
*/
				$header=array("Laitekoodi","Laitteen nimi","Laiteympäristö","Rakennus","Huone","Vastuuhenkilö","Aktiviteetti","Tavoiteajankohta","Tekijä/yhteyshenkilö");
                //$cols=$this->config->item('tlr_deve_show');
				$cols=array(
						$this->config->item('tlr_deve_dev')
						,$this->config->item('tlr_deve_dev_name')
						,$this->config->item('tlr_deve_dev_env')
						,$this->config->item('tlr_deve_building')
						,$this->config->item('tlr_deve_room')
						,$this->config->item('tlr_deve_resp')
						,$this->config->item('tlr_deve_act')
						,$this->config->item('tlr_deve_target_time')
						,$this->config->item('tlr_deve_performer')

						);

				$output=$group_tlr_data["lt_all_xml"];
			}else if($type=='dev_rem') {
				$output=$group_tlr_data["tot_not_in_service_xml"];
			}else if($type=='gr_ell_lo') {
				$output=$group_tlr_data["ell_lo_xml"];
			}else if($type=='gr_ell_no_lo') {
				$output=$group_tlr_data["ell_no_lo_xml"];
			}
			
			//quality
			//Quality defaults
			if($pass){
				$pass=false;
/*				$header=array("LaiteID","Laitevastuuhenkilö","Tila","Laitteen nimi","Laiteympäristö","Kategoria","Valmistaja","Käyttötarkoitus","Riskitarkastelu");
				$cols=array($this->config->item('tlr_dev_id')
						,$this->config->item('tlr_dev_resp')
						,$this->config->item('tlr_dev_status')
						,$this->config->item('tlr_dev_name')
						,$this->config->item('tlr_dev_laitekoord')
						,$this->config->item('tlr_dev_cat')
						,$this->config->item('tlr_dev_man')
						,$this->config->item('tlr_dev_name')
						,$this->config->item('tlr_dev_rt_stat'));*/
				$header=array("LaiteID","Laitteen nimi","Kategoria","Erityisluokka","Tila","Riskien arviointi","Rakennus","Huone","Laiteympäristö","Laitekoordinaattori","Laitevastuuhenkilö");//,"LVH:n tiimi"
				$cols=array($this->config->item('tlr_dev_id')
							,$this->config->item('tlr_dev_name')
							,$this->config->item('tlr_dev_cat')
							,$this->config->item('apu_tlr_dev_erit_luokka')
							,$this->config->item('tlr_dev_status')
							,$this->config->item('tlr_dev_rt_stat')
							,$this->config->item('tlr_dev_building')
							,$this->config->item('tlr_dev_room')
							,$this->config->item('tlr_dev_env')
							,$this->config->item('tlr_dev_env').":tlr_raportit_laitekoordinaattorit"
							,$this->config->item('apu_tlr_dev_laitevastuuhlo')
							//,$this->config->item('tlr_dev_resp_team')
							);
						
			}

			if($type=='ql_tot') {
				$output=$quality_tlr_data["tot_xml"];
				//$output = $this->tlr->get_tlr_devices($user,$isSuperiorTo);
			}else if($type=='ql_risk') {
/*				$header=array("LaiteID","Laitevastuuhenkilö","Tila","Laitteen nimi","Laiteympäristö","Kategoria","Valmistaja","Käyttötarkoitus","Riskitarkastelu","Riskitarkastelun tekijä");
                $cols=array($this->config->item('tlr_dev_id')
						,$this->config->item('tlr_dev_resp')
						,$this->config->item('tlr_dev_status')
						,$this->config->item('tlr_dev_name')
						,$this->config->item('tlr_dev_laitekoord')
						,$this->config->item('tlr_dev_cat')
						,$this->config->item('tlr_dev_man')
						,$this->config->item('tlr_dev_name')
						,$this->config->item('tlr_dev_rt_stat')
						,$this->config->item('tlr_dev_rt_stat_by')
						);
*/
				$header=array("LaiteID","Laitteen nimi","Kategoria","Erityisluokka","Tila","Riskien arviointi","Rakennus","Huone","Laitevastuuhenkilö","Riskitarkastelun tekijä","Riskitarkastelun tulos","Riskitarkastelun päivämäärä","Linkki riskitarkasteludokumenttiin");
				$cols=array($this->config->item('tlr_dev_id')
							,$this->config->item('tlr_dev_name')
							,$this->config->item('tlr_dev_cat')
							,$this->config->item('apu_tlr_dev_erit_luokka')
							,$this->config->item('tlr_dev_status')
							,$this->config->item('tlr_dev_rt_stat')
							,$this->config->item('tlr_dev_building')
							,$this->config->item('tlr_dev_room')
							,$this->config->item('apu_tlr_dev_laitevastuuhlo')
							,$this->config->item('tlr_dev_rt_madeby')
							,$this->config->item('tlr_dev_rt_res')
							,$this->config->item('tlr_dev_rt_date')
							,$this->config->item('tlr_dev_rt_doc')
							);
				$output=$quality_tlr_data["risk_xml"];
			}else if($type=='ql_risk_no') {
				$header=array("LaiteID","Laitteen nimi","Kategoria","Erityisluokka","Tila","Riskien arviointi","Rakennus","Huone","Laitevastuuhenkilö","Riskitarkastelun tekijä","Riskitarkastelun tulos","Riskitarkastelun päivämäärä","Linkki riskitarkasteludokumenttiin");
				$cols=array($this->config->item('tlr_dev_id')
							,$this->config->item('tlr_dev_name')
							,$this->config->item('tlr_dev_cat')
							,$this->config->item('apu_tlr_dev_erit_luokka')
							,$this->config->item('tlr_dev_status')
							,$this->config->item('tlr_dev_rt_stat')
							,$this->config->item('tlr_dev_building')
							,$this->config->item('tlr_dev_room')
							,$this->config->item('apu_tlr_dev_laitevastuuhlo')
							,$this->config->item('tlr_dev_rt_madeby')
							,$this->config->item('tlr_dev_rt_res')
							,$this->config->item('tlr_dev_rt_date')
							,$this->config->item('tlr_dev_rt_doc')
							);
				$output=$quality_tlr_data["no_risk_xml"];
			}else if($type=='ql_del') {
/*				$header=array("LaiteID","Tila","Laitteen nimi","Laiteympäristö","Kategoria","Valmistaja","Käyttötarkoitus");
				$cols=array($this->config->item('tlr_dev_id')
							,$this->config->item('tlr_dev_status')
							,$this->config->item('tlr_dev_name')
							,$this->config->item('tlr_dev_laitekoord')
							,$this->config->item('tlr_dev_cat')
							,$this->config->item('tlr_dev_man')
							,$this->config->item('tlr_dev_name')
							);
							*/
				$header=array("LaiteID","Laitteen nimi","Kategoria","Erityisluokka","Ostopäivä","Poistopäivä","Riskien arviointi","Rakennus","Huone","Laiteympäristö","Laitevastuuhenkilö");//,"LVH:n tiimi"
				$cols=array($this->config->item('tlr_dev_id')
							,$this->config->item('tlr_dev_name')
							,$this->config->item('tlr_dev_cat')
							,$this->config->item('apu_tlr_dev_erit_luokka')
							,$this->config->item('tlr_dev_pur_date')
							,$this->config->item('tlr_dev_status_change')
							,$this->config->item('tlr_dev_rt_stat')
							,$this->config->item('tlr_dev_building')
							,$this->config->item('tlr_dev_room')
							,$this->config->item('tlr_dev_env')
							,$this->config->item('apu_tlr_dev_laitevastuuhlo')
							//,$this->config->item('tlr_dev_resp_team')
							);
							
				$output=$quality_tlr_data["del_xml"];
			}else if($type=='ql_dev_rem') {
				$output=$quality_tlr_data["tot_not_in_service_xml"];
			}else if($type=='ql_unass') {
				$header=array("Laiteympäristö","Laiteympäristön kuvaus","Laiteympäristön toiminto","Laiteympäristön tutkimusalue","Laitekoordinaattori");
				$cols=array($this->config->item('tlr_devy_name')
							,$this->config->item('tlr_devy_desc')
							,$this->config->item('tlr_devy_toim')
							,$this->config->item('tlr_devy_tutk')
							,$this->config->item('tlr_devy_koord')
							);
//var_dump($quality_tlr_data["tot_unassigned_xml"]);
				$output=$quality_tlr_data["tot_unassigned_xml"];
			}else if($type=='ql_wait') {
/*				$header=array("LaiteID","Laitevastuuhenkilö","Tila","Laitteen nimi","Kategoria","Valmistaja","Käyttötarkoitus","Riskitarkastelu");
                $cols=array($this->config->item('tlr_dev_id')
						,$this->config->item('tlr_dev_resp')
						,$this->config->item('tlr_dev_status')
						,$this->config->item('tlr_dev_name')
						,$this->config->item('tlr_dev_cat')
						,$this->config->item('tlr_dev_man')
						,$this->config->item('tlr_dev_name')
						,$this->config->item('tlr_dev_rt_stat'));
*/
				$header=array("LaiteID","Laitteen nimi","Tila","Laitevastuuhenkilö","Rakennus","Laiterekisterinumero(Kasperi)","Kohdenumero(Kasperi)","Ostopäivä","Hinta","Toimittaja");//"LVH:n tiimi",
				$cols=array($this->config->item('tlr_dev_id')
							,$this->config->item('tlr_dev_name')
							,$this->config->item('tlr_dev_status')
							,$this->config->item('apu_tlr_dev_laitevastuuhlo')
							//,$this->config->item('tlr_dev_resp_team')
							,$this->config->item('tlr_dev_building')
							,$this->config->item('tlr_dev_kas_laitenro')
							,$this->config->item('tlr_dev_kas_kohdenro')
							,$this->config->item('tlr_dev_pur_date')
							,$this->config->item('tlr_dev_pur_price')
							,$this->config->item('tlr_dev_pur_supl')
							);

				$output=$quality_tlr_data["ko_xml"];
			}else if($type=='ql_kk') {
				$output=$quality_tlr_data["kk_xml"];
			}else if($type=='ql_no_lvh') {
				$output=$quality_tlr_data["arr_no_lvh_xml"];
			}else if($type=='ql_ell') {
				$output=$quality_tlr_data["ell_xml"];
			}else if($type=='ql_open_jobs') {
				$header=array("Laitetapahtuman ID","Tila","Laitevastuuhenkilö","Laitekoodi","Laitteen nimi","Tekijä/yhdyshenkilö","Vastuuhenkilö","Aktiviteetti","Vastuuhenkilö","Tavoiteajankohta","Toteutumisajankohta");
                //$cols=$this->config->item('tlr_deve_show');
				$cols=array($this->config->item('tlr_deve_id')
						,$this->config->item('tlr_deve_status')
						,$this->config->item('tlr_deve_resp')
						,$this->config->item('tlr_deve_dev')
						,$this->config->item('tlr_deve_dev_name')
						,$this->config->item('tlr_deve_performer')
						,$this->config->item('tlr_deve_resp')
						,$this->config->item('tlr_deve_act')
						,$this->config->item('tlr_deve_dev_env')
						,$this->config->item('tlr_deve_target_time')
						,$this->config->item('tlr_deve_act_date')
						);

				$output=$quality_tlr_data["lt_xml"];
			}else if($type=='ql_open_jobs_all') {
/*				$header=array("Laitetapahtuman ID","Tila","Laitevastuuhenkilö","Laitekoodi","Laitteen nimi","Tekijä/yhdyshenkilö","Vastuuhenkilö","Aktiviteetti","Vastuuhenkilö","Tavoiteajankohta","Toteutumisajankohta");
                //$cols=$this->config->item('tlr_deve_show');
				$cols=array($this->config->item('tlr_deve_id')
						,$this->config->item('tlr_deve_status')
						,$this->config->item('tlr_deve_resp')
						,$this->config->item('tlr_deve_dev')
						,$this->config->item('tlr_deve_dev_name')
						,$this->config->item('tlr_deve_performer')
						,$this->config->item('tlr_deve_resp')
						,$this->config->item('tlr_deve_act')
						,$this->config->item('tlr_deve_dev_env')
						,$this->config->item('tlr_deve_target_time')
						,$this->config->item('tlr_deve_act_date')
						);
*/
				$header=array("Laitekoodi","Laitteen nimi","Laiteympäristö","Rakennus","Huone","Vastuuhenkilö","Aktiviteetti","Tavoiteajankohta","Tekijä/yhteyshenkilö");
                //$cols=$this->config->item('tlr_deve_show');
				$cols=array(
						$this->config->item('tlr_deve_dev')
						,$this->config->item('tlr_deve_dev_name')
						,$this->config->item('tlr_deve_dev_env')
						,$this->config->item('tlr_deve_building')
						,$this->config->item('tlr_deve_room')
						,$this->config->item('tlr_deve_resp')
						,$this->config->item('tlr_deve_act')
						,$this->config->item('tlr_deve_target_time')
						,$this->config->item('tlr_deve_performer')

						);

				$output=$quality_tlr_data["lt_all_xml"];
			}else if($type=='ql_rem') {
				$output=$quality_tlr_data["tot_not_in_service_xml"];
			}else if($type=='ql_ell_lo') {
				$output=$quality_tlr_data["ell_lo_xml"];
			}else if($type=='ql_ell_no_lo') {
				$output=$quality_tlr_data["ell_no_lo_xml"];
			}

			
			//coord
			//Coord defaults
			if($pass){
/*				$header=array("LaiteID","Laitevastuuhenkilö","Tila","Laitteen nimi","Laiteympäristö","Kategoria","Valmistaja","Käyttötarkoitus","Riskitarkastelu");
				$cols=array($this->config->item('tlr_dev_id')
						,$this->config->item('tlr_dev_resp')
						,$this->config->item('tlr_dev_status')
						,$this->config->item('tlr_dev_name')
						,$this->config->item('tlr_dev_laitekoord')
						,$this->config->item('tlr_dev_cat')
						,$this->config->item('tlr_dev_man')
						,$this->config->item('tlr_dev_name')
						,$this->config->item('tlr_dev_rt_stat'));*/
				$header=array("LaiteID","Laitteen nimi","Kategoria","Erityisluokka","Tila","Riskien arviointi","Rakennus","Huone","Laiteympäristö","Laitekoordinaattori","Laitevastuuhenkilö");//,"LVH:n tiimi"
				$cols=array($this->config->item('tlr_dev_id')
							,$this->config->item('tlr_dev_name')
							,$this->config->item('tlr_dev_cat')
							,$this->config->item('apu_tlr_dev_erit_luokka')
							,$this->config->item('tlr_dev_status')
							,$this->config->item('tlr_dev_rt_stat')
							,$this->config->item('tlr_dev_building')
							,$this->config->item('tlr_dev_room')
							,$this->config->item('tlr_dev_env')
							,$this->config->item('tlr_dev_env').":tlr_raportit_laitekoordinaattorit"
							,$this->config->item('apu_tlr_dev_laitevastuuhlo')
							//,$this->config->item('tlr_dev_resp_team')
							);
						
			}

			if($type=='co_tot') {
				$output=$coord_tlr_data["tot_xml"];
				//$output = $this->tlr->get_tlr_devices($user,$isSuperiorTo);
			}else if($type=='co_risk') {
/*				$header=array("LaiteID","Laitevastuuhenkilö","Tila","Laitteen nimi","Laiteympäristö","Kategoria","Valmistaja","Käyttötarkoitus","Riskitarkastelu","Riskitarkastelun tekijä");
                $cols=array($this->config->item('tlr_dev_id')
						,$this->config->item('tlr_dev_resp')
						,$this->config->item('tlr_dev_status')
						,$this->config->item('tlr_dev_name')
						,$this->config->item('tlr_dev_laitekoord')
						,$this->config->item('tlr_dev_cat')
						,$this->config->item('tlr_dev_man')
						,$this->config->item('tlr_dev_name')
						,$this->config->item('tlr_dev_rt_stat')
						,$this->config->item('tlr_dev_rt_stat_by')
						);
*/
				$header=array("LaiteID","Laitteen nimi","Kategoria","Erityisluokka","Tila","Riskien arviointi","Rakennus","Huone","Laitevastuuhenkilö","Riskitarkastelun tekijä","Riskitarkastelun tulos","Riskitarkastelun päivämäärä","Linkki riskitarkasteludokumenttiin");
				$cols=array($this->config->item('tlr_dev_id')
							,$this->config->item('tlr_dev_name')
							,$this->config->item('tlr_dev_cat')
							,$this->config->item('apu_tlr_dev_erit_luokka')
							,$this->config->item('tlr_dev_status')
							,$this->config->item('tlr_dev_rt_stat')
							,$this->config->item('tlr_dev_building')
							,$this->config->item('tlr_dev_room')
							,$this->config->item('apu_tlr_dev_laitevastuuhlo')
							,$this->config->item('tlr_dev_rt_madeby')
							,$this->config->item('tlr_dev_rt_res')
							,$this->config->item('tlr_dev_rt_date')
							,$this->config->item('tlr_dev_rt_doc')
							);

				$output=$coord_tlr_data["risk_xml"];
			}else if($type=='co_risk_no') {
				$header=array("LaiteID","Laitteen nimi","Kategoria","Erityisluokka","Tila","Riskien arviointi","Rakennus","Huone","Laitevastuuhenkilö","Riskitarkastelun tekijä","Riskitarkastelun tulos","Riskitarkastelun päivämäärä","Linkki riskitarkasteludokumenttiin");
				$cols=array($this->config->item('tlr_dev_id')
							,$this->config->item('tlr_dev_name')
							,$this->config->item('tlr_dev_cat')
							,$this->config->item('apu_tlr_dev_erit_luokka')
							,$this->config->item('tlr_dev_status')
							,$this->config->item('tlr_dev_rt_stat')
							,$this->config->item('tlr_dev_building')
							,$this->config->item('tlr_dev_room')
							,$this->config->item('apu_tlr_dev_laitevastuuhlo')
							,$this->config->item('tlr_dev_rt_madeby')
							,$this->config->item('tlr_dev_rt_res')
							,$this->config->item('tlr_dev_rt_date')
							,$this->config->item('tlr_dev_rt_doc')
							);
				$output=$coord_tlr_data["no_risk_xml"];
			}else if($type=='co_del') {
/*				$header=array("LaiteID","Tila","Laitteen nimi","Laiteympäristö","Kategoria","Valmistaja","Käyttötarkoitus");
				$cols=array($this->config->item('tlr_dev_id')
							,$this->config->item('tlr_dev_status')
							,$this->config->item('tlr_dev_name')
							,$this->config->item('tlr_dev_laitekoord')
							,$this->config->item('tlr_dev_cat')
							,$this->config->item('tlr_dev_man')
							,$this->config->item('tlr_dev_name')
							);
*/
				$header=array("LaiteID","Laitteen nimi","Kategoria","Erityisluokka","Ostopäivä","Poistopäivä","Riskien arviointi","Rakennus","Huone","Laiteympäristö","Laitevastuuhenkilö");//,"LVH:n tiimi"
				$cols=array($this->config->item('tlr_dev_id')
							,$this->config->item('tlr_dev_name')
							,$this->config->item('tlr_dev_cat')
							,$this->config->item('apu_tlr_dev_erit_luokka')
							,$this->config->item('tlr_dev_pur_date')
							,$this->config->item('tlr_dev_status_change')
							,$this->config->item('tlr_dev_rt_stat')
							,$this->config->item('tlr_dev_building')
							,$this->config->item('tlr_dev_room')
							,$this->config->item('tlr_dev_env')
							,$this->config->item('apu_tlr_dev_laitevastuuhlo')
							//,$this->config->item('tlr_dev_resp_team')
							);

				$output=$coord_tlr_data["del_xml"];
			}else if($type=='co_wait') {
/*				$header=array("LaiteID","Laitevastuuhenkilö","Tila","Laitteen nimi","Kategoria","Valmistaja","Käyttötarkoitus","Riskitarkastelu");
               $cols=array($this->config->item('tlr_dev_id')
						,$this->config->item('tlr_dev_resp')
						,$this->config->item('tlr_dev_status')
						,$this->config->item('tlr_dev_name')
						,$this->config->item('tlr_dev_cat')
						,$this->config->item('tlr_dev_man')
						,$this->config->item('tlr_dev_name')
						,$this->config->item('tlr_dev_rt_stat'));
*/
				$header=array("LaiteID","Laitteen nimi","Tila","Laitevastuuhenkilö","Rakennus","Laiterekisterinumero(Kasperi)","Kohdenumero(Kasperi)","Ostopäivä","Hinta","Toimittaja");//"LVH:n tiimi",
				$cols=array($this->config->item('tlr_dev_id')
							,$this->config->item('tlr_dev_name')
							,$this->config->item('tlr_dev_status')
							,$this->config->item('apu_tlr_dev_laitevastuuhlo')
							//,$this->config->item('tlr_dev_resp_team')
							,$this->config->item('tlr_dev_building')
							,$this->config->item('tlr_dev_kas_laitenro')
							,$this->config->item('tlr_dev_kas_kohdenro')
							,$this->config->item('tlr_dev_pur_date')
							,$this->config->item('tlr_dev_pur_price')
							,$this->config->item('tlr_dev_pur_supl')
							);

				$output=$coord_tlr_data["ko_xml"];
			}else if($type=='co_kk') {
				$output=$coord_tlr_data["kk_xml"];
			}else if($type=='co_no_lvh') {
				$output=$coord_tlr_data["arr_no_lvh_xml"];				
			}else if($type=='co_ell') {
				$output=$coord_tlr_data["ell_xml"];
			}else if($type=='co_open_jobs') {
				$header=array("Laitetapahtuman ID","Tila","Laitevastuuhenkilö","Laitekoodi","Laitteen nimi","Tekijä/yhdyshenkilö","Vastuuhenkilö","Aktiviteetti","Vastuuhenkilö","Tavoiteajankohta","Toteutumisajankohta");
                //$cols=$this->config->item('tlr_deve_show');
				$cols=array($this->config->item('tlr_deve_id')
						,$this->config->item('tlr_deve_status')
						,$this->config->item('tlr_deve_resp')
						,$this->config->item('tlr_deve_dev')
						,$this->config->item('tlr_deve_dev_name')
						,$this->config->item('tlr_deve_performer')
						,$this->config->item('tlr_deve_resp')
						,$this->config->item('tlr_deve_act')
						,$this->config->item('tlr_deve_dev_env')
						,$this->config->item('tlr_deve_target_time')
						,$this->config->item('tlr_deve_act_date')
						);

				$output=$coord_tlr_data["lt_xml"];
			}else if($type=='co_open_jobs_all') {
/*				$header=array("Laitetapahtuman ID","Tila","Laitevastuuhenkilö","Laitekoodi","Laitteen nimi","Tekijä/yhdyshenkilö","Vastuuhenkilö","Aktiviteetti","Vastuuhenkilö","Tavoiteajankohta","Toteutumisajankohta");
                //$cols=$this->config->item('tlr_deve_show');
				$cols=array($this->config->item('tlr_deve_id')
						,$this->config->item('tlr_deve_status')
						,$this->config->item('tlr_deve_resp')
						,$this->config->item('tlr_deve_dev')
						,$this->config->item('tlr_deve_dev_name')
						,$this->config->item('tlr_deve_performer')
						,$this->config->item('tlr_deve_resp')
						,$this->config->item('tlr_deve_act')
						,$this->config->item('tlr_deve_dev_env')
						,$this->config->item('tlr_deve_target_time')
						,$this->config->item('tlr_deve_act_date')
						);
*/
				$header=array("Laitekoodi","Laitteen nimi","Laiteympäristö","Rakennus","Huone","Vastuuhenkilö","Aktiviteetti","Tavoiteajankohta","Tekijä/yhteyshenkilö");
                //$cols=$this->config->item('tlr_deve_show');
				$cols=array(
						$this->config->item('tlr_deve_dev')
						,$this->config->item('tlr_deve_dev_name')
						,$this->config->item('tlr_deve_dev_env')
						,$this->config->item('tlr_deve_building')
						,$this->config->item('tlr_deve_room')
						,$this->config->item('tlr_deve_resp')
						,$this->config->item('tlr_deve_act')
						,$this->config->item('tlr_deve_target_time')
						,$this->config->item('tlr_deve_performer')

						);

				$output=$coord_tlr_data["lt_all_xml"];
			}else if($type=='co_rem') {
				$output=$coord_tlr_data["tot_not_in_service_xml"];
			}else if($type=='co_ell_lo') {
				$output=$coord_tlr_data["ell_lo_xml"];
			}else if($type=='co_ell_no_lo') {
				$output=$coord_tlr_data["ell_no_lo_xml"];
			}
			
			
			
			
			//				$output = $this->tlr->get_tlr_devices_risk($isSuperiorTo);
			//$output = $this->tlr->get_device_summary($user);
			//var_dump($output);
			$this->template->set('type', $type);
			$this->template->set('header', $header);
			$this->template->set('cols', $cols);
			$this->template->set('output', $output);
	        $this->template->render();
		}
	}

	public function get_subnavi() {
		if ($this->input->is_ajax_request() === true) {
			$this->load->model('user_model','user');
			$user = $this->user->getLogin(); 

			$this->load->model('efecte_model','efecte');

	        $this->template->render();
		}
	}
	
	public function save_close() {
		if ($this->input->is_ajax_request() === true) {
			$this->load->model('user_model','user');
			$user = $this->user->getLogin(); 

			$this->load->model('efecte_model','efecte');
			$p = $_POST;
	        unset($p['send']);

	        $params = array();

	        foreach ($p as $k => $v) {
	            $this->efecte->createSaveParams($k,'value',$v,$params);
	        }
	        $this->efecte->createSaveParams('contact','value',$user,$params);

	        $this->efecte->createSaveParams('template_code',false, $this->config->item('helpdesk_template'),$params);
	        $this->efecte->createSaveParams('folder_code',false,$this->config->item('helpdesk_folder'),$params);
	        $saveResult = $this->efecte->saveEntity($params);

	        $this->template->set('message',"Your input has been saved.");
	        $this->template->render();
		}
	}

	public function search() {
		if ($this->input->is_ajax_request() === true) {
			$this->load->model('knowledge_model','knowledge');
			$content = $this->knowledge->search_by_term($this->input->get('term'));
			$this->output->set_content_type('application/json;charset=utf-8');
			$this->template->set('ajax_content',json_encode($content));
			$this->template->render();
		}
	}

	public function search_knowledge() {
		if ($this->input->is_ajax_request() === true) {
			$this->load->model('user_model','user');
			$user = $this->user->getLogin(); // fetch the session username from browser
			$this->load->model('knowledge_model','knowledge');
			$content = $this->knowledge->get_single_article($this->input->post('entityid'));

			$this->template->set('article',$content);
			$this->template->render();
		}
	}

	public function search_request() {
		if ($this->input->is_ajax_request() === true) {
			$this->load->model('user_model','user');
			$user = $this->user->getLogin(); // fetch the session username from browser
			$this->load->model('request_model','request');
			$content = $this->request->get_single_request($this->input->post('entityid'), $user);

			$this->template->set('request',$content);
			$this->template->render();
		}
	}
	
	public function get_employees () {
		if ($this->input->is_ajax_request() === true) {
			$this->load->model('user_model','user');
			//$user = $this->user->getLogin();
			$user = $this->session->userdata('user');
			
			$searchUser = $this->input->post('search_user');
			if($searchUser === ""){
				$searchUser = $this->session->userdata('user');
			}				
			
			$data = $this->user->getDirectSuperiorTo($searchUser);
					
			$this->template->set('data',$data);
			$this->template->render();
		}
	}
	
	public function get_devices () {
		if ($this->input->is_ajax_request() === true) {
			$this->load->model('device_model','device');
			$this->load->model('user_model','user');
			$account = $this->input->post('account');
			
			//var_dump($account);
			
			if($account === ""){
				$group = "ext";
				$account = $this->session->userdata('user');
			} 

			$pc = $this->device->get_pc($account,$group);		
			$displ = $this->device->get_display($account,$group);		
			$mob = $this->device->get_mobile($account,$group);		
			$this->template->set('pc' ,$pc);
			$this->template->set('displ' ,$displ);
			$this->template->set('mob' ,$mob);
			
			$this->template->render();
		} 
	}


}