<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Download extends CI_Controller {

	public function file ($entityId = false, $attributeId = false, $fileName = false, $realFileName = false ) {
		
		$url = $this->config->item('file_dl').'?entityId='.$entityId.'&attributeId='.$attributeId.'&fileName='.$fileName;
	

		$headers = array(
			"POST HTTP/1.0",
			"Content-type: application/octec-stream",
			"Cache-Control: no-cache",
			"Pragma: no-cache",
			"Authorization: Basic " . base64_encode($this->config->item('webapi_user').':'.$this->config->item('webapi_pw'))
		);

		log_message('debug', 'URL: '.$url);
		if ( ! $realFileName )
			header('Content-disposition: attachment; filename='.$fileName);
		else 
			header('Content-disposition: attachment; filename='.$realFileName);
		header('Content-type: application/octec-stream');
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_URL,$url);
		$data = curl_exec($ch);
		die;
	}
}