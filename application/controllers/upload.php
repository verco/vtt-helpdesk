<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends CI_Controller {

	public function file ($debug = true) {

		import java.io.File;
		import org.apache.commons.httpclient.HttpClient;
		import org.apache.commons.httpclient.HttpStatus;
		import org.apache.commons.httpclient.methods.PostMethod;
		import org.apache.commons.httpclient.methods.multipart.StringPart;
		import org.apache.commons.httpclient.methods.multipart.FilePart;
		import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
		import org.apache.commons.httpclient.methods.multipart.Part;
		import org.apache.commons.httpclient.methods.multipart.PartSource;
		import org.apache.commons.httpclient.params.HttpMethodParams;
		import org.apache.commons.httpclient.auth.AuthScope;
		import org.apache.commons.httpclient.UsernamePasswordCredentials;
		log_message('debug',print_r($_POST));
		$entityId = trim($this->input->get_post('entityId'));
		$attributeId = $this->input->get_post('attributeId');

		if ($this->input->post("howMany") != false) {
			$howMany = $this->input->get_post('howMany');
		}
		else $howMany = 1;

		if ($debug) {
			log_message('debug','howMany: '.$howMany);
			log_message('debug',"attributeId: " . $attributeId);
			log_message('debug',"entityId: " . $entityId);
			log_message('debug', "file1: " . $_FILES["HD_FILE".$howMany]["name"]);
			log_message('debug', "file1 tmpname: " . $_FILES["HD_FILE".$howMany]["tmp_name"]);
		}

		$originalFileName = $_FILES["HD_FILE".$howMany]["name"];
		$fileName = $_FILES["HD_FILE".$howMany]["tmp_name"];

		$targetFile = new File($fileName);
		$targetURL = $this->config->item('file_ul');

		$filePost = new PostMethod($targetURL);

		if ($debug)	echo "Uploading " . $fileName . " to " . $targetURL . "\n";

		$filePart = new FilePart("HD_FILE".$howMany, $originalFileName, $targetFile);
		$filePart->setTransferEncoding(null);

		//	Part[] parts;

		$entityIdPart = new StringPart("entityId", $entityId);
		$entityIdPart->setTransferEncoding(null);

		$attributeIdPart = new StringPart("attributeId", $attributeId);
		$attributeIdPart->setTransferEncoding(null);

		$operationTypePart = new StringPart("operationType", 'add');
		$operationTypePart->setTransferEncoding(null);

		$parts = array($entityIdPart, $attributeIdPart, $operationTypePart, $filePart);

		$filePost->setRequestEntity(
			new MultipartRequestEntity($parts, $filePost->getParams())
		);
		$client = new HttpClient();
		$client->getState()->setCredentials(
			new AuthScope($this->config->item('ip'), 80, "Efecte Web Services"),
			new UsernamePasswordCredentials($this->config->item('webapi_user'), $this->config->item('webapi_pw'))
		);

		$client->getHttpConnectionManager()->getParams()->setConnectionTimeout(5000);
		$status = $client->executeMethod($filePost);
		if ($status == HttpStatus::SC_OK) {
			echo "Upload complete, response=" . $filePost->getResponseBodyAsString();
		} else {
			echo "Upload failed, response=" . HttpStatus::getStatusText($status);
		}
		
	}

}