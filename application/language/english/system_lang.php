<?
// MAIN

$lang['h1'] = 'Compliance Portal';

// DOCUMENT / LIST

$lang['doc_current_doc'] = 'Current document';
$lang['doc_version'] = 'Version';
$lang['doc_format'] = 'Format';
$lang['doc_lang'] = 'Document language';
$lang['doc_download_current'] = 'Download current document';
$lang['doc_email'] = 'Email current document';
$lang['doc_language'] = 'Language';
$lang['doc_description'] = 'Description';
