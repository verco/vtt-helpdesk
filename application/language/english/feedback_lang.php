<?php
// Feedback page
$lang['tabtext'] = "VTT Feedback request";
$lang['feedback_title'] = 'Feedback request';
$lang['feedback_subtitle'] = '- Ole hyvä ja anna palautteesi -';
$lang['subject'] = 'Title';
$lang['ticket_id'] = 'Incident ID';
$lang['description'] = 'Description';
$lang['solution'] = 'Resolution';
$lang['acceptance_question'] = 'Do you accept the incident performed?';
$lang['yes'] = 'Yes';
$lang['no'] = 'No';
$lang['expectations_question'] = 'How the service responded to your expectations?';
$lang['expectations_happy'] = 'Delighted';
$lang['expectations_ok'] = 'OK';
$lang['expectations_disappointed'] = 'Disappointed';
$lang['satisfaction_question'] = 'How satisfied are you with the service you received?';
$lang['satisfaction_legend'] = '(10 = very pleased, 1 = very dissatisfied)';
$lang['free_comment'] = 'Comments and greetings related to your support request';
$lang['additional_comment'] = 'Please provide details about your support request';
$lang['submit'] = 'Submit';
$lang['mandatory_field'] = 'mandatory field';

// Thank you page
$lang['thanks_title'] = 'Thank you for your feedback!';
$lang['thanks_text'] = 'Your comments are valuable in developing our service.';

$lang['error_title'] = 'Invalid identification information!';
$lang['error_text'] = 'The identification information of the incident is invalid or outdated.';

$lang['accepted_title'] = 'Service request feedback';
$lang['accepted_title_inc'] = 'Incident feedback';
$lang['accepted_text'] = 'This service request has already received positive feedback!';
$lang['accepted_text_inc'] = 'This incident has already received positive feedback!';
$lang['feedback_title2'] = 'VTT Information Management';