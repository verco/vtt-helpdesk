<?php
// Feedback page
$lang['tabtext'] = "VTT Palautepyyntö";
$lang['feedback_title'] = 'Palautepyyntö';
$lang['feedback_subtitle'] = '- Ole hyvä ja anna palautteesi -';
$lang['subject'] = 'Otsikko';
$lang['ticket_id'] = 'Tukipyynnön ID';
$lang['description'] = 'Kuvaus';
$lang['solution'] = 'Ratkaisu';
$lang['acceptance_question'] = 'Hyväksytkö tykipyynnön suoritetuksi?';
$lang['yes'] = 'Kyllä';
$lang['no'] = 'Ei';
$lang['expectations_question'] = 'Miten palvelu vastasi odotuksiasi?';
$lang['expectations_happy'] = 'Ilahduin';
$lang['expectations_ok'] = 'OK';
$lang['expectations_disappointed'] = 'Petyin';
$lang['satisfaction_question'] = 'Kuinka tyytyväinen olet saamaasi palveluun?';
$lang['satisfaction_legend'] = '(10 = erittäin tyytyväinen, 1 = erittäin tyytymätön)';
$lang['free_comment'] = 'Tukipyyntöösi liittyvät kommentit ja terveiset';
$lang['additional_comment'] = 'Anna lisätietoja tukipyyntöäsi koskien';
$lang['submit'] = 'Lähetä';
$lang['mandatory_field'] = 'pakollinen kenttä';

// Thank you page
$lang['thanks_title'] = 'Kiitos palautteestasi!';
$lang['thanks_text'] = 'Kommenttisi ovat meille arvokkaita palvelun kehittämisessä.';

$lang['error_title'] = 'Virheellinen tunnistetieto!';
$lang['error_text'] = 'Palautteeseen liittyvän tukipyynnön tunnistetieto on virheellinen tai vanhentunut.';

$lang['accepted_title'] = 'Tukipyynnön palaute';
$lang['accepted_text'] = 'Tukipyynnöstä on jo annettu positiivinen palaute. Kiitos palautteesta!';
$lang['feedback_title2'] = 'VTT Tietohallinto';