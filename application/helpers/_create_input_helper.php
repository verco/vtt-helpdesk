<?
function create_input($attribute_id = false, $name = false, $type = false, $classes = array(), $debug = false) {
    $CI =& get_instance();
    $CI->load->model('efecte_model','efecte');
    $CI->config->load('efecte');
    $params["full_search_string"] = "select value from com.efecte.datamodel.statics.StaticString where attribute.id = ".$attribute_id;
    $xml = new SimpleXMLElement($CI->efecte->readEntity($params,$debug));
    $arr = array();

    if (sizeof($classes) > 0) {
        $classes = implode(' ',$classes);
    } else $classes = '';

    foreach ($xml->string as $value) {
        $arr[$value.""] = $value."";
    }

    $ret = '';

    if ($type == 'radio') {
        foreach ($arr as $k => $v) {
            $ret .= "<label class='radio'>".$v."</label><input type='radio' id='".$name."_".strtolower(str_replace(array(' ','ä','ö'),array('_','a','o'),$v))."' name='$name' value='$v' class='$classes'><br class='clearleft'>";
        }
    } elseif ($type == 'select' || $type === false) {
        $ret = "<select name='$name' id='$name' class='$classes'>";
            foreach ($arr as $k => $v) {
                $ret .= "<option value='$v'>$v</option>";
            }
        $ret .= "</select>";

    } elseif ($type == 'checkbox') {
        foreach ($arr as $k => $v) {
            $ret .= "<div><input id='".$name."_".strtolower(str_replace(array(' ','ä','ö'),array('_','a','o'),$v))."' type='checkbox' class='$classes' name='".$name."[]' value='$v'><label>".$v."</lable></div><br />";
        }
    }
    return $ret;

}
