<?= $this->template->block('header','default/header') ?>
<?= $this->template->block('nav','default/sidebar') ?>
<div id="content">

	<div id="request_desc">
		<h1><?= $entity['###entity_name###'] ?></h1>
		<h2><?= $entity['category_description'][0] ?></h2>
		<? if ($entity['required_data'] != ''): ?>
			<br />
			<h2>Required data:</h2>
			<p><?= $entity['required_data'] ?></p>
		<? endif; ?>
	</div>
	<div id="form">
	<?= form_open(site_url('basic/save'), array('id' => 'fsc_request')) ?>
		<?= form_hidden('exclude_template_code',config_item('request_template')); ?>
		<?= form_hidden('exclude_folder_code',config_item('helpdesk_folder')) ?>
		<? // default values to be put to each request ?>
		<?= form_hidden(config_item('type_name'),$entity['category_name'][0]); ?>
		<?= form_hidden('customer2',$user); ?>
		<?= form_hidden('contact_type','Web form'); ?>
		<?= form_fieldset('General request fields'); ?>
		<? foreach ($inputs_general as $input): ?>
			<?= create_inputs($input); ?>
		<? endforeach; ?> 
		<?= form_fieldset_close(); ?>
		
		<? if (!empty($inputs_type)): ?> 
			<?= form_fieldset('Additional info'); ?>
			<? foreach ($inputs_type as $input): ?>
				<?= create_inputs($input); ?>
			<? endforeach; ?> 
			<?= form_fieldset_close(); ?> 
		<? endif; ?>
		
		<?= form_fieldset(); ?>
		<button class="submit" name="exclude_submitbuttonname" value="submitbuttonvalue" id="submiting">Submit</button>
		<?= form_fieldset_close(); ?>
	<?= form_close(); ?>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$("[name=customer]").val($("[name=customer2]").val()).attr('readonly','readonly');
		$("[name=customer2]").detach();
	});
</script>