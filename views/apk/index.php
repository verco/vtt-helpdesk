<?= $this->template->block('header','default/apk_header') ?>

<section id="content">
 <!--	<div class="widgets g6"-->        
		<!--?= config_item('enable_history') ? $this->template->block('history','default/history_service') : '' ?-->
	<!--/div-->
	<!--div class="widgets g6"-->  
		<!--?= config_item('enable_history') ? $this->template->block('history','default/history') : '' ?-->
	<!--/div-->
 	<!--div class="widgets g6">        
		<!--?= config_item('enable_history') ? $this->template->block('history','default/device_report') : '' ?>
	</div-->
	



<!--html lang="fi" xmlns="http://www.w3.org/1999/xhtml" xml:lang="fi">
<head>
 <title>Palvelupyyntö / Service Request 227072</title>
 <link rel="stylesheet" type="text/css" href="http://130.188.1.51:8008/vttinfo/sr_acceptance.css" title="perustyyliarkki" media="screen" />
 <script language="JavaScript" type="text/javascript" src="http://130.188.1.51:8008/vttinfo/sr_acceptance.js"></script>

</head>
<body>

-->

<script language="JavaScript" type="text/javascript">
	var bDebug = false
</script>
 

<p>Asiakaspalvelu on saanut seuraavan palvelupyynnön<br/>Customer service has received the following service request</p>

<table border="0" width="100%" summary="Service Request in Efectessä">
<tr><th colspan="2" class="data_group_header">Palvelupyynnön tiedot / Service Request information</th></tr>
<tr><td class="col1">Asiakas / Customer</td><td><?=$pp[0][config_item('pp_client')][0] ?></td></tr>
<tr><td class="col1">Sähköposti / E-mail</td><td><?=$pp[0][config_item('pp_email')][0] ?></td></tr>
<tr><td class="col1">Kansallisuus / Nationality</td><td><?=$pp[0][config_item('pp_nat')][0] ?></td></tr>
<tr><td class="col1">Puhelin / GSM / Telephone / Mobile</td><td><?=$pp[0][config_item('pp_phone')][0] ?></td></tr>
<tr><td class="col1">Yritys / Company</td><td><?=$pp[0][config_item('pp_company')][0] ?></td></tr>
<tr><td class="col1">Palvelupyynnön aihe / Service request title</td><td><?=$pp[0][config_item('pp_title')][0] ?></td></tr>
<tr><td class="col1">Palvelupyynnön kuvaus / Service request description</td><td><?=nl2br($pp[0][config_item('pp_desc')][0]) ?></td></tr>
</table>
<form method="post" id="form" name="form" enctype="multipart/form-data" action="<?=config_item('app_url')?>basic/save">
<!--form name="pageForm" method="post" action="http://efecte.vtt.fi/efecte/EntityQueryStaticFormSave.do?authuid=efecteweb&hash=R7qKmI1PxNzN8w5%2F8aePAQ%3D%3D">
<input type="hidden" name="staticFormRedirectTarget" value="http://efecte.vtt.fi:8008/vttinfo/sr_handled.html">
<input type="hidden" name="saveEntity" value="1">
<input type="hidden" name="entityEntityGroupID" value="567">
<input type="hidden" name="staticFormUsed" value="true">
<input type="hidden" name="entityTemplateID" value="158" />
<input type="hidden" name="e" value="618933" />
<input type="hidden" name="sr_expert_selected" value="Laurila Maija" />
<input type="hidden" name="ca_1949" value="" />
<tr><td width="5%"><input type="radio" name="accept_radio" value="accept" onclick="var foobar=showReason(0);"/></td><td>Palvelupyyntö on käsitelty ja yhteydenottajan kanssa on sovittu toimenpiteistä<br/>Service request has been considered and actions have been agreed upon with the customer</td></tr>
<tr><td width="5%"><input type="radio" name="accept_radio" value="reject" onclick="var foobar=showReason(1);"/></td><td>Palvelupyyntö ei kuulu vastuulleni. Palautan palvelupyynnön Asiakaspalveluun, koska:<br/>Service request is returned to Customer service while it cannot be handled because:</td></tr>

<input type="hidden" name="ca_1950" value="" /-->
<input type="hidden" name="sr_id" value="<?=$_GET["sr"]?>" />
<input type="hidden" name="exclude_template_code" value="service_request" />
<input type="hidden" name="exclude_folder_code" value="pkp" />


<table border="0" width="100%" summary="Service Request handling">
<tr><th class="data_group_header" colspan="2">Palvelupyynnön käsittely / Service request reply</th></tr>
<tr><td colspan="2"><div class="error_message"></div></td></tr>

<tr><td width="5%"><input type="radio" name="jobstate" value="Suljettu" class="required text_mandatory"></td><td>Palvelupyyntö on käsitelty ja yhteydenottajan kanssa on sovittu toimenpiteistä<br/>Service request has been considered and actions have been agreed upon with the customer</td></tr>
<tr><td width="5%"><input type="radio" name="jobstate" value="Uudelleen selvitys" class="required text_mandatory"></td><td>Palvelupyyntö ei kuulu vastuulleni. Palautan palvelupyynnön Asiakaspalveluun, koska:<br/>Service request is returned to Customer service while it cannot be handled because:</td></tr>
<tr id="reject_reason"><td colspan="2"><textarea name="sr_reject_reason" id="sr_reject_reason" rows="3" cols="40"></textarea></td></tr>

<? if ($attachments!=""): ?>	
<tr><td>Liitteet / Attatchments</td><td><?=$attachments?></td></tr>
<? endif; ?>

<!--tr><td>Lisää tiedosto / Add attatchment</td><td><input type="file" name="<?= config_item('pp_files') ?>[]"></td></tr-->
<tr><td colspan="2"><input type="submit" name="accept_button" value="Laheta / Send"/>&nbsp;<a href="#" onclick="setTimeout(function(){var ww = window.open(window.location, '_self'); ww.close(); }, 1000);">
sulje / close</a></td></tr>
<!--tr><td colspan="2"><input type="submit" name="accept_button" value="Laheta / Send" onClick="return checkForm(this.form)" />&nbsp;<input type="button" name="close_button" value="Sulje / Close" onClick="window.close()" /></td></tr-->
</table>

</form>
<div class="footer" id="foot">

</div>
</section>

<script type="text/javascript">
$().ready(function() {
    // validate the comment form when it is submitted
    $("#form").validate({
         invalidHandler: function(event, validator) {
            // 'this' refers to the form
            var errors = validator.numberOfInvalids();
            if (errors) {
				if($(".text_mandatory:checked").val() == 'Uudelleen selvitys') {
					var message = 'Ole hyvä ja täytä myös syy / Please, fill in the reason field';
				}else{
				  var message = errors == 1
					? 'Ole hyvä ja valitse toinen allaolevista vaihtoehdoista. / Please fill in one of the following options.'
					: 'Ole hyvä ja täytä kaikki pakolliset kentät.';
				}
				$("div.error_message").html(message);
            } else {
              $("section.error_messages").hide();
              $("div.error_message span").hide('slow').removeClass('alert_red');
            }

          },
          errorPlacement: function(error, element) {}
    });
});

$(".text_mandatory").on('change', function() {
	if($(this).val() == 'Uudelleen selvitys') {
		$("#sr_reject_reason").addClass('required');
		message = 'xxxy';
	}else {
		$("#sr_reject_reason").removeClass('required');
	}
});
</script>
	