<?= $this->template->block('header','default/header') ?>
    <section id="content">
        <div class="g12"> 
             
            <h2 class="announ">There will be system update <?=date('Y-m-d 00:00',strtotime('next thursday'))?> UTC. All logins will be disabled for 2 hours. </h2>
            <!--<div class="g6 center">
                <button class="full">Helpdesk<br />
                    <span>1. request</span><br />
                    <span>2. request</span>
                </button>
            </div>
            <div class="g6 center">
                <button class="full">Knowledge base <br />
                    <span>1. article</span><br />
                    <span>2. article</span>
            </button>
            </div>
            <div class="g6 center">
                <button class="full">Services <br />
                    <span>1. service</span> <br>
                    <span>2. service</span>
                </button>
            </div>
            <div class="g6 center">
                <button class="full">My requests<br />
                    <span>1. latest request</span><br />
                    <span>2. latest request</span>
                </button>
            </div>-->

        </section><!-- end div #content -->
<!--<div id="modal">
    
</div>-->
<script>
    $(function() {
        $(".announ").animate({opacity:0},2400,"linear",function(){
            $(this).animate({opacity:1},2400);
        });
        setInterval(function() {
            $(".announ").animate({opacity:0},2400,"linear",function(){
                $(this).animate({opacity:1},2400);
            });
        }, 4800);
        
        <?= config_item('enable_kb') ? $this->template->block('knowledge_js','default/knowledge_js') : '' ?>

        <?= config_item('enable_history') ? $this->template->block('history_js','default/history_js') : '' ?>
        
    });
</script>
      