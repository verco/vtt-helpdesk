<div id="emails">
	<? foreach ($emails as $email): ?>
		<? if ($email->email): ?>
			<div class="email">
				<a href="#" class="remove" title='<?=$email->efecte_id?>'>X</a><span>&nbsp;<?= $email->email.'<br />' ?>
			</div>
		<? endif; ?>
	<? endforeach; ?>
</div>
<a class="button" href="<?=site_url('document/list')?>">Back</a>
<script type="text/javascript">
	$(document).ready(function () {
		$("#emails").on('click','.remove',function(e) {
			e.preventDefault();
			$this = $(this);
			var email = 'efecte_id='+$this.attr('title').trim();
			$.post('/efecte/hpp/index.php/ajax/remove_recipient',email,function(data) {
				/*if (data == 'true')*/ $this.parent().fadeOut('slow',function() { $(this).detach() });
			});
		});
	});
</script>