<? foreach ($doc as $doc): ?>
	<h2><?= $this->lang->line('doc_current_doc') ?></h2>
	<dl>
		<dt><?=$this->lang->line('doc_description')?></dt>
		<dd><?=$doc->description?></dd>
		<dt><?=$this->lang->line('doc_version')?></dt>
		<dd><?=$doc->version?></dd>
		<dt><?=$this->lang->line('doc_format')?></dt>
		<dd><?=$doc->format?></dd>
		<dt><?=$this->lang->line('doc_language')?></dt>
		<dd><?=$doc->language?></dd>
	</dl>
<? endforeach; ?>
<a href="<?=$doc->location?>" class="button"><?=$this->lang->line('doc_download_current')?></a>
<a href="<?=site_url('document/email');?>" class="button"><?=$this->lang->line('doc_email')?></a>