<? $b = base_url(); $br=$b.'resources/'; ?>
<!DOCTYPE html>
<html lang="en-us">
<head>
	<meta charset="utf-8">
	 <meta http-equiv="X-UA-Compatible" content="IE=edge" >
	<title>VTT - APK</title>	
	<link rel="stylesheet" href="<?=$br?>css/style_apk.css?<?=rand(10000000)?>">
<!--	<link rel="stylesheet" href="<?=$br?>css/style2.css?<?=rand(10000000)?>">-->
	<link rel="stylesheet" href="<?=$br?>css/light/theme.css?<?=rand(10000000)?>" id="themestyle">
	<link rel="stylesheet" href="<?=$br?>css/south-street/jquery-ui-1.10.0.custom.min.css?<?=rand(10000000)?>" id="jquery-ui-theme">
	<?= (strpos(current_url(),'services') === false ? '<link rel="stylesheet" href="'.$br.'css/style_helpdesk.css?'.rand(100000000).'">' : '') ?>
	<!--[if lt IE 9]>
	<script src="<?=$br?>js/html5.js"></script>
	<link rel="stylesheet" href="<?=$br?>css/ie.css">
	<![endif]-->
	
	<!-- Apple iOS and Android stuff -->
	<meta name="apple-mobile-web-app-capable" content="no">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
	
	<!-- Apple iOS and Android stuff - don't remove! -->
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1">
	
	<!-- Use Google CDN for jQuery and jQuery UI -->
	<script src="<?=$br?>js/jquery.min.js?<?=rand(10000000)?>"></script>
	<script src="<?=$br?>js/jquery-migrate-1.0.0.js?<?=rand(10000000)?>"></script>
	<script src="<?=$br?>js/jquery-ui-1.10.0.custom.min.js"></script>
	<script src="<?=$br?>js/jquery.validate.min.js"></script>

	<script type="text/javascript" src="<?=$br?>js/jquery.uniform.min.js?<?=rand(10000000)?>"></script>

	<script src="<?=$br?>js/script.js?<?=rand(10000000)?>"></script>
	<script type="text/javascript">
		$(function() {
			$.ajaxSetup({
				cache: false
			});
		});
	</script>		
</head>
<body>
	<?= $this->template->yield() ?>
</body>
</html>