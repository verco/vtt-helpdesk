<? $b = base_url(); $br=$b.'resources/'; ?>
<div class="container">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"><? //= lang('feedback_subtitle') ?></h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <?if(isset($ticket)): ?>
                    <form id="form" role="form" action="<?= site_url('feedback/save') ?><?= isset($language) && $language == 'fi' ? '?lang=fi' : '?lang=en' ?>" method="post">
                        <div class="col-md-12">
                            <span class="pull-right">
                                <a href="javascript:void(0)" id="lang-fi"><img src="<?= $br ?>img/fi.png" style="height:20px;"/></a> | <a href="javascript:void(0)" id="lang-en"><img src="<?= $br ?>img/en.png" style="height:20px;"/></a>
                            </span>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="subject" class="control-label"><?= lang('subject') ?></label>
                            <input type="text" class="form-control" id="subject" value="<?= htmlspecialchars($ticket[config_item('ticket_subject')]) ?>" readonly disabled>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="ticket_id" class="control-label"><?= lang('ticket_id') ?></label>
                            <input type="text" class="form-control" id="ticket_id" name="ticket_id" value="<?= htmlspecialchars($ticket[config_item('ticket_efecteid')]) ?>" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="description" class="control-label"><?= lang('description') ?></label>
                            <textarea class="form-control" id="description" rows="6" readonly><?= htmlspecialchars($ticket[config_item('ticket_description')]) ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="solution" class="control-label"><?= lang('solution') ?></label>
                            <textarea class="form-control" id="solution" rows="6" readonly><?= htmlspecialchars($ticket[config_item('ticket_resolution')]) ?></textarea>
                        </div>

                        <div class="col-md-5">
                            <label class="control-label"><?= lang('acceptance_question') ?> *</label>
                        </div>
                        <div id="acceptance" class="form-group col-md-7 btn-group" data-toggle="buttons">
                            <label class="btn btn-success">
                                <input type="radio" name="accept" value="yes" id="accept_yes"> <i class="glyphicon glyphicon-ok"></i> <?= lang('yes') ?>
                            </label>
                            <label class="btn btn-danger">
                                <input type="radio" name="accept" value="no" id="accept_no"> <i class="glyphicon glyphicon-remove"></i> <?= lang('no') ?>
                            </label>
                        </div>

                        <div class="col-md-12"><hr></div>

                        <div data-toggle="false" class="collapse" id="evaluation">
                            <div class="col-md-5">
                                <label class="control-label"><?= lang('expectations_question') ?></label>
                            </div>
                            <div class="form-group col-md-7 btn-group" data-toggle="buttons">
                                <label class="btn btn-success meet_expectation">
                                    <input type="radio" name="meet_expectation" value="Ilahduin" id="happy"> <i class="glyphicon glyphicon-thumbs-up"></i> <?= lang('expectations_happy') ?>
                                </label>
                                <label class="btn btn-warning meet_expectation">
                                    <input type="radio" name="meet_expectation" value="Ihan ok" id="ok"> <i class="glyphicon glyphicon-ok"></i> <?= lang('expectations_ok') ?>
                                </label>
                                <label class="btn btn-danger meet_expectation">
                                    <input type="radio" name="meet_expectation" value="Petyin" id="disappointed"> <i class="glyphicon glyphicon-thumbs-down"></i> <?= lang('expectations_disappointed') ?>
                                </label>
                            </div>

                            <div class="clearfix">&nbsp;</div>

                            <div class="col-md-5">
                                <label for="numerical_evaluation" class="control-label"><?= lang('satisfaction_question') ?> *</label><br><?= lang('satisfaction_legend') ?>
                            </div>
                            <div class="form-group col-md-7">
                                10 <input id="numerical_evaluation" name="numerical_evaluation" data-slider-id='numerical_evaluation_slider' type="text" data-slider-min="1" data-slider-max="10" data-slider-value="8.5" data-slider-step="1" data-slider-reversed="true" data-slider-selection="none"> 1
                            </div>

                            <div class="clearfix">&nbsp;</div>

                            <div class="form-group col-md-12">
                                <label class="control-label"><?= lang('free_comment') ?></label>
                                <textarea class="form-control" id="free_comment" name="free_comment" rows="5"></textarea>
                            </div>
                        </div>

                        <div data-toggle="false" class="collapse" id="no_acceptance">
                            <div class="form-group col-md-12">
                                <label class="control-label"><?= lang('additional_comment') ?>*</label>
                                <textarea class="form-control" id="additional_comment" name="additional_comment" rows="5"></textarea>
                            </div>
                        </div>
                        
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="pull-right">* <?= lang("mandatory_field") ?></p>
                                </div>
                            </div class="row">
                            <div class="row">
                                <div class="col-md-12">
                                    <button id="submit" type="submit" class="btn btn-primary pull-right" disabled><?= lang('submit') ?></button>
                                </div>
                            </div>
                        </div>
                    </form>
               <?else: ?>
                    <? /*<form id="form" role="form" action="<?= site_url('feedback/save') ?><?= isset($language) && $language == 'fi' ? '?lang=fi' : '?lang=en' ?>" method="post">
                        <input type="hidden" value="regular_feedback" name="exclude_feedback">
                        <div data-toggle="false" id="evaluation">
                            <div class="col-md-4">
                                <label class="control-label"><?= lang('expectations_question') ?></label>
                            </div>
                            <div class="form-group col-md-8 btn-group" data-toggle="buttons">
                                <label class="btn btn-success">
                                    <input type="radio" name="meet_expectation" value="happy" id="happy"> <i class="glyphicon glyphicon-thumbs-up"></i> <?= lang('expectations_happy') ?>
                                </label>
                                <label class="btn btn-warning">
                                    <input type="radio" name="meet_expectation" value="ok" id="ok"> <i class="glyphicon glyphicon-ok"></i> <?= lang('expectations_ok') ?>
                                </label>
                                <label class="btn btn-danger">
                                    <input type="radio" name="meet_expectation" value="disappointed" id="disappointed"> <i class="glyphicon glyphicon-thumbs-down"></i> <?= lang('expectations_disappointed') ?>
                                </label>
                            </div>

                            <div class="clearfix">&nbsp;</div>

                            <div class="col-md-4">
                                <label for="numerical_evaluation" class="control-label"><?= lang('satisfaction_question') ?></label><br><?= lang('satisfaction_legend') ?>
                            </div>
                            <div class="form-group col-md-8">
                                10 <input id="numerical_evaluation" name="numerical_evaluation" data-slider-id='numerical_evaluation_slider' type="text" data-slider-min="1" data-slider-max="10"
                                          data-slider-value="9" data-slider-step="1" data-slider-reversed="true" data-slider-selection="none"> 1
                            </div>

                            <div class="clearfix">&nbsp;</div>

                            <div class="form-group col-md-12">
                                <label class="control-label"><?= lang('free_comment') ?></label>
                                <textarea class="form-control" id="free_comment" name="free_comment" rows="10"></textarea>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <button id="submit" type="submit" class="btn btn-primary pull-right" disabled><?= lang('submit') ?></button>
                        </div>
                    </form> 
                    */ ?>
                <?endif; ?> 
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {

        
        $('#lang-fi').on('click', function() {
            location.replace(location.origin + location.pathname + '?lang=fi');
        });
        $('#lang-en').on('click', function() {
            location.replace(location.origin + location.pathname + '?lang=en');
        });
            
        $('#numerical_evaluation').slider();

		$('#acceptance').on('change', function(){
            if($('#accept_no').is(':checked')) {
                $('#free_comment').val('');
            } else {
                $('#additional_comment').val('');
                $('.meet_expectation').removeClass('active');
                $('#submit').prop("disabled", true);
            }
		});

        $('#numerical_evaluation').on('change',function() {
            if($('#submit').prop("disabled")) {
                $('#submit').prop("disabled", false);
            }
        });


        $('#accept_yes').on('change', function() {
            $('#no_acceptance').collapse('hide');
            $('#evaluation').collapse('show');
            $('#additional_comment').prop('required',false);
        });

        $('#accept_no').on('change', function() {
            $('#evaluation').collapse('hide');
            $('#no_acceptance').collapse('show');
            $('#additional_comment').prop('required',true);
            $('#submit').prop("disabled", true);
        });

        $('#additional_comment').on('keyup', function() {
            if($('#additional_comment').val().length > 0) {
                $('#submit').prop("disabled", false);
            } else {
                $('#submit').prop("disabled", true);  
            }
        });
		
        


        /*$.extend($.validator.messages, {
            required: "Tämä kenttä on pakollinen"
        });


        //$('#form').validate();
       */ 
    });
</script>