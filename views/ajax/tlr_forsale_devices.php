<? if ($output): ?>
				<?//=var_dump($output)?>
		
		<div id="container">
			<table class="ddtable" id="ddtable" style="width:500px;">
				<thead><tr>
					<!--th>&nbsp;</th-->
				<th>LaiteID</th>
				<th>Laitteen nimi</th>
				<th>Kategoria</th>
				<th>Erityisluokka</th>
				<th>Rakennus</th>
				<th>Huone</th>
				<th>Laitteen tila</th>
				<th>Laiteympäristö</th>
				<!--th>Laitekoordinaattori</th-->
				<th>Laitevastuuhenkilö</th>
				<th>Kohdenumero (Kasperi)</th>
				<th>Laiterekisterinumero (Kasperi)</th>
				<th>Ostopäivä</th>
				<th>Laite vapaa alkaen</th>
				<th>Ilmoitus voimassa</th>
				<!--th>Lisätiedot</th-->
				<th>Myyntiä hoitaa</th>
				<th>Lisätiedot</th>

				<!--th>&nbsp;</th-->
				<!--th>Laitetapahtumat</th>
				</tr-->
				</thead>


				<?//$i=0?>
				<tbody>
				<? foreach ($output as $p): ?>
				<tr>
                    <!--td><?echo $i+1;?></td-->
					<? foreach ($cols as $c): ?>
						<td>
						<?//=$p[$c]?>
						<?
						if (substr($p[$c],0,7)=='http://'){
								echo "<a href=\"".$p[$c]."\" target=\"_blank\">".$p[$c]."</a>";						
							}else{
								$time_strip=array(" 00:00:00 EEST"," 00:00:00 EET");
								//echo str_replace($time_strip,"",$p[$c])."";
								if ((str_replace($time_strip,"",$p[$c])."") == $p[$c].""){
									if($c==config_item('tlr_dev_handler')){
										if($p[$c]==""){
											echo nl2br($p[config_item('tlr_dev_laitekoordinaattori_ref')]."");
										}else{ 
											echo nl2br($p[$c]."");
										}	
									}else{
										echo nl2br($p[$c]."");
									}
								}else{ 
									echo date_format(date_create($p[$c]),"d.m.Y");
								}
							}
						?>
						<?
/*
							if ($p[$c][1]){
								echo "arr: ";
							}
							if (substr($p[$c][0],0,7)=='http://'){
								echo "<a href=\"".$p[$c][0]."\" target=\"_blank\">".$p[$c][0]."</a>";						
							}else{
								$time_strip=array(" 00:00:00 EEST"," 00:00:00 EET");
								echo str_replace($time_strip,"",$p[$c][0]);
							}
*/						
						foreach ($p[$c] as $x){
							if (substr($x,0,7)=='http://'){
								echo "<a href=\"".$x."\" target=\"_blank\">".$x."</a>";						
							}else{
								$time_strip=array(" 00:00:00 EEST"," 00:00:00 EET");
								echo str_replace($time_strip,"",$x)."";
							}
						}
						?>

							<?// foreach ($p[$c] as $values): ?>
								<?//=$values?>
							<?// endforeach; ?>

						</td>
					<? endforeach; ?>
				 </tr>        
            <? $i++ ?>                             
            <? endforeach; ?>
			</tbody>
			</table>
				
	</div>
        <? else: ?>
            <h5>Tällä hetkellä ei ole myynnissä olevia laitteita.</h5>
        <? endif; ?>

<script type="text/javascript">
$(document).ready(function() {

	var table = $('#dtable').DataTable({
	//$('#table').dataTable({
		//sDom: 'lfrtipWT<"clear"><"top"i>',
		sDom: 'TWirft<"clear"><"bottom"lp>',
		"sScrollX": "100%",
			"sScrollY": "100%",
		
        //sDom: ''	,	
		//sDom: 'T<"clear">lfrtip',
		"oColumnFilterWidgets": {
        <? if ($all): ?>		
			"aiExclude": [ 2,3,4,6,8,9,13 ],
        <? elseif ($output): ?>
			"aiExclude": [ 0,1,2,10,11 ],
        <? endif; ?>		
			"bGroupTerms": false,
			"sSeparator": "\\s*/+\\s*"
		},
		"oTableTools": {
            "sSwfPath": "<?=base_url()?>resources/swf/copy_csv_xls_pdf.swf",
            "sRowSelect": "multi",
            "aButtons": [
                {
                    "sExtends": "csv",
                    "bSelectedOnly": true,
                    "fnComplete": function ( nButton, oConfig, oFlash, sFlash ) {
                        var oTT = TableTools.fnGetInstance( 'dtable' );
                        var nRow = $('#dtable tbody tr');
                        oTT.fnDeselect(nRow);
                    }
                }
				
            ]		
        }
		
		
		}	
	);
	    $('a.DTTT_button_csv').mousedown(function(){
        var oTT = TableTools.fnGetInstance( 'dtable' );
        var nRow = $('#dtable tbody tr');
        oTT.fnSelect(nRow);
    });
/*
    $('#dtable').DataTable( {
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );	*/
	

    // Setup - add a text input to each footer cell
    $('#dtable tfoot th').each( function () {
        var title = $('#dtable thead th').eq( $(this).index() ).text();
		//        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
			$(this).html('<input type="text" placeholder="Filter by '+title+'" />' );
    } );
 
    // DataTable
//	var table = $('#dtable').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
		
    } );

    } 
	);

</script>