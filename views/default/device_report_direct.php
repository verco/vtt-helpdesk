<!--<div class="widget" id="last_widget_1">
Mikäli listaus sisältää laitteita, jotka eivät omassa käytössäsi, tai listalta puuttuu laitteitasi tai jos muissa tiedoissa on virheitä, ilmoita puutteelliset tiedot <a href="mailto:kayttotuki@vtt.fi">käyttötukeen</a>. Kiitos!
</div>-->
<!--div class="widget" id="last_widget">
If the list contains devices that are no longer in your use or if some device is missing from the list or displayed information is otherwise incorrect, please notify <a href="mailto:kayttotuki@vtt.fi">Käyttötuki</a> to update the list. Thank you!
</div-->
     
<? if ($user_data): ?>
<div class="widget" id="last_widget">
<h3>Device Report for
<?=$user_data[0][config_item('person_fullname')][0]?>
 (<?=$user_data[0][config_item('person_account')][0]?>)
<i><?=$user_data[0][config_item('title')][0]?></i></h3>
</div>
<? endif; ?>

<div class="widget" id="last_widget_3">
    <h3 class="handle">Computers</h3>
    <div id="pc">
        <? if ($pc): ?>
            <? $i = 0 ?>
			<table id="pc_table">
				<thead><tr><th>Computer name</th><th>Serialnumber</th><th>F-id</th><th>User</th><th>Model</th><th>Purchase date</th><th>Scanned</th><th>Use type</th><th>Status</th></tr></thead>
            <? foreach ($pc as $p): ?>
				<tr>
                    <td><?=$p[config_item('pc_hostname')][0]?></td>
                    <td><?=$p[config_item('pc_serial')][0]?></td>
                    <td><?=$p[config_item('pc_f')][0]?></td>
                    <td><?=$p[config_item('pc_user')][0]?></td>
                    <td><?=$p[config_item('pc_model')][0]?></td>
                    <td><? if ($p[config_item('pc_pdate')][0]): ?><?=date_format(date_create($p[config_item('pc_pdate')][0]),"d.m.Y")?><? endif; ?></td>
                    <td><? if ($p[config_item('pc_scandate')][0]): ?><?=date_format(date_create($p[config_item('pc_scandate')][0]),"d.m.Y")?><? endif; ?></td>
                    <td><?=$p[config_item('pc_billing_type')][0]?></td>
                    <td><?=$p[config_item('pc_status')][0]?></td>
                </tr>
					<? //if ($i == 4) break; ?>
            <? $i++ ?>                             
            <? endforeach; ?>
			</table>
        <? else: ?>
            <h4>None found</h4>
        <? endif; ?>
    </div>
</div>

<div class="widget" id="last_widget_4">
    <h3 class="handle">Displays</h3>
    <div id="displ">
        <? if ($displ): ?>
            <? $i = 0 ?>
			<table id="displ_table">
				<thead><tr><th>F-id</th><th>Model</th><th>Size</th><th>User</th><th>Location</th><th>Status</th><th>Purchase date</th></tr></thead>
            <? foreach ($displ as $d): ?>
				<tr>
                    <td><?=$d[config_item('displ_fno')][0]?></td>
                    <td><?=$d[config_item('displ_model')][0]?></td>
                    <td><?=$d[config_item('displ_size')][0]?></td>
                    <td><?=$d[config_item('displ_user')][0]?></td>
                    <td><?=$d[config_item('displ_loc')][0]?></td>
                    <td><?=$d[config_item('displ_status')][0]?></td>
                    <td><? if ($d[config_item('displ_pdate')][0]): ?><?=date_format(date_create($d[config_item('displ_pdate')][0]),"d.m.Y")?><? endif; ?></td>
                </tr>
					<? //if ($i == 4) break; ?>
            <? $i++ ?>                             
            <? endforeach; ?>
			</table>
        <? else: ?>
            <h4>None found</h4>
        <? endif; ?>
    </div>
</div>


<div class="widget" id="last_widget_5">
    <h3 class="handle">Mobile phones</h3>
    <div id="mobile">
        <? if ($mob): ?>
            <? $i = 0 ?>
			<table id="mobile_table">
				<thead><tr><th>SIM</th><th>Model</th><th>Serialnumber</th><th>User</th><th>Status</th><th>Purchase date</th></tr></thead>
            <? foreach ($mob as $m): ?>
				<tr>
                    <td><?=$m[config_item('mob_sim')][0]?></td>
                    <td><?=$m[config_item('mob_model')][0]?></td>
                    <td><?=$m[config_item('mob_serial')][0]?></td>
                    <td><?=$m[config_item('mob_user')][0]?></td>
                    <td><?=$m[config_item('mob_status')][0]?></td>
                    <td><? if ($m[config_item('mob_pdate')][0]): ?><?=date_format(date_create($m[config_item('mob_pdate')][0]),"d.m.Y")?><? endif; ?></td>
                </tr>
					<? //if ($i == 4) break; ?>
            <? $i++ ?>                             
            <? endforeach; ?>
			</table>
        <? else: ?>
            <h4>None found</h4>
        <? endif; ?>
    </div>
</div>



<script type="text/javascript">
$(document).ready(function() {
	var dataTablesOptions = {
				"aLengthMenu" : [10, 50, 100, 200,1000],
				"order": [[3, "asc"]],
				"columnDefs": [
					{ 'type': 'de_date', 'targets': [5] }
				]
			};
	var table = $('#pc_table').DataTable(dataTablesOptions);

	var ddataTablesOptions = {
				"aLengthMenu" : [10, 50, 100, 200,1000],
				"order": [[3, "asc"]],
				"columnDefs": [
					{ 'type': 'de_date', 'targets': [6] }
			
				]
			};
    var dtable = $('#displ_table').DataTable(ddataTablesOptions);
	var mdataTablesOptions = {
				"aLengthMenu" : [10, 50, 100, 200,1000],
				"order": [[3, "asc"]],
				"columnDefs": [
					{ 'type': 'de_date', 'targets': [5] }
			
				]
			};	
    var mtable = $('#mobile_table').DataTable(mdataTablesOptions);
	}

);

</script>

<!--<div id="modal">
</div>>
<script type="text/javascript">
$(function() {
        var modal = $("#modal").dialog({ autoOpen: false})
        <!--?= config_item('enable_history') ? $this->template->block('history_js','default/history_js') : '' ?>
});

</script-->