<div id="sidebar">
	<div class="sidebar-header">&nbsp;</div>
	<div class="sidebar-content">
		<div class="sidenavi">
			<ul>
				<li class="selected"><a href="<?=config_item('app_url')?>">Select Category</a>
				<ul>-
					<li><a href="<?=site_url('fsc_myrequests/')?>">My Requests</a>
						<ul>
							<li><a href="<?=site_url('fsc_myrequests/show/open_requests')?>">Open requests</a></li>
							<li><a href="<?=site_url('fsc_myrequests/show/waiting_approvals')?>">Waiting for approvals</a></li>
							<li><a href="<?=site_url('fsc_myrequests/show/waiting_reviews')?>">Waiting for reviews</a></li>
							<li><a href="<?=site_url('fsc_myrequests/show/waiting_completion')?>">Waiting for completion</a></li>
							<li><a href="<?=site_url('fsc_myrequests/show/closed')?>">Closed requests</a></li>
						</ul>
					</li>
					<li><a href="<?=site_url('fsc_myrequests/')?>">Waiting for my action</a>
						<ul>
						<li><a href="<?=site_url('fsc_myrequests/show/waiting_approvals/own')?>">Waiting for approvals</a></li>
						<li><a href="<?=site_url('fsc_myrequests/show/waiting_reviews/own_reviews')?>">Waiting for reviews</a></li>
					</ul>
					</li>
	<? /* foreach ($processes as $process): ?>
			<li class="process" id="<?=$process->process_name?>"><a title="<?=$process->process_description?>" href='#' class='processes'><?=$process->process_name?></a>
			<? if ($process->types): ?>
				<ul style="display:none" class="alanavi">
					<? foreach($process->types as $type): ?>
					<? /* <?var_dump($type)?>  ?>
					<li><a class="category" href="<?=site_url('fsc_request/form'.'/'.$type->entityid)?>" title="<?=$type->category_description?>"><?= $type->category_name ?></a></li>
				<? endforeach; ?>
				</ul>
			<? endif; ?> 
			<? /* $field == config_item('sscpro_desc') ? "<div>" . $process->$field . "</div>" : ""  ?>

		</li>
	<? endforeach; */ ?>
			</ul>
			</li>
		</ul>
	</div>
	</div>
</div>
<script type="text/javascript">

</script>