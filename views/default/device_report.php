<!--<div class="widget" id="last_widget_1">
Mikäli listaus sisältää laitteita, jotka eivät omassa käytössäsi, tai listalta puuttuu laitteitasi tai jos muissa tiedoissa on virheitä, ilmoita puutteelliset tiedot <a href="mailto:kayttotuki@vtt.fi">käyttötukeen</a>. Kiitos!
</div>-->
<div class="widget" id="last_widget">
If the list contains devices that are no longer in your use or if some device is missing from the list or displayed information is otherwise incorrect, please notify <a href="mailto:kayttotuki@vtt.fi">Käyttötuki</a> to update the list. Thank you!
</div>
<?
	//var_dump($account);
	//var_dump($sessio);
	//var_dump($isSuperior);

?>
<? if ($isSuperior): ?>
	<div class="widget" id="last_widget">
		<h3 class="handle report_by_user hide">Report by user</h3>
		<button class="report_by_user hide" id="reset_user_selection">Reset Selection</button>
		<div class="report_by_user hide" id="columns"></div>
	</div>
	
<? endif ?>

<!-- /*
<? if ($noaccess): ?>
	Access denied.
 <? elseif ($account): ?>
		<div class="widget" id="last_widget">

		Device report for <?=$account_fullname[0]['full_name'][0]?> (<?=$account?>)

		<br><br>
		 <a href="<?=site_url('my_requests')?>/index/group"> << Back </a><br> 
		</div> 
<? endif; ?>
<? if ($user_data): ?>
<div class="widget" id="last_widget">
Laiteraportti käyttäjälle: 
<?=$user_data[0][config_item('person_fullname')][0]?>
 (<?=$user_data[0][config_item('person_account')][0]?>)
<i><?=$user_data[0][config_item('title')][0]?></i>
</div>
<? endif; ?> -->

<div id="devices"></div>
<div class="modal"><!-- Place at bottom of page --></div>


<script type="text/javascript">
$(document).ready(function() {
	$body = $("body");
	
	function draw_columns(init) {
		$("#columns").hColumns({
			nodeSource: function(node_id, callback) {
				$.ajax({
					url: "<?=site_url('ajax/get_employees')?>", 
					method: "POST",
					dataType: "json",
					data: {search_user: node_id}
				})
				.done(function(data) {
					if(init===true) {
						draw_devices("<?=$account?>", "group");
						init = false;
					} else {
						draw_devices(node_id);
					}
					return callback(null, data);
				}).fail(function() {
					return callback("AJAX error");
				});
			}
		});
	}

	function draw_devices(account, group) {
		
		$.ajax({
			url: "<?=site_url('ajax/get_devices')?>",
			method: "POST",
			data: {account: account, group: group}
		}).done(function(data) {
			$("#devices").html(data);
			$body.removeClass("loading");
		});
	}
	
	draw_columns(true);
	
	$("#my_requests_btn").on('click', function() {
		$body.addClass("loading");
		$(".report_by_user").addClass("hide");
		draw_devices("<?=$account?>", "group");
	});

	$("#my_group_devices_btn").on('click', function() {
		$body.addClass("loading");
		$(".report_by_user").removeClass("hide");
		$("#columns").remove();
		$("button.report_by_user").after('<div class="report_by_user" id="columns"></div>');
		draw_columns();		
	});
		
	$("#reset_user_selection").on('click', function() {
		$body.addClass("loading");
		$("#columns").remove();
		$("button.report_by_user").after('<div class="report_by_user" id="columns"></div>');
		draw_columns();		
	});

});

</script>