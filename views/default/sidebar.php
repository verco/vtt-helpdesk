<nav>
	<ul>
	<? foreach ($processes as $process): ?>
			<li class="process" id="<?=$process['process_name']?>">
				<a title="<?=$process['process_description']?>" href='#' class='processes'>
					<span><?=$process['process_name']?></span>
				</a>
			<? if ($process['types']): ?>
				<ul>
					<? foreach($process['types'] as $type): ?>
					<li><a class="category" href="<?=site_url('services/form'.'/'.urlencode($type['category_name']))?>" title="<?=$type['category_description']?>"><span><?= $type['category_name'] ?></span></a></li>
				<? endforeach; ?>
				</ul>
			<? endif; ?> 
			<? /* $field == config_item('sscpro_desc') ? "<div>" . $process->$field . "</div>" : "" */ ?>
		</li>
	<? endforeach; ?>
	</ul>
</nav>
<script type="text/javascript">
	$(document).ready(function() {
		var cont = $("#contentbox").html();
		$(".processes").on('click', function(e) {
			e.preventDefault();
			$(".alanavi").slideUp();
			/*$.get('<?=site_url("ajax/get_subnavi")?>')*/
			$(this).next().slideToggle();
			 $(".category").hover(function() {
			 	$("#contentbox").html($(this).attr('title'));
			 	$(this).css('cursor','pointer');
			 }, function() {
			 	$("#contentbox").html(cont);
			 });
		});
		$(".processes").hover(function() {
			 	$("#contentbox").html($(this).attr('title'));
			 	$(this).css('cursor','pointer');
			 }, function() {
			 	$("#contentbox").html(cont);
			 });
	});
</script>