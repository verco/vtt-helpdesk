<div class="widget" id="last_widget">
    <h3 class="handle">Last helpdesk requests</h3>
    <div id="requests">
        <? if ($requests): ?>
            <? $i = 0 ?>
            <? foreach ($requests as $req): ?>
                <div class="<?=$req->entityid?>">
                    <h3><?=$req[config_item('helpdesk_subject')]?></h3>
                    <p><?=$req[config_item('helpdesk_description')]?></p>
                    <hr />
                </div>  
            <? if ($i == 4) break; ?>
            <? $i++ ?>                             
            <? endforeach; ?>
        <? else: ?>
            <h4>No previous requests found</h4>
        <? endif; ?>
    </div>
</div>