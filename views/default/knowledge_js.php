var words = Array();
var cases = Array();
var model = $("#modal");
var knowledge = $("#knowledge_content").html();
$("#textarea_auto, #text_field").on('keyup blur', function (e) {
    $this = $(this);

    if (e.which == 8 && $this.val().length == 0) {
        if (($this.attr('id') == 'textarea_auto' && $("#text_field").val().length < 1) || ($this.attr('id') == 'text_field' && $("#textarea_auto").val().length <1 )) {
            $("#knowledge_content").slideUp().empty().html(knowledge).slideDown();
            
            words = [];
            cases = [];
        }
    }
    if ((e.which == 32 || e.which == 190 || e.which == 9) || e.originalEvent.type == 'blur') { // space (32) pressed or . (190) pressend or tab (9)
        $last = lastWord($this.val()); // get last word
        if ($last.length > 4) { // is greater than 4 in length
            if ($.inArray($last, words) < 0) { // hasn't been typed before 
                words.push($last); // saved so not fetched again
                $.getJSON('<?=site_url("ajax/search")?>',{term: $last}, function(data) {
                   if (data != '') {
                        $.each(data, function (index, kb) {
                            if ($.inArray(kb.entityid, cases) < 0) {
                                cases.push(kb.entityid);
                                $("#remove").detach();
                                $("#knowledge_content").append('<div class="kb_name hidden" id="'+kb.entityid['0']+'"><h4>'+kb.kb_name+'</h4><p class="kb_desc">'+kb.kb_description+'</p></div>');
                                $("#knowledge").slideDown();
                                $(".kb_name").slideDown();
                                $(".kb_name").on('click', function(e) {
                                    modal.empty();
                                    modal.html(loading);
                                    modal.dialog('open');
                                    $.post('<?=site_url("ajax/search_knowledge")?>', {entityid: $(this).attr('id')}, function(data) {
                                        modal.html(data);
                                        
                                    });
                                });
                            }
                        });
                    }
                });
            }
        }
    }
});
var lastWord = function(o) {
    return (""+o).replace(/[\s-]+$/,'').split(/[\s-]/).pop();
};